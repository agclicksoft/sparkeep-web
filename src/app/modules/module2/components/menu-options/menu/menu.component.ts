import { UtilitiesService } from './../../../../../core/services/utilities.service';
import { PostService } from './../../../../../core/services/post.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  profile = '';

  constructor(
    private postService: PostService,
    private utilitiesService: UtilitiesService
  ) { }

  ngOnInit(): void {
  }

  alternarProfile(profile){
    this.profile = profile;
    this.postService.profile.emit(profile);
  }

}
