import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UtilitiesService } from './../../../../../core/services/utilities.service';
import { Post } from '../../../../../shared/models/post.model';
import { PostService } from '../../../../../core/services/post.service';
import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { ShareExternallyComponent } from 'src/app/shared/template/share-externally/share-externally.component';
import { ComplaintPostComponent } from 'src/app/shared/template/complaint-post/complaint-post.component';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css']
})
export class GlobalComponent implements OnInit, OnDestroy {

  posts: Post = new Post;
  page = 1;
  profile = '';
  profileEvent: any;

  img_clap = '../../../../../assets/svg/clap.svg';
  img_clap_roxo = '../../../../../assets/svg/clap-roxo.svg';

  reported = false;

  user: any;

  ownerPost = false;

  constructor(
    private postService: PostService,
    private utilitiesService: UtilitiesService,
    private dialog: MatDialog,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.getUserLogged();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    const pos = document.body.scrollHeight;
    const max = window.innerHeight + window.scrollY;

    if (pos === max) {
      if (this.posts.page <= this.posts.lastPage) {
        this.appendPosts();
      }
    }
  }

  openDialog(post, event) {
    this.dialog.open(ComplaintPostComponent, {
     data: {
       post,
       event
     },
     maxWidth: '580px',
     width: '100%'
    })
  }

  getLastPost() {
    this.postService.addPost.subscribe(response => {
      if (response !== null) {
        this.posts.data.unshift(response);
        this.postService.addPost.next(null);
      }
    });
  }

  getGlobal() {
    this.profileEvent = this.postService.profile.subscribe(Response => {
      this.page = 1;
      this.profile = Response;
      this.postService.getGlobal(this.profile, this.page).subscribe(res => {
        this.posts = this.checkIfOwner(res);
        // this.posts = res;
        console.log('POST: ', this.posts);
      });
    });

    this.postService.getGlobal(this.profile, this.page).subscribe(Response => {
      this.posts = this.checkIfOwner(Response);
      // this.posts = Response;
      // console.log('POST: ', this.posts);
    }, error=> {}, () => {
      this.getLastPost();
    });

    this.page++;
  }

  checkIfOwner(response: Post): any {
    let newResponse = {
      data: [],
      lastPage: response.lastPage,
      perPage: response.perPage,
      total: response.total,
      page: response.page
    };
    for (let post of response.data) {
      if (post.user_id === this.user.id) {
        post.owner = true;
      } else {
        post.owner = false;
      }
      newResponse.data.push(post);
    }
    return newResponse;
  }

  appendPosts() {
    this.profileEvent = this.postService.profile.subscribe(Response => {
      this.profile = Response;
      this.postService.getGlobal(this.profile, this.page).subscribe(
        (res: any) => {
          res = this.checkIfOwner(res);
          console.log(res);
          this.posts.total = res.total,
          this.posts.page = res.page,
          this.posts.perPage = res.perPage,
          this.posts.lastPage = res.lastPage;
          this.posts.data = [...this.posts.data, ...res.data]
        },
        error => {
          console.log(error);
        }
      );
    });

    this.postService.getGlobal(this.profile, this.page).subscribe(
      res => {
        res = this.checkIfOwner(res);
        this.posts.total = res.total,
        this.posts.page = res.page,
        this.posts.perPage = res.perPage,
        this.posts.lastPage = res.lastPage,
        this.posts.data = [...this.posts.data, ...res.data];
      },
      error => {
        console.log(error);
      }
    );

    this.page++;
  }

  ngOnDestroy(): void {
    this.profileEvent.unsubscribe();
    // this.postService.addPost.unsubscribe();
  }

  likePost(id) {
    this.postService.likePost(id).subscribe(Response => {
      const message = Response?.message;
      const index = this.posts.data.map(el => el.id).indexOf(id);
      this.posts.data[index].liked = !this.posts.data[index].liked;

      if (this.posts.data[index].liked){
        this.posts.data[index].__meta__.total_inspireds = parseInt(this.posts.data[index].__meta__.total_inspireds) + 1;
      }else{
        this.posts.data[index].__meta__.total_inspireds = parseInt(this.posts.data[index].__meta__.total_inspireds) - 1;
      }
    }, error => {
      this.utilitiesService.openSnackBar(`${error.error}`);
    });
  }

  sharePost(id) {
    this.utilitiesService.idPostShare = id;
    this.dialog.open(ShareExternallyComponent);
  }

  gotoProfile(post){
    // console.log('POST>>>', post);
    post.author === 'user' ? this.router.navigate([`/perfil/${post?.authorUser?.id}`]) : '';
    post.author === 'event' ? this.router.navigate([`/evento/${post?.authorEvent?.id}`]) : '';
    post.author === 'project' ? this.router.navigate([`/projeto/${post?.authorProject?.id}`]) : '';
    post.author === 'organization' ? this.router.navigate([`/entidade/${post?.authorOrganization?.id}`]) : '';
  }

  removePostOfData(arr, data) {
    const index = arr.indexOf(data);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
  }

  deletePost(post){
    this.postService.delete(post.id).subscribe(
      response => {
        this.removePostOfData(this.posts.data, post);
        this.utilitiesService.openSnackBar('Postagem deletada.');
      },
      error => {
        this.utilitiesService.openSnackBar(`${error.error.message}`);
      }
    );
  }

  handleDeletePost(post){
    // console.log('Esse é o post: ', post);
    if ( post.authorUser ){
      this.user.id === post.authorUser.id ? this.deletePost(post) : '';
    }
    if ( post.authorProject ){
      this.user.id === post.authorProject.user_id ? this.deletePost(post) : '';
    }
    if ( post.authorOrganization ){
      this.user.id === post.authorOrganization.user_id ? this.deletePost(post) : '';
    }
    if ( post.authorEvent ){
      this.user.id === post.authorEvent.user_id ? this.deletePost(post) : '';
    }
  }

  getUserLogged(){
    this.userService.getAccountLogged().subscribe(
      response => {
        this.user = response;
        // console.log('usuário ===>', this.user);
      },
      error => {
        this.utilitiesService.openSnackBar(`${error.error.message}`);
      },
      () => {
        this.getGlobal();
      }
    );
  }
}
