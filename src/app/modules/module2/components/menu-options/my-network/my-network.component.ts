import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { ShareExternallyComponent } from './../../../../../shared/template/share-externally/share-externally.component';
import { MatDialog } from '@angular/material/dialog';
import { UtilitiesService } from './../../../../../core/services/utilities.service';
import { Post } from './../../../../../shared/models/post.model';
import { PostService } from './../../../../../core/services/post.service';
import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { ComplaintPostComponent } from 'src/app/shared/template/complaint-post/complaint-post.component';

@Component({
  selector: 'app-my-network',
  templateUrl: './my-network.component.html',
  styleUrls: ['./my-network.component.css']
})
export class MyNetworkComponent implements OnInit, OnDestroy {

  posts: Post = new Post;
  page = 1;
  profile = '';
  profileEvent: any;

  img_clap = '../../../../../assets/svg/clap.svg';
  img_clap_roxo = '../../../../../assets/svg/clap-roxo.svg';

  reported = false;

  user: any;

  constructor(
    private postService: PostService,
    private utilitiesService: UtilitiesService,
    private dialog: MatDialog,
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.getUserLogged();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    const pos = document.body.scrollHeight;
    const max = window.innerHeight + window.scrollY;

    if (pos === max) {
      if (this.posts.page < this.posts.lastPage) {
        this.appendPosts();
      }
    }
  }

  openDialog(post, event) {
    this.dialog.open(ComplaintPostComponent, {
     data: {
       post,
       event
     },
     maxWidth: '580px',
     width: '100%'
    });
  }

  getMyNetwork() {
    this.profileEvent = this.postService.profile.subscribe(
      Response => {
        this.page = 1;
        this.profile = Response;
        this.postService.getMyNetwork(this.profile, this.page).subscribe(
          res => {
            this.posts = this.checkIfOwner(res);
            // console.log('Post minha rede:', this.posts);
          },
          error => {
            console.log('Usuário não possui nenhum interesse', error.error);
          });
      },
      error => {
        console.log('Erro no profileEvent: ', error.error);
      });

    this.postService.getMyNetwork(this.profile, this.page).subscribe(Response => {
      this.posts = this.checkIfOwner(Response);
      // console.log('Post minha rede:', Response);
    },
      error => {
        console.log('Usuário não possui nenhum interesse', error.error);
      });

    this.page++;
  }

  checkIfOwner(response: Post): any {
    const newResponse = {
      data: [],
      lastPage: response.lastPage,
      perPage: response.perPage,
      total: response.total,
      page: response.page
    };
    for (const post of response.data) {
      if (post.user_id === this.user.id) {
        post.owner = true;
      } else {
        post.owner = false;
      }
      newResponse.data.push(post);
    }
    return newResponse;
  }

  appendPosts() {
    this.profileEvent = this.postService.profile.subscribe(Response => {
      this.profile = Response;
      console.log('profile:', this.profile);
      this.postService.getMyNetwork(this.profile, this.page).subscribe(res => {
        res = this.checkIfOwner(res);
        this.posts.total = res.total,
          this.posts.page = res.page,
          this.posts.perPage = res.perPage,
          this.posts.lastPage = res.lastPage;
        this.posts.data = [...this.posts.data, ...res.data];
      },
        error => {
          console.log('Usuário não possui nenhum interesse', error.error);
        });
    });

    this.postService.getMyNetwork(this.profile, this.page).subscribe(Response => {
      this.posts = Response;
      console.log('Post minha rede:', this.posts);
    },
      error => {
        console.log('Usuário não possui nenhum interesse', error.error);
      });

    this.page++;
  }

  ngOnDestroy(): void {
    this.profileEvent.unsubscribe();
  }

  likePost(id) {
    this.postService.likePost(id).subscribe(Response => {
      const message = Response?.message;
      const index = this.posts.data.map(el => el.id).indexOf(id);
      this.posts.data[index].liked = !this.posts.data[index].liked;
      // this.utilitiesService.openSnackBar(`${message}`);
    }, error => {
      this.utilitiesService.openSnackBar(`${error.error}`);
    });
  }

  // reportPost(id, event) {
  //   event.stopPropagation();

  //   this.postService.reportPost(id).subscribe(
  //     response => {
  //       this.reported = true;
  //       setTimeout(() => {
  //         document.getElementById('dropdown').click();
  //       }, 1500);
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  sharePost(id) {
    this.utilitiesService.idPostShare = id;
    this.dialog.open(ShareExternallyComponent);
  }

  gotoProfile(post) {
    // console.log('POST>>>', post);
    post.author === 'user' ? this.router.navigate([`/perfil/${post?.authorUser?.id}`]) : '';
    post.author === 'event' ? this.router.navigate([`/evento/${post?.authorEvent?.id}`]) : '';
    post.author === 'project' ? this.router.navigate([`/projeto/${post?.authorProject?.id}`]) : '';
    post.author === 'organization' ? this.router.navigate([`/entidade/${post?.authorOrganization?.id}`]) : '';
  }

  removePostOfData(arr, data) {
    const index = arr.indexOf(data);
    if (index > -1) {
      arr.splice(index, 1);
    }
    return arr;
  }

  deletePost(post){
    this.postService.delete(post.id).subscribe(
      response => {
        this.removePostOfData(this.posts.data, post);
        this.utilitiesService.openSnackBar('Postagem deletada.');
      },
      error => {
        this.utilitiesService.openSnackBar(`${error.error.message}`);
      }
    );
  }

  handleDeletePost(post){
    // console.log('Esse é o post: ', post);
    if ( post.authorUser ){
      this.user.id === post.authorUser.id ? this.deletePost(post) : '';
    }
    if ( post.authorProject ){
      this.user.id === post.authorProject.user_id ? this.deletePost(post) : '';
    }
    if ( post.authorOrganization ){
      this.user.id === post.authorOrganization.user_id ? this.deletePost(post) : '';
    }
    if ( post.authorEvent ){
      this.user.id === post.authorEvent.user_id ? this.deletePost(post) : '';
    }
  }

  getUserLogged(){
    this.userService.getAccountLogged().subscribe(
      response => {
        this.user = response;
        // console.log('', this.user);
      },
      error => {
        this.utilitiesService.openSnackBar(`${error.error.message}`);
      },
      () => {
        this.getMyNetwork();
      }
    );
  }

}
