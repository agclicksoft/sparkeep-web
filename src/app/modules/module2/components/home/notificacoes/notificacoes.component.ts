import { UtilitiesService } from './../../../../../core/services/utilities.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-notificacoes',
  templateUrl: './notificacoes.component.html',
  styleUrls: ['./notificacoes.component.css']
})
export class NotificacoesComponent implements OnInit {

  notification: any = [];
  page = 1;
  id: number;

  constructor(
    private notificationService: NotificationService,
    private utilsService: UtilitiesService,
  ) {
    this.id = JSON.parse(sessionStorage.getItem('user')).id;
  }

  ngOnInit(): void {
    this.getNotifications();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    let pos = document.body.scrollHeight;
    let max = window.innerHeight + window.scrollY;
    // console.log('POS: ', pos, ' MAX: ', max );
    if (pos === max) {
      // console.log("NOTIFICAÇÕES", this.notification);
      if (this.page <= this.notification.lastPage) {
        this.appendNotifications();
      }
    }
  }

  getNotifications() {
    this.notificationService.getAllNotifications(this.id, this.page).subscribe(
      response => {
        this.notification = response;
        console.log('Response notificação: ', response);
        this.page++;
      },
      error => {
        console.log(error);
      }
    );
  }

  handleGetProfile(data): string {
    if (data.event_send_id) {
      return `/evento/${data.event_send_id}`;
    }
    if (data.organization_send_id) {
      return `/entidade/${data.organization_send_id}`;
    }
    if (data.user_send_id) {
      return `/perfil/${data.user_send_id}`;
    }
    if (data.project_send_id) {
      return `/projeto/${data.project_send_id}`;
    }
  }

  appendNotifications() {
    this.notificationService.getAllNotifications(this.id, this.page).subscribe(
      response => {
        this.notification.data = [...this.notification.data, ...response.data];
        this.page++;
      },
      error => {
        console.log(error);
      }
    );
  }

  handleAcceptAdministrator(notificacao) {
    let role;
    let role_id;
    let action = { action: 'accepted', notification_id: notificacao.notification_id };

    if (notificacao?.notification?.event_send_id) { role = 'event'; role_id = notificacao.notification.event_send_id; }
    if (notificacao?.notification?.project_send_id) { role = 'project'; role_id = notificacao.notification.project_send_id; }
    if (notificacao?.notification?.organization_send_id) { role = 'organization'; role_id = notificacao.notification.organization_send_id; }

    // console.log(notificacao);
    // console.log('Role: ', role, 'role_id: ', role_id);

    this.notificationService.handleAcceptOrRecuseAdministrator(this.id, role, role_id, action).subscribe(
      response => {
        this.utilsService.openSnackBar(`Pedido de administração aceito.`);
      },
      error => {
        this.utilsService.openSnackBar(`${error.error.message}`);
      }
    );
  }

  handleDeclineAdministrator(notificacao) {
    let role;
    let role_id;
    let action = { action: 'rejected', notification_id: notificacao.notification_id };

    if (notificacao?.notification?.event_send_id) { role = 'event'; role_id = notificacao.notification.event_send_id; }
    if (notificacao?.notification?.project_send_id) { role = 'project'; role_id = notificacao.notification.project_send_id; }
    if (notificacao?.notification?.organization_send_id) { role = 'organization'; role_id = notificacao.notification.organization_send_id; }

    this.notificationService.handleAcceptOrRecuseAdministrator(this.id, role, role_id, action).subscribe(
      response => {
        this.utilsService.openSnackBar(`Pedido de administração recusado.`);
      },
      error => {
        this.utilsService.openSnackBar(`${error.error.message}`);
      }
    );
  }
}
