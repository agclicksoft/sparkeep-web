import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProjectService } from 'src/app/core/services/project.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meus-projetos',
  templateUrl: './meus-projetos.component.html',
  styleUrls: ['./meus-projetos.component.css']
})
export class MeusProjetosComponent implements OnInit {

  userSession = JSON.parse(sessionStorage.getItem('user')) || JSON.parse(localStorage.getItem('user'));

  ownerProjects: Array<any>;
  administratorsProjects: Array<any>;

  constructor(
    private projectService: ProjectService,
    private ngxSpinner: NgxSpinnerService,
    private utilitiesService: UtilitiesService,
  ) { }

  ngOnInit(): void {
    this.getAllProjects();
  }

  getAllProjects() {
    this.ngxSpinner.show();
    this.projectService.getMyProjects(this.userSession.id).subscribe(Response => {
      // console.log('Response Owner: ', Response);
      this.ownerProjects = Response.ownerProjects;
      this.administratorsProjects = Response.administratorsProjects;
    }, error => {
      this.ngxSpinner.hide();
      this.utilitiesService.openSnackBar('Não foi possivel buscar os projetos!');
    }, () => {
      this.ngxSpinner.hide();
      this.ownerProjects = this.reduceDescripion(this.ownerProjects);
      this.administratorsProjects = this.reduceDescripion(this.administratorsProjects);
    });
  }

  reduceDescripion(projects) {
    // for (let item of projects) {
    //   if (item.description !== null) {
    //     item.description = item.description.substring(0, 22) + '...';
    //   } else {
    //     item.description = '...';
    //   }
    // }
    return projects;
  }

}
