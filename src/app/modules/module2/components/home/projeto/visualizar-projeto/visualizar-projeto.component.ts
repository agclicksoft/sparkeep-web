import { ComplaintPostComponent } from 'src/app/shared/template/complaint-post/complaint-post.component';
import { OrganizationService } from 'src/app/core/services/organization.service';
import { MyPartnersDialogComponent } from 'src/app/shared/template/my-partners-dialog/my-partners-dialog.component';
import { UserService } from './../../../../../../core/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Project } from '../../../../../../shared/models/project.model';
import { ProjectService } from 'src/app/core/services/project.service';
import { Post } from 'src/app/shared/models/post.model';
import { PostService } from 'src/app/core/services/post.service';
import { UtilitiesService } from '../../../../../../core/services/utilities.service';


@Component({
  selector: 'app-visualizar-projeto',
  templateUrl: './visualizar-projeto.component.html',
  styleUrls: ['./visualizar-projeto.component.css']
})
export class VisualizarProjetoComponent implements OnInit{

  id: number;
  isLoading = true;

  project: Project = new Project();

  myProjects;
  myOrganizations;

  posts: Post = new Post();
  page = 1;

  reported = false;

  img_clap = 'assets/svg/clap.svg';
  img_clap_roxo = 'assets/svg/clap-roxo.svg';

  ownerMode = false;
  userId;
  userSession = JSON.parse(sessionStorage.getItem('user'));

  routeId;

  constructor(
    private projectService: ProjectService,
    private postService: PostService,
    private utilitiesService: UtilitiesService,
    private organizationService: OrganizationService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
  ) {
    // this.id = parseInt(this.activatedRoute.snapshot.params.id);

    this.routeId = this.activatedRoute.params.subscribe(params => {
      this.id = (params.id);
      this.ngOnInit();
    });

    this.userId = JSON.parse(sessionStorage.getItem('user')).id;
  }

  ngOnInit(): void {
    this.getMyOrganizations();
    this.getMyProjects();
    this.getProject();
    this.getPosts();
  }

  getMyProjects() {
    // console.log(this.userId);
    this.projectService.getMyProjects(this.userId).subscribe(
      response => {
        this.myProjects = response;
      },
      error => {
        this.utilitiesService.openSnackBar('Oops, obtivemos um error.');
      }
    );
  }

  getMyOrganizations(){
    this.organizationService.getAllUserOrganizations(this.userId).subscribe(
      response => {
        this.myOrganizations = response;
      },
      error => {
        this.utilitiesService.openSnackBar('Oops, obtivemos um error.');
      },
      () => {
        this.verifyOwnedProject();
      }
    );
  }

  getProject() {
    this.projectService.getProject(this.id).subscribe(
      response => {
        this.project = response;
        console.log('VALOR response Project: ', this.project);
        // document.getElementsByClassName('bg-img')[0].setAttribute('style', `background-image: url(${this.project.image_url})`);
      },
      error => {
        console.log('GET PROJECT ERROR: ', error);
      },
      () => {
      }
    );
  }

  getPosts() {
    this.projectService.getPosts(this.id, this.page).subscribe(
      response => {
        this.posts = response;
      },
      error => {
        console.log(error);
      }
    );
  }

  likePost(id){
    this.postService.likePost(id).subscribe( Response => {
      const message = Response?.message;
      const index = this.posts.data.map(el => el.id).indexOf(id);
      this.posts.data[index].liked = !this.posts.data[index].liked;
      if (this.posts.data[index].liked){
        this.posts.data[index].__meta__.total_inspireds = parseInt(this.posts.data[index].__meta__.total_inspireds) + 1;
      }else{
        this.posts.data[index].__meta__.total_inspireds = parseInt(this.posts.data[index].__meta__.total_inspireds) - 1;
      }
    }, error => {
      this.utilitiesService.openSnackBar(`${error.error}`);
    });
  }

  // reportPost(id, event) {
  //   event.stopPropagation();

  //   this.postService.reportPost(id).subscribe(
  //     response => {
  //       this.reported = true;
  //       setTimeout(() => {
  //         document.getElementById('dropdown').click();
  //       }, 1500);
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  openDialogReport(post, event) {
    this.dialog.open(ComplaintPostComponent, {
     data: {
       post,
       event
     },
     maxWidth: '580px',
     width: '100%'
    });
  }

  customMask() {
    return '(00) 0000-0000';
  }

  openDialog() {
    const receiver = {
      id: this.id,
      role: 'project',
    };

    if (this.ownerMode === false) {
      // Emite os valores necessários para executar a função da parceria.
      this.utilitiesService.partnershipReceiverValue = receiver;
      this.dialog.open(MyPartnersDialogComponent, {
        data: {
          shortages: this.project.shortages,
          maintenance_times: this.project.maintenance_times,
          has_shortages: this.project.has_shortages,
        }
      });
    }
  }

  verifyOwnedProject() {
    this.ownerMode = false;
    if (this.project.user_id !== null){
      this.project.user_id === this.userId ? this.ownerMode = true : this.ownerMode = false;
    }
    if (this.project.organization_id !== null){
      for (const item of this.myOrganizations?.ownerOrganizations) {
        this.project.organization_id === item.id ? this.ownerMode = true : '';
      }
    }
    // projectOwnedId == userId ? this.ownerMode = true : this.ownerMode = false;
  }

  handleTypeOwnerAdmin(project: Project): string {
    if (project.organization_id !== null) { return 'Organization'; }
    if (project.user_id !== null) { return 'User'; }
  }
}
