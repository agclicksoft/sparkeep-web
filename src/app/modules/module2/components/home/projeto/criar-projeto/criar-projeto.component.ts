import { MatDialog } from '@angular/material/dialog';
import { ModalDeleteUserComponent } from './../../../../../../shared/template/modal-delete-user/modal-delete-user.component';
import { LocationService } from './../../../../../../core/services/location.service';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ProjectService } from 'src/app/core/services/project.service';
import { NgxSpinnerService } from 'ngx-spinner';

import Swal from 'sweetalert2';
import { OrganizationService } from 'src/app/core/services/organization.service';

@Component({
  selector: 'app-criar-projeto',
  templateUrl: './criar-projeto.component.html',
  styleUrls: ['./criar-projeto.component.css'],
})
export class CriarProjetoComponent implements OnInit {
  id: number;
  user: any;
  isLoading = false;
  isSubmited = false;
  // tslint:disable-next-line: variable-name
  profile_image = 'assets/img/user.svg';
  projectImage: FormData = new FormData();
  imgURL = 'assets/svg/camera_alt.svg';

  owner;

  projectForm: FormGroup;

  locationState;
  locationCity;

  myOrganizations: Array<any> = [];
  administrators: Array<any> = [];
  showAdministratorsModal = false;
  states: Array<any> = [];
  editMode = false;
  userSession =
    JSON.parse(sessionStorage.getItem('user')) ||
    JSON.parse(localStorage.getItem('user'));

  constructor(
    private fb: FormBuilder,
    private projectService: ProjectService,
    private organizationService: OrganizationService,
    private spinner: NgxSpinnerService,
    private locationService: LocationService,
    private utilitiesService: UtilitiesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) {
    this.id = this.activatedRoute.snapshot.params.id;

    if (this.id) {
      this.editMode = true;
    }
  }

  ngOnInit(): void {
    // this.getStates();
    this.projectForm = this.fb.group({
      owner: ['', Validators.required],
      user_id: [''],
      organization_id: [''],
      event_id: [''],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(250)]],
      local: ['', [Validators.required]],
      telephone: ['', [Validators.required]],
      cellphone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      address_state: ['', [Validators.required]],
      address_city: ['', Validators.required],
      address_complement: [''],
      address_neighborhood: [''],
      address_street: [''],
      show_telephone: [''],
      show_cellphone: [''],
      image_url: ['assets/img/img_project_default.png'],
      kind_of_help: [''],
      administrators: [[]],
      passions: [[], [Validators.required]],
      maintenance_times: [[]],
      shortages: [[]],
      has_shortages: [false],
      youtube: [''],
      facebook: [''],
      instagram: [''],
      twitter: [''],
      site: [''],
    });

    this.getAllStates();
    this.getMyOrganizations();
    this.user = sessionStorage.getItem('user') || localStorage.getItem('user');
    this.user = JSON.parse(this.user);
    if (this.id) {
      this.getProject();
    }

    // console.log(this.user);
  }

  getProject() {
    this.projectService.getProject(this.id).subscribe(
      (response) => {
        this.projectForm.patchValue(response);

        this.projectForm.controls.administrators.patchValue(null);
        this.administrators = response.administrators;

        if (response.image_url) {
          this.imgURL = response.image_url;
        }

        this.onStateOptionSelected(response.address_state);

        // console.log(this.projectForm.value);
      },
      (error) => {
        console.log(error);
      },
      () => {
        this.setOwner();
      }
    );
  }

  setOwner() {
    if (this.projectForm.get('user_id').value) {
      this.projectForm
        .get('owner')
        .patchValue(`user_${this.projectForm.get('user_id').value}`);
    }
    if (this.projectForm.get('organization_id').value) {
      this.projectForm
        .get('owner')
        .patchValue(
          `organization_${this.projectForm.get('organization_id').value}`
        );
    }
    if (this.projectForm.get('event_id').value) {
      this.projectForm
        .get('owner')
        .patchValue(`event_${this.projectForm.get('event_id').value}`);
    }
    this.handleOwner(this.projectForm.get('owner').value);
  }

  getMyOrganizations() {
    this.organizationService
      .getAllUserOrganizations(this.userSession.id)
      .subscribe(
        (response) => {
          this.myOrganizations = [
            ...response.administratorsOrganizations,
            ...response.ownerOrganizations,
          ];
          // console.log(response);
        },
        (error) => {
          console.log(error);
        },
        () => {
          this.handleShowOwner();
        }
      );
  }

  handleOwner(value) {
    const role = value.split('_')[0];
    // tslint:disable-next-line: radix
    const id = parseInt(value.split('_')[1]);

    this.projectForm.patchValue({
      user_id: null,
      organization_id: null,
      event_id: null,
    });

    switch (role) {
      case 'user':
        this.profile_image =
          this.userSession.image_url !== null
            ? this.userSession.image_url
            : 'assets/img/user.svg';
        this.projectForm.get('user_id').patchValue(id);
        break;
      case 'organization':
        this.setOrganizationImg(id);
        this.projectForm.get('organization_id').patchValue(id);
        break;
    }
    // console.log(this.projectForm.value);
  }

  setOrganizationImg(id) {
    for (const organization of this.myOrganizations) {
      // tslint:disable-next-line: radix
      if (organization.id === parseInt(id)) {
        this.profile_image = organization.image_url;
      }
    }
  }

  attachmentPicker() {
    document.getElementById('attachmentBtn').click();
  }

  onAttachmentSelect(event) {
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgURL = reader.result as string;
    };

    this.projectImage = new FormData();
    this.projectImage.append('attachment', event.target.files[0]);
  }

  toggleModal() {
    const modal = document.querySelector('app-add-administrators');
    modal.classList.contains('d-none')
      ? modal.classList.remove('d-none')
      : modal.classList.add('d-none');
  }

  onPassionChange(event) {
    event.length > 0
      ? this.projectForm.controls.passions.patchValue(event.map((el) => el.id))
      : this.projectForm.controls.passions.patchValue([]);
  }

  onShortageChange(event) {
    event.length > 0
      ? this.projectForm.controls.shortages.patchValue(event.map((el) => el.id))
      : this.projectForm.controls.shortages.patchValue([]);
  }

  onMaintenanceTimeChange(event) {
    // this.projectForm.controls.maintenance_time.patchValue(event);
    event.length > 0
      ? this.projectForm.controls.maintenance_times.patchValue(
        event.map((el) => el.id)
      )
      : this.projectForm.controls.maintenance_times.patchValue([]);
  }

  onKindOfHelpChange(event) {
    this.projectForm.controls.kind_of_help.patchValue(event);
  }

  administratorsControl(event) {
    const arrayAdmin = [...event, ...this.administrators];
    this.administrators = [...new Set(arrayAdmin)];
  }

  deleteAdministrator(index) {
    this.administrators.splice(index, 1);
  }

  onSubmit() {
    // console.log(this.projectForm.value);
    this.isSubmited = true;

    if (this.projectForm.status === 'VALID') {
      // this.changeOwner();
      this.spinner.show();
      this.isLoading = true;
      // this.projectForm.controls.user_id.patchValue(this.user.id);

      if (
        this.projectForm.value.shortages.length > 0 &&
        this.projectForm.value.shortages[0].id
      ) {
        this.projectForm.controls.shortages.patchValue(
          this.projectForm.value.shortages.map((el) => el.id)
        );
      }

      if (
        this.projectForm.value.maintenance_times.length > 0 &&
        this.projectForm.value.maintenance_times[0].id
      ) {
        this.projectForm.controls.maintenance_times.patchValue(
          this.projectForm.value.maintenance_times.map((el) => el.id)
        );
      }

      if (
        this.projectForm.value.passions.length > 0 &&
        this.projectForm.value.passions[0].id
      ) {
        this.projectForm.controls.passions.patchValue(
          this.projectForm.value.passions.map((el) => el.id)
        );
      }

      this.projectForm.controls.administrators.patchValue(null);
      const adminIds = this.administrators.map((el) => el.id);
      this.projectForm.controls.administrators.patchValue(adminIds);

      if (this.editMode) {
        this.projectService.update(this.id, this.projectForm.value).subscribe(
          (response) => {
            this.uploadAttachments(response.id, 'atualizado');
          },
          (error) => {
            Swal.fire('Oops...', `${error.error.message}`, 'error');
            this.spinner.hide();
            this.isLoading = false;
          }
        );
      } else {
        this.projectService.store(this.projectForm.value).subscribe(
          (response) => {
            this.uploadAttachments(response.id, 'cadastrado');
          },
          (error) => {
            Swal.fire('Oops...', `${error.error.message}`, 'error');
            this.spinner.hide();
            this.isLoading = false;
          }
        );
      }
    } else {
      this.utilitiesService.openSnackBar(
        'Formulário inválido, insira corretamente os valores.'
      );
    }
  }

  uploadAttachments(id, status) {
    this.projectService.uploadAttachment(id, this.projectImage).subscribe(
      (response) => {
        this.utilitiesService.myProjectsReload.emit(response);
        this.utilitiesService.openSnackBar(`Projeto ${status} com sucesso`);
        this.spinner.hide();
        this.isLoading = false;
      },
      (error) => {
        Swal.fire(
          'Oops...',
          `Seu projeto foi ${status}, mas tivemos um erro ao salvar a imagem do Projeto! Tente novamente mais tarde.`,
          'error'
        );
        this.spinner.hide();
        this.isLoading = false;
      },
      () => {
        this.router.navigate(['/projetos']);
      }
    );
  }

  handleShowOwner() {
    this.owner = null;
    if (this.editMode) {
      this.projectForm.get('owner').clearValidators();
      this.projectForm.controls.owner.patchValue(null);
      if (this.projectForm.get('user_id').value !== null) {
        this.owner = this.userSession.name;
        this.userSession.image_url !== null
          ? (this.profile_image = this.userSession.image_url)
          : (this.profile_image = 'assets/img/user.svg');
      }
      if (this.projectForm.get('organization_id').value !== null) {
        // console.log('Minhas organizações: ', this.myOrganizations);
        for (const item of this.myOrganizations) {
          // console.log('Item: ', item);
          if (item.id === this.projectForm.get('organization_id').value) {
            this.owner = item.name;
            item.image_url !== null
              ? (this.profile_image = item.image_url)
              : (this.profile_image = 'assets/img/user.svg');
          }
        }
      }
    }
  }

  handleHasShortages(value) {
    if (value) {
      this.projectForm.get('shortages').setValidators(Validators.required);
      this.projectForm
        .get('maintenance_times')
        .setValidators(Validators.required);
      this.projectForm.get('shortages').updateValueAndValidity();
      this.projectForm.get('maintenance_times').updateValueAndValidity();
      // console.log('TEM CARENCIA: ', this.projectForm.controls);
    } else {
      this.projectForm.get('shortages').patchValue('');
      this.projectForm.get('shortages').clearValidators();
      this.projectForm.get('shortages').updateValueAndValidity();
      this.projectForm.get('maintenance_times').patchValue('');
      this.projectForm.get('maintenance_times').clearValidators();
      this.projectForm.get('maintenance_times').updateValueAndValidity();
      // console.log('NÃO TEM CARENCIA: ', this.projectForm.controls);
    }
  }

  getAllStates() {
    this.locationService.getStates().subscribe((response) => {
      this.locationState = response;
    });
  }

  onStateOptionSelected(event) {
    let idState;
    for (const item of this.locationState) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityByStateId(idState);
        break;
      }
    }
  }

  getCityByStateId(id) {
    this.locationService.getCity(id).subscribe((response) => {
      this.locationCity = response;
    });
  }

  deleteProject() {
    this.dialog.open(ModalDeleteUserComponent, {
      data: {
        id: this.id,
        type: 'Project',
      },
    });
  }

  checkAndSetValidation() {
    if (this.projectForm.get('email').value) {
      this.projectForm
        .get('email')
        .setValidators([Validators.required, Validators.email]);
      this.projectForm.get('telephone').clearValidators();
      this.projectForm.get('cellphone').clearValidators();
    }
    if (this.projectForm.get('telephone').value) {
      this.projectForm.get('telephone').setValidators([Validators.required]);
      this.projectForm.get('email').clearValidators();
      this.projectForm.get('cellphone').clearValidators();
    }
    if (this.projectForm.get('cellphone').value) {
      this.projectForm.get('cellphone').setValidators([Validators.required]);
      this.projectForm.get('email').clearValidators();
      this.projectForm.get('telephone').clearValidators();
    }
    this.projectForm.get('cellphone').updateValueAndValidity();
    this.projectForm.get('email').updateValueAndValidity();
    this.projectForm.get('telephone').updateValueAndValidity();
  }
}
