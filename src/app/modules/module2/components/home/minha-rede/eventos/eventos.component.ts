import { MyNetworkService } from './../../../../../../core/services/my-network.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {

  search = '';
  searchInMyNetwork: any;
  events: any;
  page: number = 1;

  constructor(
    private myNetworkService: MyNetworkService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.getAllEvent('', this.page);
    this.handleEventSearch();
  }

  getAllEvent(filtro, page){
    let events;
    this.ngxSpinner.show();
    this.myNetworkService.getEvents(filtro, page).subscribe(response => {
      events = response;
      events.data = this.reduceDescription(events.data);
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      this.events = events;
    });
  }

  reduceDescription(events){
    for (const item of events) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return events;
  }

  handleEventSearch(){
    this.searchInMyNetwork = this.utilitiesService.searchInMyNetwork.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT PROJETOS: ', response );
      this.getAllEvent(this.search, 1);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  ngOnDestroy(){
    this.searchInMyNetwork.unsubscribe();
  }

  changePage(page: number) {
    if (page <= 1) {
      this.page = 1;
    } else {
      this.page = page;
    }
    this.getAllEvent(this.search, this.page);
  }
}
