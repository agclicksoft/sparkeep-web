import { MyNetworkService } from './../../../../../../core/services/my-network.service';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { UserService } from 'src/app/core/services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from './../../../../../../shared/models/user.model';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-pessoas',
  templateUrl: './pessoas.component.html',
  styleUrls: ['./pessoas.component.css']
})
export class PessoasComponent implements OnInit, OnDestroy {

  search = '';
  searchInMyNetwork: any;
  users: User = new User();
  page = 1;
  // TODO: CHANGE THE PAGE AND APPEND USERS

  constructor(
    private userService: UserService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
    private myNetworkService: MyNetworkService
  ) { }

  ngOnInit(): void {
    this.getAllUsers('', this.page);
    this.handlePeopleSearch();
  }

  getAllUsers(filtro, page){
    let usuarios;
    this.ngxSpinner.show();
    this.myNetworkService.getUsers(filtro, page).subscribe(response => {
      usuarios = response;
      this.users = response;
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      this.users = this.reduceDescription(usuarios);
    });
  }

  reduceDescription(usuarios){
    for (const item of usuarios.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return usuarios;
  }

  handlePeopleSearch(){
    this.searchInMyNetwork = this.utilitiesService.searchInMyNetwork.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT PESSOAS: ', response );
      this.getAllUsers(this.search, 1);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  ngOnDestroy(){
    this.searchInMyNetwork.unsubscribe();
  }

  changePage(page: number) {
    if (page <= 1) {
      this.page = 1;
    } else {
      this.page = page;
    }
    this.getAllUsers(this.search, this.page);
  }
}
