import { MyNetworkService } from './../../../../../../core/services/my-network.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { ProjectService } from './../../../../../../core/services/project.service';
import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/shared/models/project.model';

@Component({
  selector: 'app-projetos',
  templateUrl: './projetos.component.html',
  styleUrls: ['./projetos.component.css']
})
export class ProjetosComponent implements OnInit {

  search = '';
  searchInMyNetwork: any;
  projects: Project = new Project;
  page: number = 1;
  //TODO: CHANGE PAGE AND APPEND PROJECTS
  constructor(
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
    private myNetworkService: MyNetworkService
  ) { }

  ngOnInit(): void {
    this.getAllProject('', this.page);
    this.handleProjectSearch();
  }

  getAllProject(filtro, page){
    let projects;
    this.ngxSpinner.show();
    this.myNetworkService.getProjects(filtro, page).subscribe(response => {
      projects = response;
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      this.projects = this.reduceDescription(projects);
    });
  }

  reduceDescription(projects){
    for (const item of projects.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return projects;
  }

  handleProjectSearch(){
    this.searchInMyNetwork = this.utilitiesService.searchInMyNetwork.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT PROJETOS: ', response );
      this.getAllProject(this.search, 1);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  ngOnDestroy(){
    this.searchInMyNetwork.unsubscribe();
  }

  changePage(page: number) {
    if (page <= 1) {
      this.page = 1;
    } else {
      this.page = page;
    }
    this.getAllProject(this.search, this.page);
  }

}
