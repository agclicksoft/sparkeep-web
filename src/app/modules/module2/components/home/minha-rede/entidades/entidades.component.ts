import { ActivatedRoute } from '@angular/router';
import { MyNetworkService } from './../../../../../../core/services/my-network.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { OrganizationService } from 'src/app/core/services/organization.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-entidades',
  templateUrl: './entidades.component.html',
  styleUrls: ['./entidades.component.css']
})
export class EntidadesComponent implements OnInit, OnDestroy {

  search = null;
  searchInMyNetwork: any;
  organizations: any;
  page = 1;

  constructor(
    private organizationService: OrganizationService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
    private myNetworkService: MyNetworkService,
  ) {}

  ngOnInit(): void {
    this.getAllOrganizations('', this.page);
    this.handlePeopleSearch();
  }

  getAllOrganizations(filtro, page){
    let organizations;
    this.ngxSpinner.show();
    this.myNetworkService.getOrganizations(filtro, page).subscribe(response => {
      organizations = response;
      console.log(response);
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      this.organizations = this.reduceDescription(organizations);
    });
  }

  reduceDescription(organizations){
    for (const item of organizations.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return organizations;
  }

  handlePeopleSearch(){
    this.searchInMyNetwork = this.utilitiesService.searchInMyNetwork.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT ENTIDADES: ', response );
      this.getAllOrganizations(this.search, 1);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  ngOnDestroy(){
    this.searchInMyNetwork.unsubscribe();
  }

  changePage(page) {
    if (page <= 1) {
      this.page = 1;
    } else {
      this.page = page;
    }
    this.getAllOrganizations(this.search, this.page);
  }

}
