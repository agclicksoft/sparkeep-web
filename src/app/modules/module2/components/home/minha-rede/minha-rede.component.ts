import { UtilitiesService } from './../../../../../core/services/utilities.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-minha-rede',
  templateUrl: './minha-rede.component.html',
  styleUrls: ['./minha-rede.component.css']
})
export class MinhaRedeComponent implements OnInit {

  search = null;

  constructor(
    private utilitiesService: UtilitiesService,
  ) { }

  ngOnInit(): void {
  }

  handleSearch(event){
    if (event){
      // event.preventDefault();
      this.utilitiesService.searchInMyNetwork.emit(event);
    }else{
      this.utilitiesService.searchInMyNetwork.emit('');
    }
  }
}
