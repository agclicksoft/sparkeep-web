import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { OrganizationService } from './../../../../../../core/services/organization.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-pes-entidades',
  templateUrl: './pes-entidades.component.html',
  styleUrls: ['./pes-entidades.component.css']
})
export class PesEntidadesComponent implements OnInit, OnDestroy {

  search = '';
  globalSearch: any;
  organizations: any;
  page = 1;

  constructor(
    private organizationService: OrganizationService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.getAllOrganizations('', this.page);
    this.handlePeopleSearch();
  }

  getAllOrganizations(filtro, page){
    let organizations;
    this.ngxSpinner.show();
    this.organizationService.getAllOrganizations(filtro, page).subscribe(response => {
      organizations = response;
      console.log(response);
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      organizations = this.reduceDescription(organizations);
      organizations = this.reduceName(organizations);
      this.organizations = organizations;
    });
  }

  reduceDescription(organizations){
    for (const item of organizations.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return organizations;
  }

  reduceName(organizations){
    for (const item of organizations.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    return organizations;
  }

  handlePeopleSearch(){
    this.globalSearch = this.utilitiesService.globalSearch.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT ENTIDADES: ', response );
      this.getAllOrganizations(this.search, 1);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  ngOnDestroy(){
    this.globalSearch.unsubscribe();
  }

  changePage(page: number) {
    console.log(page);
    this.page = page;
    this.getAllOrganizations(this.search, this.page);
  }
}
