import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesEntidadesComponent } from './pes-entidades.component';

describe('PesEntidadesComponent', () => {
  let component: PesEntidadesComponent;
  let fixture: ComponentFixture<PesEntidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesEntidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesEntidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
