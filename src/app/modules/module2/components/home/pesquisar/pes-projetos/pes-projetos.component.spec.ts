import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesProjetosComponent } from './pes-projetos.component';

describe('PesProjetosComponent', () => {
  let component: PesProjetosComponent;
  let fixture: ComponentFixture<PesProjetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesProjetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesProjetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
