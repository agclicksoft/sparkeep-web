import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { ProjectService } from './../../../../../../core/services/project.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Project } from 'src/app/shared/models/project.model';

@Component({
  selector: 'app-pes-projetos',
  templateUrl: './pes-projetos.component.html',
  styleUrls: ['./pes-projetos.component.css']
})
export class PesProjetosComponent implements OnInit, OnDestroy {

  search = '';
  globalSearch: any;
  projects: Project = new Project;
  page: number = 1;
  //TODO: CHANGE PAGE AND APPEND PROJECTS;

  constructor(
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.getAllProject('', this.page);
    this.handleProjectSearch();
  }

  getAllProject(filtro, page){
    let projects;
    this.ngxSpinner.show();
    this.projectService.getAllProjects(filtro, page).subscribe(response => {
      projects = response;
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      projects = this.reduceDescription(projects);
      projects = this.reduceName(projects);
      setTimeout(() => {
        this.projects = projects;
      }, 300);
    });
  }

  reduceDescription(projects){
    // console.log('PROJECTS:', projects)
    for (const item of projects.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return projects;
  }

  reduceName(projects){
    for (const item of projects.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    return projects;
  }

  handleProjectSearch(){
    this.globalSearch = this.utilitiesService.globalSearch.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT PROJETOS: ', response );
      this.getAllProject(this.search, this.page);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  changePage(page: number) {
    this.page = page;
    this.getAllProject(this.search, this.page);
  }

  ngOnDestroy(){
    this.globalSearch.unsubscribe();
  }

}
