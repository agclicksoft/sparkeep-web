import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from './../../../../../../core/services/user.service';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-pes-pessoas',
  templateUrl: './pes-pessoas.component.html',
  styleUrls: ['./pes-pessoas.component.css']
})
export class PesPessoasComponent implements OnInit, OnDestroy {


  search = '';
  globalSearch: any;
  users: User = new User;
  page = 1;
  // TODO: CHANGE PAGE AND APPEND USERS

  constructor(
    private userService: UserService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.getAllUsers('', this.page);
    this.handlePeopleSearch();
  }

  getAllUsers(filtro, page){
    let usuarios;
    this.ngxSpinner.show();
    this.userService.getAllUsers(filtro, page).subscribe(response => {
      console.log(response);
      usuarios = response;
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      this.users = this.reduceDescription(usuarios);
    });
  }

  reduceDescription(usuarios){
    for (const item of usuarios?.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return usuarios;
  }

  handlePeopleSearch(){
    this.globalSearch = this.utilitiesService.globalSearch.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT PESSOAS: ', response );
      this.getAllUsers(this.search, 1);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  changePage(page: number) {
    this.page = page;
    this.getAllUsers(this.search, this.page);
  }

  ngOnDestroy(){
    this.globalSearch.unsubscribe();
  }
}
