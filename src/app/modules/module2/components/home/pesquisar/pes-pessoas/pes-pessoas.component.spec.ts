import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesPessoasComponent } from './pes-pessoas.component';

describe('PesPessoasComponent', () => {
  let component: PesPessoasComponent;
  let fixture: ComponentFixture<PesPessoasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesPessoasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesPessoasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
