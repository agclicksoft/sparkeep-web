import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { SearchService } from './../../../../../../core/services/search.service';
import { LocationService } from './../../../../../../core/services/location.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pes-localizacao',
  templateUrl: './pes-localizacao.component.html',
  styleUrls: ['./pes-localizacao.component.css']
})
export class PesLocalizacaoComponent implements OnInit {

  // Dados para pesquisa.
  search = '';
  globalSearch: any; // Recebe o eventEmitter

  // Localização do IBGE
  locationState;
  locationCity;

  // Valores selecionados
  address = {
    address_country: '',
    address_state: '',
    address_city: '',
  };

  // Valores recebidos
  objectLocalization;

  // Páginas de pesquisa
  userPage = 1;
  eventPage = 1;
  projectPage = 1;
  organizationPage = 1;

  constructor(
    private locationService: LocationService,
    private searchService: SearchService,
    private utilService: UtilitiesService,
  ) { }

  ngOnInit(): void {
    this.getAllStates();
    this.handleLocationSearch();
  }

  onChangeCountry(event) {
    this.address.address_country = event;
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }

  getAllStates() {
    this.locationService.getStates().subscribe(response => {
      this.locationState = response.map((uf) => {
        const state = {
          sigla: uf.sigla,
          nome: uf.nome,
          id: uf.id
        };
        return state;
      }
      ).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      // console.log('Localidades Estados ->', this.locationState);
    });
  }

  getCityByStateId(id) {
    this.locationService.getCity(id).subscribe(response => {
      this.locationCity = response;
      // console.log('location City: ', this.locationCity);
    });
  }

  onStateOptionSelected(event) {
    let idState;
    this.address.address_state = event;
    for (const item of this.locationState) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityByStateId(idState);
        break;
      }
    }
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }

  onChangeCity(city) {
    this.address.address_city = city;
    console.log('Valores do Address: ', this.address);
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }

  getLocations(address, keyword, userPage, eventPage, projectPage, organizationPage) {
    let objectLocalization;
    this.searchService.getLocations(address, keyword, userPage, eventPage, projectPage, organizationPage).subscribe(
      response => {
        console.log(response);
        objectLocalization = response;
      }, error => {
        this.utilService.openSnackBar(`Obtivemos um erro ao buscar os dados da localização, ERRO: ${error.error.message}`);
      }, () => {
        objectLocalization = this.reduceDescription(objectLocalization);
        objectLocalization = this.reduceName(objectLocalization);
        this.objectLocalization = objectLocalization;
        // console.log('RESPOSTA DO SERV COMPONENT LOCALIZAÇÃO: ', this.objectLocalization);
      }
    );
  }

  handleLocationSearch() {
    this.globalSearch = this.utilService.globalSearch.subscribe(response => {
      this.search = response;
      // console.log('PESQUISAR NO COMPONENT LOCALIZAÇÃO: ', response);
      this.getLocations(this.address, this.search, 1, 1, 1, 1);
    }, error => {
      this.utilService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  reduceDescription(object){
    for (const item of object.users.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    for (const item of object.events.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    for (const item of object.projects.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    for (const item of object.organizations.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }

    return object;
  }

  reduceName(object){
    for (const item of object.users.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    for (const item of object.events.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    for (const item of object.organizations.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    for (const item of object.projects.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    return object;
  }

  changePageUser(event) {
    this.userPage = event.page;
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }
  changePageEvent(event) {
    this.eventPage = event.page;
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }
  changePageProject(event) {
    this.projectPage = event.page;
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }
  changePageOrganization(event) {
    this.organizationPage = event.page;
    this.getLocations(this.address, this.search, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }

}
