import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesLocalizacaoComponent } from './pes-localizacao.component';

describe('PesLocalizacaoComponent', () => {
  let component: PesLocalizacaoComponent;
  let fixture: ComponentFixture<PesLocalizacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesLocalizacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesLocalizacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
