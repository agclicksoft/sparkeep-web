import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesCarenciasComponent } from './pes-carencias.component';

describe('PesCarenciasComponent', () => {
  let component: PesCarenciasComponent;
  let fixture: ComponentFixture<PesCarenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesCarenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesCarenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
