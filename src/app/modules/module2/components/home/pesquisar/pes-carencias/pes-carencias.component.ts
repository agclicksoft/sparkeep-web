import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { SearchService } from './../../../../../../core/services/search.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { MaintenanceTypeService } from './../../../../../../core/services/maintenance-type.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-pes-carencias',
  templateUrl: './pes-carencias.component.html',
  styleUrls: ['./pes-carencias.component.css']
})
export class PesCarenciasComponent implements OnInit, OnDestroy {

  globalSearch;
  search = '';

  isLoading;

  shortages: Array<any> = [];
  shortagesForm = this.fb.group({});
  shortagesSelected = {
    shortages: []
  };

  objectShortages;

  eventPage = 1;
  projectPage = 1;

  constructor(
    private maintenanceService: MaintenanceTypeService,
    private fb: FormBuilder,
    private utilService: UtilitiesService,
    private searchService: SearchService,
  ) { }

  ngOnDestroy(): void {
    this.globalSearch.unsubscribe();
  }

  ngOnInit(): void {
    this.getAllShortages();
    this.handleShortagesSearch();
  }

  getAllShortages() {
    this.maintenanceService.getAll().subscribe(
      response => {
        for (const item of response) { this.shortagesForm.addControl(item.description, new FormControl(false)); }
        this.shortages = response;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  checkMaintenanceTypeControl() {
    const shortages = [];
    for (const i in this.shortages) {
      if (this.shortagesForm.controls[this.shortages[i].description].value) {
        shortages.push(this.shortages[i].id);
      }
    }

    this.shortagesSelected.shortages = shortages;
    // console.log('THIS SHORTAGES SELECTED: ', this.shortagesSelected);
    this.getSearchShortages(this.shortagesSelected, this.search, this.eventPage, this.projectPage);
  }

  getSearchShortages(shortages, keyword, eventPage, projectPage) {
    let objectShortages;
    this.searchService.getShortages(shortages, keyword, eventPage, projectPage).subscribe(response => {
      objectShortages = response;
      // console.log('objectShortages: ', this.objectShortages);
    }, error => {
      this.utilService.openSnackBar(`Obtivemos um error, ERRO: ${error.error.message}`);
    }, () => {
      objectShortages = this.reduceDescription(objectShortages);
      objectShortages = this.reduceName(objectShortages);
      this.objectShortages = objectShortages;
      console.log('RESPOSTA DO SERV COMPONENT objectShortages: ', this.objectShortages);
    });
  }

  handleShortagesSearch() {
    this.globalSearch = this.utilService.globalSearch.subscribe(response => {
      this.search = response;
      // console.log('PESQUISAR NO COMPONENT SHORTAGES: ', response);
      this.getSearchShortages(this.shortagesSelected, this.search, 1, 1);
    }, error => {
      this.utilService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  reduceName(object) {
    for (const item of object.events.data) {
      if (item.name !== null) {
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else {
          item.name = item.name;
        }
      } else {
        item.name = '...';
      }
    }
    for (const item of object.projects.data) {
      if (item.name !== null) {
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else {
          item.name = item.name;
        }
      } else {
        item.name = '...';
      }
    }
    return object;
  }

  reduceDescription(object){
    for (const item of object.events.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    for (const item of object.projects.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return object;
  }

  changeEventPage(event) {
    this.eventPage = event.page;
    this.getSearchShortages(this.shortagesSelected, this.search, this.eventPage, this.projectPage);
  }
  changeProjectPage(event) {
    this.projectPage = event.page;
    this.getSearchShortages(this.shortagesSelected, this.search, this.eventPage, this.projectPage);
  }

}
