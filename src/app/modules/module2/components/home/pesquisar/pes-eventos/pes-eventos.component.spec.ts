import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesEventosComponent } from './pes-eventos.component';

describe('PesEventosComponent', () => {
  let component: PesEventosComponent;
  let fixture: ComponentFixture<PesEventosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesEventosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesEventosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
