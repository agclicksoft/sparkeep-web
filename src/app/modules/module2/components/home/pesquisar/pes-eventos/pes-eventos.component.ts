import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { EventService } from './../../../../../../core/services/event.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Event } from 'src/app/shared/models/event.model';

@Component({
  selector: 'app-pes-eventos',
  templateUrl: './pes-eventos.component.html',
  styleUrls: ['./pes-eventos.component.css']
})
export class PesEventosComponent implements OnInit, OnDestroy {

  search = '';
  globalSearch: any;
  events: Event = new Event;
  page: number = 1;

  constructor(
    private eventService: EventService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.getAllEvent('', this.page);
    this.handleEventSearch();
  }

  getAllEvent(filtro, page){
    let events;
    this.ngxSpinner.show();
    this.eventService.getAllEvents(filtro, page).subscribe(response => {
      events = response;
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.ngxSpinner.hide();
      events = this.reduceDescription(events);
      events = this.reduceName(events);
      setTimeout(() => {
        this.events = events;
      }, 300);
      // console.log("EVENTS", this.events);
    });
  }

  reduceDescription(events){
    for (const item of events.data) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return events;
  }

  reduceName(events){
    for (const item of events.data) {
      if (item.name !== null ){
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else{
          item.name = item.name;
        }
      }else{
        item.name = '...';
      }
    }
    return events;
  }

  handleEventSearch(){
    this.globalSearch = this.utilitiesService.globalSearch.subscribe( response => {
      this.search = response;
      // console.log('RESPOSTA NO COMPONENT PROJETOS: ', response );
      this.getAllEvent(this.search, this.page);
    }, error => {
      this.utilitiesService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  changePage(page: number) {
    this.page = page;
    this.getAllEvent(this.search, this.page);
  }

  ngOnDestroy(){
    this.globalSearch.unsubscribe();
  }
}
