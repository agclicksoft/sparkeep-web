import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesPaixoesComponent } from './pes-paixoes.component';

describe('PesPaixoesComponent', () => {
  let component: PesPaixoesComponent;
  let fixture: ComponentFixture<PesPaixoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesPaixoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesPaixoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
