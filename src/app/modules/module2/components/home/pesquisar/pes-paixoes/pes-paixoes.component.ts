import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { SearchService } from './../../../../../../core/services/search.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { PassionsService } from './../../../../../../core/services/passions.service';
import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-pes-paixoes',
  templateUrl: './pes-paixoes.component.html',
  styleUrls: ['./pes-paixoes.component.css']
})
export class PesPaixoesComponent implements OnInit, OnChanges, OnDestroy {

  globalSearch;
  search = '';

  isLoading = true;
  passions: Array<any> = [];
  passionsSelected = {
    passions: []
  };

  passionsForm = this.fb.group({});

  objectPassions;

  usersPage = 1;
  eventsPage = 1;
  projectsPage = 1;
  organizationsPage = 1;

  constructor(
    private passionsService: PassionsService,
    private searchService: SearchService,
    private utilService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
    private fb: FormBuilder,
  ) { }

  ngOnDestroy(): void {
    this.globalSearch.unsubscribe();
  }

  ngOnInit(): void {
    this.ngxSpinner.show();
    this.getAllPassions();
    this.handlePassionsSearch();
  }

  ngOnChanges() {

  }

  getAllPassions() {
    this.passionsService.getAll().subscribe(
      response => {
        for (const item of response) { this.passionsForm.addControl(item.name, new FormControl(false)); }
        this.passions = response;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      },
      () => {
        this.ngxSpinner.hide();
      }
    );
  }

  checkControl() {
    const passions = [];
    for (const i in this.passions) {
      if (this.passionsForm.controls[this.passions[i].name].value) {
        passions.push(this.passions[i].id);
      }
    }

    this.passionsSelected.passions = passions;
    console.log('THIS PASSIONS SELECTED: ', this.passionsSelected);
    this.getSearchPassions(this.passionsSelected, this.search, this.usersPage, this.eventsPage, this.projectsPage, this.organizationsPage);
  }

  getSearchPassions(passions, keyword, userPage, eventsPage, projectsPage, organizationPage) {
    let objectPassions;
    this.searchService.getPassions(passions, keyword, userPage, eventsPage, projectsPage, organizationPage).subscribe(
      response => {
        objectPassions = response;
        // console.log('Objeto de resposta das paixões: ', response);
      }, error => {
        this.utilService.openSnackBar(`Obtivemos um error, ERRO: ${error.error.message}`);
      }, () => {
        objectPassions = this.reduceDescription(objectPassions);
        objectPassions = this.reduceName(objectPassions);
        this.objectPassions = objectPassions;
        // console.log('RESPOSTA DO SERV COMPONENT PAIXÃO: ', this.objectPassions);
      }
    );
  }

  handlePassionsSearch() {
    this.globalSearch = this.utilService.globalSearch.subscribe(response => {
      this.search = response;
      console.log('PESQUISAR NO COMPONENT PASSIONS: ', response);
      this.getSearchPassions(this.passionsSelected, this.search, 1, 1, 1, 1);
    }, error => {
      this.utilService.openSnackBar('Não foi possível receber os valores do input.');
    });
  }

  reduceDescription(object) {
    for (const item of object.users.data) {
      if (item.description !== null) {
        item.description = item.description.substring(0, 22) + '...';
      } else {
        item.description = '...';
      }
    }
    for (const item of object.events.data) {
      if (item.description !== null) {
        item.description = item.description.substring(0, 22) + '...';
      } else {
        item.description = '...';
      }
    }
    for (const item of object.projects.data) {
      if (item.description !== null) {
        item.description = item.description.substring(0, 22) + '...';
      } else {
        item.description = '...';
      }
    }
    for (const item of object.organizations.data) {
      if (item.description !== null) {
        item.description = item.description.substring(0, 22) + '...';
      } else {
        item.description = '...';
      }
    }

    return object;
  }

  reduceName(object) {
    for (const item of object.users.data) {
      if (item.name !== null) {
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else {
          item.name = item.name;
        }
      } else {
        item.name = '...';
      }
    }
    for (const item of object.events.data) {
      if (item.name !== null) {
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else {
          item.name = item.name;
        }
      } else {
        item.name = '...';
      }
    }
    for (const item of object.organizations.data) {
      if (item.name !== null) {
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else {
          item.name = item.name;
        }
      } else {
        item.name = '...';
      }
    }
    for (const item of object.projects.data) {
      if (item.name !== null) {
        if (item.name.length > 20) {
          item.name = item.name.substring(0, 17) + '...';
        }
        else {
          item.name = item.name;
        }
      } else {
        item.name = '...';
      }
    }
    return object;
  }

  changeUsersPage(event) {
    this.usersPage = event.page;
    this.getSearchPassions(this.passionsSelected, this.search, this.usersPage, this.eventsPage, this.projectsPage, this.organizationsPage);
  }
  changeEventsPage(event) {
    this.eventsPage = event.page;
    this.getSearchPassions(this.passionsSelected, this.search, this.usersPage, this.eventsPage, this.projectsPage, this.organizationsPage);
  }
  changeProjectsPage(event) {
    this.projectsPage = event.page;
    this.getSearchPassions(this.passionsSelected, this.search, this.usersPage, this.eventsPage, this.projectsPage, this.organizationsPage);
  }
  changeOrganizationsPage(event) {
    this.organizationsPage = event.page;
    this.getSearchPassions(this.passionsSelected, this.search, this.usersPage, this.eventsPage, this.projectsPage, this.organizationsPage);
  }

}
