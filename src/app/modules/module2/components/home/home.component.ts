import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { PassionsService } from './../../../../core/services/passions.service';
import { AuthService } from './../../../../core/authentication/auth.service';
import { PostService } from './../../../../core/services/post.service';
import { Post } from './../../../../shared/models/post.model';
import { FormBuilder, FormGroup, NgControlStatusGroup, Validators } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { NgZone, ViewChild } from '@angular/core';
import { take } from 'rxjs/operators';
import { UserService } from 'src/app/core/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { TermsPoliciesAboutComponent } from 'src/app/shared/template/terms-policies-about/terms-policies-about.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // RADAR
  nameRadar = 'Alex Medeiros';
  data_post = '08 de junho de 2020';

  userSession = JSON.parse(sessionStorage.getItem('user'));
  formulario: FormGroup;
  rotaPrincipal = 'ver-tudo';

  // filtroPrincipal = 'global';
  // post = new Post();

  events: Array<any> = [];
  projects: Array<any> = [];


  // TUDO RELACIONADO A POST E A ENVIO DE IMAGENS
  post;
  imgUrlForm;
  imgElement;
  imgPost; // POST IMAGEM
  passions;

  constructor(
    private activatedRoute: ActivatedRoute,
    private postService: PostService,
    private formBuilder: FormBuilder,
    private _ngZone: NgZone,
    private authService: AuthService,
    private passionsService: PassionsService,
    private userService: UserService,
    private ngxSpinner: NgxSpinnerService,
    private dialog: MatDialog
  ) { }

  // Autosize Material Design
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  triggerResize() {
    // autoSize TextArea.
    this._ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  ngOnInit(): void {
    this.post = new Post();
    this.imgUrlForm = Array();
    this.imgElement = Array();
    this.imgPost = new FormData();

    this.formulario = this.formBuilder.group({
      user: ['user'],
      privacity: ['public'],
      post: ['', Validators.required],
      img: [''],
      passion: [1],
    });

    this.passionsService.getAll().subscribe(Response => {
      this.passions = Response;
    });
  }

  getMyEvents() {
    this.userService.getMyEvents(this.userSession.id).subscribe(
      response => {
        this.events = response;
      },
      error => {
        this.userService.openSnackBar('Erro ao buscar meus eventos')
      }
    )
  }

  getMyProjects() {
    this.userService.getMyProjects(this.userSession.id).subscribe(
      response => {
        this.projects = response;
      },
      error => {
        this.userService.openSnackBar('Erro ao buscar meus projetos')
      }
    )
  }

  envioImg(event) {

    // this.imgPost = new FormData();
    // console.log('Evento: ', event);
    // console.log('---IMG ELEMENT---: ', this.imgElement);

    if (this.imgElement.length < 10 && this.imgUrlForm.length < 10) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < event.target.files.length; i++) {
        const reader = new FileReader(); // Gera uma instancia nova do FileReader

        reader.readAsDataURL(event.target.files[i]);
        reader.onload = () => {
          this.imgUrlForm.push(reader.result as string);
        };

        this.imgElement.push(event.target.files[i]);
        // this.imgPost.append('attachments', this.imgElement[i]);
        // Enviar as img *** Deve se importar as imagens para o formulario na hora do submit
      }
    } else {
      this.postService.openSnackBar('Excedeu o limite de imagens');
    }

    // DEBUG
    // console.log('IMG_POST: ', this.imgPost);
    // console.log('IMG_ELEMENT: ', this.imgElement);
    // setTimeout(() => {
    //   console.log('image_url:', this.imgUrlForm);
    // }, 900);

  }

  removeImgDOM(index) {
    this.imgUrlForm.splice(index, 1); // no index remova 1
    this.imgElement.splice(index, 1);
    // this.imgPost.splice(index, 1);

    // DEBUG
    // console.log('Element: ', this.imgElement);
    // console.log('image_url_FORM: ', this.imgUrlForm);
  }

  onSubmit() {
    this.ngxSpinner.show();

    let authorID: number;
    let role: string; // Identifica qual é o tipo do author

    // console.log('IMG ELEMENT: ', this.imgElement);

    // Adicionando as imagens no formulário **--**
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.imgElement.length; i++) {
      this.imgPost.append('attachments', this.imgElement[i]);
    }

    // DEBUG
    // console.log('IMGs: ', this.imgPost);
    // console.log('Formulário: ', this.formulario.value);

    switch (this.formulario.get('user').value) {
      case 'user': {
        authorID = this.userSession.id;
        role = 'user';
        break;
      }
      case 'event': {
        authorID = 1;
        role = 'event';
        break;
      }
      case 'organization': {
        authorID = 1;
        role = 'organization';
        break;
      }
      case 'project': {
        authorID = 1;
        role = 'project';
        break;
      }
    }

    // Cria o POST
    this.post = {
      user_id: this.userSession.id,
      privacity: this.formulario.get('privacity').value,
      description: this.formulario.get('post').value,
      author: this.formulario.get('user').value,
      passions: this.formulario.get('passion').value,
    };
    this.post[`author_${role}_id`] = authorID;

    // console.log('JSON: >>> ', this.post);

    // SERVICE POST
    this.postService.create(this.post).subscribe(res => {
      this.postService.openSnackBar('Postagem feita com sucesso!');
      $('#modalPost').modal('hide');
      // console.log('Response: ', res);
      this.postService.updateImg(res.post.id, this.imgPost).subscribe(response => {
        this.postService.openSnackBar('Imagens Enviadas!');
        this.ngxSpinner.hide();
        this.ngOnInit();
      },
        error => {
          this.ngxSpinner.hide();
          console.log('Error: ', error.error);
          this.postService.openSnackBar('Não foi possível enviar as imagens.');
        });
    },
      error => {
        this.ngxSpinner.hide();
        console.log('Error: ', error.error);
        $('#modalPost').modal('hide');
        this.ngOnInit();
        this.postService.openSnackBar('Não foi possível criar a postagem.');
        setTimeout(() => {
          this.postService.openSnackBar(error.error.message);
        }, 800);
      }
    );
  }

  // Rota principal  -=- Ver Tudo -=- Pessoas -=- Entidades -=- Projetos -=- Eventos
  alternarRotaPrincipal(rota) {
    this.rotaPrincipal = rota;
  }

  // SAIR
  sair(): void {
    this.authService.logout();
  }

  openPolicies() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isPolicies: true
      }
    });
  }

  openTerms() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isTerms: true
      }
    });
  }

  openAbout() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isAbout: true
      }
    });
  }
}
