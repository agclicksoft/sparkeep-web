import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { OrganizationService } from './../../../../../../core/services/organization.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-minhas-entidades',
  templateUrl: './minhas-entidades.component.html',
  styleUrls: ['./minhas-entidades.component.css']
})
export class MinhasEntidadesComponent implements OnInit {

  userSession = JSON.parse(sessionStorage.getItem('user')) || JSON.parse(localStorage.getItem('user'));

  ownerOrganizations;
  administratorsOrganizations;

  constructor(
    private organizationService: OrganizationService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.ngxSpinner.show();
    this.organizationService.getAllUserOrganizations(this.userSession.id).subscribe( Response => {
      // console.log('Response: ', Response);
      this.ownerOrganizations = Response.ownerOrganizations;
      this.administratorsOrganizations = Response.administratorsOrganizations;
    }, error => {
      this.ngxSpinner.hide();
      this.utilitiesService.openSnackBar('Não foi possivel buscar as organizações!');
    }, () => {
      this.ngxSpinner.hide();
    });
  }

}
