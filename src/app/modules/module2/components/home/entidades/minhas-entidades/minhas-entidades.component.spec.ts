import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhasEntidadesComponent } from './minhas-entidades.component';

describe('MinhasEntidadesComponent', () => {
  let component: MinhasEntidadesComponent;
  let fixture: ComponentFixture<MinhasEntidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhasEntidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhasEntidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
