import { MatDialog } from '@angular/material/dialog';
import { ModalDeleteUserComponent } from './../../../../../../shared/template/modal-delete-user/modal-delete-user.component';
import { LocationService } from './../../../../../../core/services/location.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { OrganizationService } from './../../../../../../core/services/organization.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-criar-entidade',
  templateUrl: './criar-entidade.component.html',
  styleUrls: ['./criar-entidade.component.css'],
})
export class CriarEntidadeComponent implements OnInit {
  id: number; // validar pelo id

  imgUrlForm = 'assets/svg/camera_alt.svg';
  imgRead;
  imgPost;
  locationCity;
  states;
  stateUf;
  showCpf = false;

  administrators: Array<any> = [];
  showAdministratorsModal = false;

  userSession =
    JSON.parse(sessionStorage.getItem('user')) ||
    JSON.parse(localStorage.getItem('user'));

  editMode = false;

  isSubmited = false;

  imgProfile = 'assets/img/user.svg';

  constructor(
    private organizationService: OrganizationService,
    private utilitiesService: UtilitiesService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    private locationService: LocationService
  ) {
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.editMode = true;
    }
  }

  organizationForm = this.fb.group({
    user_id: ['', [Validators.required]],
    name: [
      '',
      [Validators.required, Validators.minLength(5), Validators.maxLength(30)],
    ],
    description: ['', [Validators.required, Validators.maxLength(250)]],
    telephone: ['', [Validators.required]],
    cellphone: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    show_telephone: [''],
    show_cellphone: [''],
    cnpj: ['', [Validators.required]],
    address_country: ['', [Validators.required]],
    address_state: ['', [Validators.required]],
    address_city: ['', [Validators.required]],
    address_complement: [''],
    address_neighborhood: [''],
    address_street: [''],
    passions: [[], [Validators.required]],
    administrators: [[]],
    has_philantropy: [false],
    philantropy_record: [''],
    image_url: 'assets/img/organization.png',
    youtube: [''],
    facebook: [''],
    instagram: [''],
    twitter: [''],
    site: [''],
  });

  ngOnInit(): void {
    if (this.id) {
      this.getOrganization();
    }
    this.getStates();
    // this.onChecked();
  }

  handleProfileOrganization() {
    this.imgProfile =
      this.userSession.image_url !== null
        ? this.userSession.image_url
        : 'assets/img/user.svg';
  }

  getOrganization() {
    this.organizationService.getById(this.id).subscribe(
      (response) => {
        this.organizationForm.patchValue(response);
        // console.log('Response da organização: ', response);

        this.organizationForm.controls.administrators.patchValue(null);
        this.administrators = response.administrators;

        this.stateUf = response?.address_state;
        if (response.image_url) {
          this.imgUrlForm = response.image_url;
        }
      },
      (error) => {},
      () => {
        this.getUserCity(this.stateUf);
      }
    );
  }

  envioImg(event) {
    this.imgPost = new FormData();
    const reader = new FileReader();
    this.imgRead = true;

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlForm = reader.result as string;
    };
    this.imgPost.append('attachment', event.target.files[0]);

    console.log('this.imgUrlForm: ', this.imgUrlForm);
    console.log('this.imgPost: ', this.imgPost);
  }

  // MODAL ADMIN
  toggleModal() {
    const modal = document.querySelector('app-add-administrators');
    modal.classList.contains('d-none')
      ? modal.classList.remove('d-none')
      : modal.classList.add('d-none');
  }

  administratorsControl(event) {
    const arrayAdmin = [...event, ...this.administrators];
    this.administrators = [...new Set(arrayAdmin)];
  }

  deleteAdministrator(index) {
    this.administrators.splice(index, 1);
  }

  // PASSIONS
  onPassionChange(event) {
    event.length > 0
      ? this.organizationForm.controls.passions.patchValue(
          event.map((el) => el.id)
        )
      : this.organizationForm.controls.passions.patchValue([]);
  }

  onSubmit() {
    this.isSubmited = true;
    // console.log('formulário: ', this.organizationForm.value);
    if (this.organizationForm.status === 'VALID') {
      this.spinner.show();

      // tslint:disable-next-line: radix
      this.organizationForm.get('user_id').patchValue(this.userSession.id);

      if (
        this.organizationForm.value.passions.length > 0 &&
        this.organizationForm.value.passions[0].id
      ) {
        this.organizationForm.controls.passions.patchValue(
          this.organizationForm.value.passions.map((el) => el.id)
        );
      }

      this.organizationForm.controls.administrators.patchValue(null);
      const adminIds = this.administrators.map((el) => el.id);
      this.organizationForm.controls.administrators.patchValue(adminIds);
      // console.log('Id de administradores: ', adminIds);

      if (this.editMode) {
        this.organizationService
          .update(this.id, this.organizationForm.value)
          .subscribe(
            (response) => {
              this.utilitiesService.openSnackBar(
                'Entidade atualizada com sucesso.'
              );
              if (this.imgPost) {
                this.uploadAttachment(response.id);
              } else {
                this.router.navigate(['/entidades']);
              }
            },
            (error) => {
              this.spinner.hide();
              this.utilitiesService.openSnackBar(
                `${error.error.message}`
              );
            }
          );
      } else {
        let res;
        this.organizationService.create(this.organizationForm.value).subscribe(
          (response) => {
            this.utilitiesService.openSnackBar('Entidade criada com sucesso.');
            res = response;
          },
          (error) => {
            this.spinner.hide();
            this.utilitiesService.openSnackBar(
              `${error.error.message}`
            );
          },
          () => {
            if (this.imgPost) {
              this.uploadAttachment(res.id);
            } else {
              this.router.navigate(['/entidades']);
            }
          }
        );
      }
    } else {
      this.utilitiesService.openSnackBar(
        'Formulário inválido, insira corretamente os valores.'
      );
    }
  }

  uploadAttachment(id) {
    if (this.imgPost) {
      this.organizationService.uploadAttachment(id, this.imgPost).subscribe(
        (response) => {
          // console.log('Valores do response imagem: ', response);
          this.utilitiesService.openSnackBar('Imagem enviada.');
          this.utilitiesService.myOrganizationsReload.emit(response);
        },
        (error) => {
          this.utilitiesService.openSnackBar(
            'Não foi possível enviar a imagem.'
          );
        },
        () => {
          this.router.navigate(['/entidades']);
        }
      );
    } else {
      this.router.navigate(['/entidades']);
    }
  }

  getCityByState(event) {
    const id = event.target.value;
    this.locationService.getCity(id).subscribe((response) => {
      this.locationCity = response;
    });
  }

  getStates(): void {
    this.locationService.getStates().subscribe((response) => {
      this.states = response
        .map((uf) => {
          const state = {
            uf: uf.sigla,
            name: uf.nome,
          };
          return state;
        })
        .sort((a, b) => {
          const x = a.name.toLowerCase();
          const y = b.name.toLowerCase();
          if (x < y) {
            return -1;
          }
          if (x > y) {
            return 1;
          }
        });
    });
  }

  getUserCity(estado) {
    this.locationService.getStates().subscribe(
      (res) => {
        for (const item of res) {
          if (estado === item.sigla) {
            this.stateUf = item.sigla;
            break;
          }
        }
      },
      (error) => {},
      () => {
        this.getCity(this.stateUf);
      }
    );
  }

  getCity(state) {
    this.locationService.getCity(state).subscribe((response) => {
      this.locationCity = response;
    });
  }

  handleHasPhilantropy(value) {
    if (value) {
      console.log('Inserindo os validators');
      this.organizationForm
        .get('philantropy_record')
        .setValidators([Validators.required]);
      this.organizationForm.get('philantropy_record').updateValueAndValidity();
    } else {
      console.log('Limpando os validators');
      this.organizationForm.get('philantropy_record').patchValue('');
      this.organizationForm.get('philantropy_record').clearValidators();
      this.organizationForm.get('philantropy_record').updateValueAndValidity();
    }
  }

  deleteOrganization() {
    this.dialog.open(ModalDeleteUserComponent, {
      data: {
        id: this.id,
        type: 'Organization',
      },
    });
  }

  checkAndSetValidation() {
    if (this.organizationForm.get('email').value) {
      this.organizationForm
        .get('email')
        .setValidators([Validators.required, Validators.email]);
      this.organizationForm.get('telephone').clearValidators();
      this.organizationForm.get('cellphone').clearValidators();
    }
    if (this.organizationForm.get('telephone').value) {
      this.organizationForm
        .get('telephone')
        .setValidators([Validators.required]);
      this.organizationForm.get('email').clearValidators();
      this.organizationForm.get('cellphone').clearValidators();
    }
    if (this.organizationForm.get('cellphone').value) {
      this.organizationForm
        .get('cellphone')
        .setValidators([Validators.required]);
      this.organizationForm.get('email').clearValidators();
      this.organizationForm.get('telephone').clearValidators();
    }
    this.organizationForm.get('cellphone').updateValueAndValidity();
    this.organizationForm.get('email').updateValueAndValidity();
    this.organizationForm.get('telephone').updateValueAndValidity();
  }
}
