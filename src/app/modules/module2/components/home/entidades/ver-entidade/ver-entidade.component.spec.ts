import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerEntidadeComponent } from './ver-entidade.component';

describe('VerEntidadeComponent', () => {
  let component: VerEntidadeComponent;
  let fixture: ComponentFixture<VerEntidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerEntidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerEntidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
