import { UserService} from './../../../../../../core/services/user.service';
import { MyPartnersDialogComponent} from './../../../../../../shared/template/my-partners-dialog/my-partners-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { OrganizationService } from './../../../../../../core/services/organization.service';
import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-ver-entidade',
  templateUrl: './ver-entidade.component.html',
  styleUrls: ['./ver-entidade.component.css']
})
export class VerEntidadeComponent implements OnInit, OnChanges {

  organization: any;
  organizationId;
  myPosts: any = null;
  ownerMode = false;
  userId: any;
  id;

  isInspired = false;

  totalPartnership;

  showMoreDescription = true;
  routeId;

  constructor(
    private organizationService: OrganizationService,
    private utilitiesService: UtilitiesService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private userService: UserService,
    // private partnerSolicitation: MyPartnersDialogComponent
  ) {
    // this.id = parseInt(route.snapshot.paramMap.get('id'));
    this.routeId = this.route.params.subscribe(params => {
      this.id = (params.id);
      this.organizationId = (params.id);
      this.ngOnInit();
    });
  }

  ngOnInit(): void {
    this.getOrganization(this.organizationId);
    this.getOrganizationPosts(this.organizationId);
  }

  openDialog() {
    const receiver = {
      id: this.organizationId,
      role: 'organization',
    };

    if (this.ownerMode === false) {
      // Emite os valores necessários para executar a função da parceria.
      this.utilitiesService.partnershipReceiverValue = receiver;
      this.dialog.open(MyPartnersDialogComponent);
    }
  }

  getLoggedUser() {
    this.userService.getAccountLogged().subscribe(response => {
      this.userId = response.id;
    }, error => {
      this.utilitiesService.openSnackBar(`Erro: ${error.message}`);
    }, () => {
      this.verifyOwner(this.organization.user_id, this.userId);
      // this.checkInspiration(this.userId);
    });
  }

  // checkInspiration(id) {
  //   this.userService.getUser(id).subscribe(response => {
  //     // console.log('CheckInspiration: ', response);
  //     for (const inspiration of response.inspirations) {
  //       if (inspiration.organization_inspiration_id === this.id) {
  //         this.isInspired = true;
  //       }
  //     }
  //   });
  // }

  verifyOwner(organizationOwnerId: string, userId: string) {
    if (userId === organizationOwnerId) {
      this.ownerMode = true;
    }
  }

  getOrganization(id){
    this.organizationService.getById(id).subscribe(response => {
      this.organization = response;
      console.log('Organização? ', this.organization);
      this.isInspired = response.inspired;
      this.getLoggedUser();
      this.totalPartnership = parseInt(response.__meta__.total_send_partners) + parseInt(response.__meta__.total_received_partners);
      // this.organizationId = response.user_id;
      // console.log('Objeto entidade: ', this.organization);
    }, error => {

    }, () => {
      // console.log('Organization: ', this.organization);
    });
  }

  getOrganizationPosts(id){
    this.organizationService.getOrganizationsPosts(id).subscribe(
      response => {
        this.myPosts = response;
        // console.log('POSTS: ', response);
      },
      error => {

      },
      () => {

      }
    );
  }

  ngOnChanges(): void {
    this.checkOwner();
  }

  private checkOwner(): void {
    this.getLoggedUser();
    if (this.ownerMode === false) {
      this.verifyOwner(this.organizationId, this.userId);
    }
  }

  setInspiration(){
    const inspiration = {
      user_inspiration_id: null,
      organization_inspiration_id: this.id,
    };

    this.utilitiesService.setInspiration(inspiration).subscribe(
      response => {
        this.utilitiesService.openSnackBar(`${response.status ? 'Você se inspirou em ' + this.organization.name : response.message}`);
        this.isInspired = response.status;
        if (this.isInspired) {
          this.organization.__meta__.total_inspireds = parseInt(this.organization.__meta__.total_inspireds) + 1;
        } else {
          this.organization.__meta__.total_inspireds = parseInt(this.organization.__meta__.total_inspireds)  - 1;
        }
      },
      error => {
        this.utilitiesService.openSnackBar(``);
      },
      () => {

      }
    );
  }

  handleTypeOwnerAdmin(organization: any): string {
    if (organization?.user_id) { return 'User'; }
    if (organization?.project_id) { return 'Project'; }
  }

  handleReturnId(organization: any): number {
    if (organization?.user_id) { return organization?.user_id; }
    if (organization?.project_id) { return organization?.project_id; }
  }

  renderDescription(data: string) {
    if (data && this.showMoreDescription) {
      return data.substring(0, 80);
    }else{
      return data;
    }
  }
}
