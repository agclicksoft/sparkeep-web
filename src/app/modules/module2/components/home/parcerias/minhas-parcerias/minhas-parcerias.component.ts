import { OrganizationService } from 'src/app/core/services/organization.service';
import { EventService } from './../../../../../../core/services/event.service';
import { ProjectService } from 'src/app/core/services/project.service';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { PartnershipService } from './../../../../../../core/services/partnership.service';
import { MaintenanceTypeService } from './../../../../../../core/services/maintenance-type.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from './../../../../../../core/services/user.service';
import { MyPartnershipDialogComponent } from './../../../../../../shared/template/my-partnership-dialog/my-partnership-dialog.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-minhas-parcerias',
  templateUrl: './minhas-parcerias.component.html',
  styleUrls: ['./minhas-parcerias.component.css']
})
export class MinhasParceriasComponent implements OnInit {

  selectOption: string = 'event';

  userId;

  usersPartners;
  projectsPartners;
  eventsPartners;
  organizationsPartners;

  search = '';
  userPage = 1;
  eventPage = 1;
  projectPage = 1;
  organizationPage = 1;
  status = 1;

  myProject;
  myEvent;
  myOrganization;

  constructor(
    public dialog: MatDialog,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private maintenanceTypeService: MaintenanceTypeService,
    private partnerService: PartnershipService,
    private utilsService: UtilitiesService,
    private projectService: ProjectService,
    private eventService: EventService,
    private organizationService: OrganizationService
  ) { }

  openDialog(partnership, type, agreement, shortage) {
    let shortageName;
    this.maintenanceTypeService.getAll().subscribe(response => {
      for (const shortageRes of response) {
        if (shortageRes.id === shortage) {
          shortageName = shortageRes.description;
        }
      }
    }, error => {

    }, () => {
      this.dialog.open(MyPartnershipDialogComponent, {
        data: {
          id: partnership.id,
          title: partnership.name,
          subtitle: type,
          description: partnership.description,
          typePartner: shortageName,
          partnership_agreement: agreement,
          image_url: partnership.image_url
        }
      });
    });
  }

  ngOnInit(): void {
    this.getAccountLogged();
  }

  getAccountLogged() {
    this.spinner.show();
    this.userService.getAccountLogged().subscribe(
      response => {
        this.userId = response.id;
      },
      error => { },
      () => {
        this.getMyPartnerships(this.userId, 1, 1, 1, 1);

        this.spinner.hide();
      }
    );
  }

  handleFavoritePartnership(id, favorite) {
    const json = {
      partner_id: id,
      user_id: this.userId,
      action: favorite
    };

    // console.log('VALOR DO FAVORITO: ', json);

    this.partnerService.favorite(json).subscribe(
      response => {
        this.utilsService.openSnackBar(`${favorite === 'favorite' ? 'Favoritado' : 'Desfavoritado'}`);
      },
      error => {
        this.utilsService.openSnackBar(`${error.error.message}`);
      }
    );
  }

  getMyPartnerships(userId, userPage, eventPage, projectPage, organizationPage) {
    this.userService.getMyPartnerships(userId, '', userPage, eventPage, projectPage, organizationPage).subscribe(
      response => {
        this.usersPartners = response.userPartners;
        this.projectsPartners = response.projectPartners;
        this.eventsPartners = response.eventPartners;
        this.organizationsPartners = response.organizationPartners;
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      }
    )
  }

  changePageEvent(page: number) {
    this.eventPage = page;
    this.getMyPartnerships(this.userId, 1, this.eventPage, this.projectPage, this.organizationPage);
  }

  changePageProject(page: number) {
    this.projectPage = page;
    this.getMyPartnerships(this.userId, 1, this.eventPage, this.projectPage, this.organizationPage);
  }

  changePageOrganization(page: number) {
    this.organizationPage = page;
    this.getMyPartnerships(this.userId, 1, this.eventPage, this.projectPage, this.organizationPage);
  }

}
