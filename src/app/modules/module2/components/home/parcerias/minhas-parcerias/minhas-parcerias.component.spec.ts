import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhasParceriasComponent } from './minhas-parcerias.component';

describe('MinhasParceriasComponent', () => {
  let component: MinhasParceriasComponent;
  let fixture: ComponentFixture<MinhasParceriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhasParceriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhasParceriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
