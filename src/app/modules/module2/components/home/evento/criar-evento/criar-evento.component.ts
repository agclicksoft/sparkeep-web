import { MatDialog } from '@angular/material/dialog';
import { ModalDeleteUserComponent } from './../../../../../../shared/template/modal-delete-user/modal-delete-user.component';
import { LocationService } from './../../../../../../core/services/location.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { EventService } from './../../../../../../core/services/event.service';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Console } from 'console';
import { ProjectService } from 'src/app/core/services/project.service';
import * as moment from 'moment';
import { OrganizationService } from 'src/app/core/services/organization.service';

@Component({
  selector: 'app-criar-evento',
  templateUrl: './criar-evento.component.html',
  styleUrls: ['./criar-evento.component.css'],
})
export class CriarEventoComponent implements OnInit {
  id: number;

  imgUrlFormEvent = 'assets/svg/camera_alt.svg';
  imgReadEvent;
  imgPostEvent;

  isSubmited = false;

  administrators: Array<any> = [];
  showAdministratorsModal = false;

  myProjects: Array<any> = [];
  myOrganizations: Array<any> = [];

  owner: any;

  editMode = false;

  locationState: any;
  locationCity: any;

  userSession =
    JSON.parse(sessionStorage.getItem('user')) ||
    JSON.parse(localStorage.getItem('user'));

  imgSelectedSender = '/assets/img/user.svg';

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private eventService: EventService,
    private organizationService: OrganizationService,
    private utilitiesService: UtilitiesService,
    private projectService: ProjectService,
    private dialog: MatDialog,
    private locationService: LocationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.editMode = true;
    }
  }

  eventForm = this.fb.group({
    owner: ['', Validators.required],
    user_id: [''],
    organization_id: [''],
    project_id: [''],
    name: ['', Validators.required],
    description: [
      '',
      [Validators.required, Validators.minLength(3), Validators.maxLength(255)],
    ],
    local: [''],
    date: ['', Validators.required],
    start_time: ['', Validators.required],
    address_complement: [''],
    address_neighborhood: [''],
    address_state: ['', [Validators.required]],
    address_street: ['', Validators.required],
    address_number: ['', Validators.required],
    address_city: ['', Validators.required],
    address_country: ['', Validators.required],
    telephone: ['', [Validators.required]],
    cellphone: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    frequency: ['', Validators.required],
    show_telephone: [''],
    show_cellphone: [''],
    maintenance_times: [[]],
    kind_of_help: [''],
    administrators: [[]],
    passions: [[], [Validators.required]],
    shortages: [[]],
    image_url: 'assets/img/img_event_default.png',
    has_shortages: false,
    youtube: [''],
    facebook: [''],
    instagram: [''],
    twitter: [''],
    site: [''],
  });

  ngOnInit(): void {
    this.getMyOrganizations();
    this.getMyProjects();
    if (this.id) {
      this.getEvent();
    }
    // this.onStateOptionSelected(this.eventForm.get('address_state').value);
    this.getAllStates();
  }

  getEvent() {
    this.eventService.getById(this.id).subscribe((response) => {
      this.eventForm.patchValue(response);

      this.eventForm.controls.administrators.patchValue(null);
      this.administrators = response.administrators;

      console.log('FORMULARIO: ', this.eventForm.value);

      this.eventForm.controls.date.patchValue(
        moment(response.date).parseZone().format('YYYY-MM-DD')
      );
      this.onStateOptionSelected(response.address_state);

      if (response.image_url) {
        this.imgUrlFormEvent = response.image_url;
      }
    });
  }

  getMyProjects() {
    this.projectService.getMyProjects(this.userSession.id).subscribe(
      (response) => {
        this.myProjects = [
          ...response.ownerProjects,
          ...response.administratorsProjects,
        ];

        this.myProjects.sort((a, b) =>
          a.name < b.name ? -1 : a.name > b.name ? 1 : 0
        );
        // console.log(this.myProjects);
      },
      (error) => {
        this.utilitiesService.openSnackBar(
          'Obtivemos um erro ao tentar buscar seus projetos'
        );
      },
      () => {
        this.handleShowOwner();
      }
    );
  }

  getMyOrganizations() {
    this.organizationService
      .getAllUserOrganizations(this.userSession.id)
      .subscribe(
        (response) => {
          this.myOrganizations = [
            ...response.ownerOrganizations,
            ...response.administratorsOrganizations,
          ];
        },
        (error) => {
          this.utilitiesService.openSnackBar(
            'Obtivemos um erro ao tentar buscar suas entidades'
          );
        },
        () => {
          this.handleShowOwner();
        }
      );
  }

  handleProfileEvent(value) {
    const roleSend = value.split('_', 2);
    this.changeOwner(roleSend[0], roleSend[1]);
    this.changeImg(roleSend[0], roleSend[1]);
  }

  changeOwner(role, id) {
    this.eventForm.patchValue({
      user_id: null,
      project_id: null,
      organization_id: null,
    });

    switch (role) {
      case 'user': {
        this.eventForm.get('user_id').patchValue(id);
        break;
      }
      case 'project': {
        this.eventForm.get('project_id').patchValue(id);
        break;
      }
      case 'organization': {
        this.eventForm.get('organization_id').patchValue(id);
        break;
      }
    }
  }

  changeImg(role, id) {
    switch (role) {
      case 'user':
        this.imgSelectedSender =
          this.userSession.image_url !== null
            ? this.userSession.image_url
            : 'assets/img/user.svg';
        break;
      case 'project':
        for (const project of this.myProjects) {
          if (project.id === parseInt(id)) {
            this.imgSelectedSender = project.image_url;
          }
        }
        break;
      case 'organization':
        for (const organization of this.myOrganizations) {
          if (organization.id === parseInt(id)) {
            this.imgSelectedSender = organization.image_url;
          }
        }
        break;
    }
  }

  envioImgEvent(event) {
    this.imgPostEvent = new FormData();
    const reader = new FileReader();
    this.imgReadEvent = true;

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlFormEvent = reader.result as string;
    };
    this.imgPostEvent.append('attachment', event.target.files[0]);
    // console.log('this.imgUrlForm: ', this.imgUrlForm);
    // console.log('this.imgPost: ', this.imgPost);
  }

  // MODAL ADMIN
  toggleModal() {
    const modal = document.querySelector('app-add-administrators');
    modal.classList.contains('d-none')
      ? modal.classList.remove('d-none')
      : modal.classList.add('d-none');
  }

  administratorsControl(event) {
    const arrayAdmin = [...event, ...this.administrators];
    this.administrators = [...new Set(arrayAdmin)];
  }

  deleteAdministrator(index) {
    this.administrators.splice(index, 1);
  }

  // PASSIONS
  onPassionChange(event) {
    event.length > 0
      ? this.eventForm.controls.passions.patchValue(event.map((el) => el.id))
      : this.eventForm.controls.passions.patchValue([]);
  }
  // CARENCIAS
  onShortageChange(event) {
    event.length > 0
      ? this.eventForm.controls.shortages.patchValue(event.map((el) => el.id))
      : this.eventForm.controls.shortages.patchValue([]);
  }
  onMaintenanceTimeChange(event) {
    // this.eventForm.controls.maintenance_time.patchValue(event);
    event.length > 0
      ? this.eventForm.controls.maintenance_times.patchValue(
          event.map((el) => el.id)
        )
      : this.eventForm.controls.maintenance_times.patchValue([]);
  }
  onKindOfHelpChange(event) {
    this.eventForm.controls.kind_of_help.patchValue(event);
  }

  onSubmit() {
    this.isSubmited = true;
    console.log('Valor do formulário: ', this.eventForm.controls);

    if (this.eventForm.status === 'VALID') {
      this.spinner.show();

      if (
        this.eventForm.value.shortages.length > 0 &&
        this.eventForm.value.shortages[0].id
      ) {
        this.eventForm.controls.shortages.patchValue(
          this.eventForm.value.shortages.map((el) => el.id)
        );
      }

      if (
        this.eventForm.value.maintenance_times.length > 0 &&
        this.eventForm.value.maintenance_times[0].id
      ) {
        this.eventForm.controls.maintenance_times.patchValue(
          this.eventForm.value.maintenance_times.map((el) => el.id)
        );
      }

      if (
        this.eventForm.value.passions.length > 0 &&
        this.eventForm.value.passions[0].id
      ) {
        this.eventForm.controls.passions.patchValue(
          this.eventForm.value.passions.map((el) => el.id)
        );
      }

      this.eventForm.controls.administrators.patchValue(null);
      const adminIds = this.administrators.map((el) => el.id);
      this.eventForm.controls.administrators.patchValue(adminIds);
      // console.log('Id de administradores: ', adminIds);

      console.log('Valor do formulário: ', this.eventForm.value);

      if (this.editMode) {
        this.eventService.update(this.id, this.eventForm.value).subscribe(
          (response) => {
            this.utilitiesService.openSnackBar(
              'Evento atualizado com sucesso.'
            );
            if (this.imgPostEvent) {
              this.uploadAttachment(response.id);
            } else {
              this.router.navigate(['/eventos']);
              this.utilitiesService.myEventsReload.emit(response);
            }
          },
          (error) => {
            this.utilitiesService.openSnackBar(
              'Desculpe, não foi possível criar o evento.'
            );
          }
        );
      } else {
        this.eventService.create(this.eventForm.value).subscribe(
          (response) => {
            this.utilitiesService.openSnackBar('Evento criado com sucesso.');
            if (this.imgPostEvent) {
              this.uploadAttachment(response.id);
            } else {
              this.router.navigate(['/eventos']);
              this.utilitiesService.myEventsReload.emit(response);
            }
          },
          (error) => {
            this.utilitiesService.openSnackBar(
              'Desculpe, não foi possível criar o evento.'
            );
          }
        );
      }
    } else {
      this.utilitiesService.openSnackBar(
        'Formulário inválido, insira corretamente os valores.'
      );
    }
  }

  uploadAttachment(id) {
    this.eventService.uploadAttachment(id, this.imgPostEvent).subscribe(
      (response) => {
        this.utilitiesService.openSnackBar('Imagem enviada.');
        this.utilitiesService.myEventsReload.emit(response);
      },
      (error) => {
        this.utilitiesService.openSnackBar('Não foi possível enviar a imagem.');
      },
      () => {
        this.router.navigate(['/eventos']);
      }
    );
  }

  getAllStates() {
    this.locationService.getStates().subscribe((response) => {
      this.locationState = response;
    });
  }

  onStateOptionSelected(event) {
    let idState;
    for (const item of this.locationState) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityByStateId(idState);
        break;
      }
    }
  }

  getCityByStateId(id) {
    this.locationService.getCity(id).subscribe((response) => {
      this.locationCity = response;
    });
  }

  handleShowOwner() {
    this.owner = null;
    if (this.editMode) {
      this.eventForm.get('owner').clearValidators();
      this.eventForm.controls.owner.patchValue(null);
      if (this.eventForm.get('user_id').value !== null) {
        this.owner = this.userSession.name;
        this.userSession.image_url !== null
          ? (this.imgSelectedSender = this.userSession.image_url)
          : (this.imgSelectedSender = 'assets/img/user.svg');
      }
      if (this.eventForm.get('organization_id').value !== null) {
        // console.log('Minhas organizações: ', this.myOrganizations);
        for (const item of this.myOrganizations) {
          // console.log('Item: ', item);
          if (item.id === this.eventForm.get('organization_id').value) {
            this.owner = item.name;
            item.image_url !== null
              ? (this.imgSelectedSender = item.image_url)
              : (this.imgSelectedSender = 'assets/img/user.svg');
          }
        }
      }
      if (this.eventForm.get('project_id').value !== null) {
        // console.log('Meus projetos: ', this.myProjects);
        for (const item of this.myProjects) {
          // console.log('Item: ', item);
          if (item.id === this.eventForm.get('project_id').value) {
            this.owner = item.name;
            item.image_url !== null
              ? (this.imgSelectedSender = item.image_url)
              : (this.imgSelectedSender = 'assets/img/user.svg');
          }
        }
      }
    }
  }

  handleHasShortages(value) {
    if (value) {
      this.eventForm.get('shortages').setValidators(Validators.required);
      this.eventForm
        .get('maintenance_times')
        .setValidators(Validators.required);
      this.eventForm.get('shortages').updateValueAndValidity();
      this.eventForm.get('maintenance_times').updateValueAndValidity();
      // console.log('TEM CARENCIA: ', this.eventForm.controls);
    } else {
      this.eventForm.get('shortages').patchValue('');
      this.eventForm.get('shortages').clearValidators();
      this.eventForm.get('shortages').updateValueAndValidity();
      this.eventForm.get('maintenance_times').patchValue('');
      this.eventForm.get('maintenance_times').clearValidators();
      this.eventForm.get('maintenance_times').updateValueAndValidity();
      // console.log('NÃO TEM CARENCIA: ', this.eventForm.controls);
    }
  }

  deleteEvent() {
    this.dialog.open(ModalDeleteUserComponent, {
      data: {
        id: this.id,
        type: 'Event',
      },
    });
  }

  checkAndSetValidation() {
    if (this.eventForm.get('email').value) {
      this.eventForm
        .get('email')
        .setValidators([Validators.required, Validators.email]);
      this.eventForm.get('telephone').clearValidators();
      this.eventForm.get('cellphone').clearValidators();
    }
    if (this.eventForm.get('telephone').value) {
      this.eventForm.get('telephone').setValidators([Validators.required]);
      this.eventForm.get('email').clearValidators();
      this.eventForm.get('cellphone').clearValidators();
    }
    if (this.eventForm.get('cellphone').value) {
      this.eventForm.get('cellphone').setValidators([Validators.required]);
      this.eventForm.get('email').clearValidators();
      this.eventForm.get('telephone').clearValidators();
    }
    this.eventForm.get('cellphone').updateValueAndValidity();
    this.eventForm.get('email').updateValueAndValidity();
    this.eventForm.get('telephone').updateValueAndValidity();
  }
}
