import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { EventService } from './../../../../../../core/services/event.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meus-eventos',
  templateUrl: './meus-eventos.component.html',
  styleUrls: ['./meus-eventos.component.css']
})
export class MeusEventosComponent implements OnInit {

  userSession = JSON.parse(sessionStorage.getItem('user')) || JSON.parse(localStorage.getItem('user'));

  ownerEvents;
  administratorsEvents;

  constructor(
    private eventService: EventService,
    private utilitiesService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.ngxSpinner.show();
    this.eventService.getAllUserEvents(this.userSession.id).subscribe( Response => {
      // console.log('Response: ', Response);
      this.ownerEvents = Response.ownerEvents;
      this.administratorsEvents = Response.administratorsEvents;
    }, error => {
      this.ngxSpinner.hide();
      this.utilitiesService.openSnackBar('Não foi possivel buscar os eventos!');
    }, () => {
      this.ngxSpinner.hide();
    });
  }

}
