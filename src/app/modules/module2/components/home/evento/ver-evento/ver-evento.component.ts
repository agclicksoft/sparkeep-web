import { MaintenanceTypeService } from './../../../../../../core/services/maintenance-type.service';
import { Event } from 'src/app/shared/models/event.model';
import { UserService } from 'src/app/core/services/user.service';
import { MyPartnersDialogComponent } from './../../../../../../shared/template/my-partners-dialog/my-partners-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { EventService } from './../../../../../../core/services/event.service';
import { Component, OnInit, EventEmitter, HostListener } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';

@Component({
  selector: 'app-ver-evento',
  templateUrl: './ver-evento.component.html',
  styleUrls: ['./ver-evento.component.css']
})
export class VerEventoComponent implements OnInit {

  event: any;
  eventId;
  owner;
  ownerMode = false;
  userId;
  isConfirmed = false;

  maintenance = [];

  confirmedPeople;

  allUserProjectsId = [];
  allUserOrganizationsId = [];

  routeId;

  posts;
  page = 1;

  constructor(
    private eventService: EventService,
    private userService: UserService,
    private utilitiesService: UtilitiesService,
    private shortageService: MaintenanceTypeService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    // private partnerSolicitation: MyPartnersDialogComponent
  ) {
    // this.eventId = this.route.snapshot.params.id;
    this.routeId = this.route.params.subscribe(params => {
      this.eventId = (params.id);
      this.ngOnInit();
    });
  }


  ngOnInit(): void {
    this.getEvent();
    this.getConfirmedPeople();
    this.getAllMaintenance();
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    const pos = document.body.scrollHeight;
    const max = window.innerHeight + window.scrollY;

    if (pos === max) {
      if (this.posts?.page < this.posts?.lastPage) {
        this.appendPosts();
      }
    }
  }

  appendPosts() {
    this.eventService.getEventsPosts(this.eventId, this.page).subscribe(
      response => {
        this.posts.total = response.total,
          this.posts.page = response.page,
          this.posts.perPage = response.perPage,
          this.posts.lastPage = response.lastPage;
        this.posts.data = [...this.posts.data, ...response.data];
        // console.log('POST: ', this.posts);
      },
      error => {
        console.log(error);
      }
    );

  }

  getEvent() {
    this.eventService.getById(this.eventId).subscribe(response => {
      this.event = response;
      // this.verifyEventOwner(response.user_id, this.userId);
      // console.log('Objeto id evento: ', this.event.id);
      // console.log('VALOR DO EVENT ID: ', this.eventId);
    }, error => {

    }, () => {
      this.loggedUser(this.event);
      this.getEventPosts(this.eventId, this.page);
    });
  }

  getEventPosts(id, page) {
    this.eventService.getEventsPosts(id, page).subscribe(response => {
      console.log(response);
      this.posts = response;
    });
  }

  getAllUserProjectsId(id) {
    this.userService.getMyProjects(id).subscribe(response => {
      this.allUserProjectsId.push(response.ownerProjects.map(p => {
        return p.id;
      }));
    }, error => {

    }, () => {
      this.verifyEventOwner(this.userId, this.owner, this.allUserProjectsId[0], this.allUserOrganizationsId[0]);
    });
  }

  getAllUserOrganizationsId(id) {
    this.userService.getMyOrganizations(id).subscribe(response => {
      this.allUserOrganizationsId.push(response.ownerOrganizations.map(p => {
        return p.id;
      }));
    }, error => {

    }, () => {
      this.verifyEventOwner(this.userId, this.owner, this.allUserProjectsId[0], this.allUserOrganizationsId[0]);
    });
  }

  getAllMaintenance(){
    this.shortageService.getAllTimes().subscribe(
      response => {
        this.maintenance = response;
      },
      error => {
        this.utilitiesService.openSnackBar(`${error.error.message}`);
      }
    );
  }

  getFrequency(frequencyId) {
    for (const item of this.maintenance) {
      if (item.id === parseInt(frequencyId)) {
        return item.description;
      }
    }
  }

  setOwner(event: Event, userId) {
    if (event.user_id) {
      this.owner = `user_${event.user_id}`;
      this.verifyEventOwner(this.userId, this.owner, this.allUserProjectsId[0], this.allUserOrganizationsId[0]);
    }
    if (event.project_id) {
      this.owner = `project_${event.project_id}`;
      this.getAllUserProjectsId(userId);
    }
    if (event.organization_id) {
      this.owner = `organization_${event.organization_id}`;
      this.getAllUserOrganizationsId(userId);
    }
  }

  loggedUser(evento) {
    this.userService.getAccountLogged().subscribe(response => {
      this.userId = response.id;
    }, error => {
      this.utilitiesService.openSnackBar(`Error: ${error.message}`);
    }, () => {
      this.checkConfirmed();
      this.setOwner(this.event, this.userId);
    });
  }

  verifyEventOwner(userId: number, owner: string, allUserProjectsId: any, allUserOrganizationsId: any) {
    const role = owner.split('_')[0];
    // tslint:disable-next-line: radix
    const id = parseInt(owner.split('_')[1]);
    switch (role) {
      case 'user':
        if (userId === id) {
          // console.log(true);
          this.ownerMode = true;
        }
        break;
      case 'project':
        for (const projectId of allUserProjectsId) {
          if (id === projectId) {
            this.ownerMode = true;
            break;
          }
        }
        break;
      case 'organization':
        for (const organizationId of allUserOrganizationsId) {
          if (id === organizationId) {
            this.ownerMode = true;
            break;
          }
        }
        break;
    }

  }

  openDialog() {
    const receiver = {
      id: this.eventId,
      role: 'event',
    };

    if (this.ownerMode === false) {
      // Emite os valores necessários para executar a função da parceria.
      this.utilitiesService.partnershipReceiverValue = receiver;
      this.dialog.open(MyPartnersDialogComponent, {
        data: {
          shortages: this.event.shortages,
          maintenance_times: this.event.maintenance_times,
          has_shortages: this.event.has_shortages,
        }
      });
    }
  }

  confirmPresence() {
    const sendIdEvent = { event_id: this.eventId };
    this.eventService.confirmPeople(sendIdEvent).subscribe(response => {
      if (!this.isConfirmed) {
        this.utilitiesService.openSnackBar('Você confirmou sua presença no evento.');
      }
      else {
        this.utilitiesService.openSnackBar('Você removeu sua presença no evento.');
      }
    }, error => {
      this.utilitiesService.openSnackBar('Obtivemos um error ao confirmar você no evento.');
    });
  }

  getConfirmedPeople() {
    this.eventService.getConfirmedPeople(this.eventId).subscribe(response => {
      this.confirmedPeople = response;
      // console.log('Confirmed ', this.confirmedPeople);
    }, error => {
      console.log('error ao buscar pessoas confirmadas, ', error.error.message);
    });
  }

  checkConfirmed() {
    this.eventService.getConfirmedPeople(this.eventId).subscribe(response => {
      for (const confirmed of response.data) {
        if (confirmed.user_id === this.userId) {
          this.isConfirmed = true;
          break;
        }
      }
    });
  }

  handleTypeOwnerAdmin(event: Event): string {
    if (event?.organization_id !== null) { return 'Organization'; }
    if (event?.user_id !== null) { return 'User'; }
    if (event?.project_id !== null) { return 'Project'; }
  }

  handleReturnId(event: Event): number {
    if (event?.organization_id !== null) { return event?.organization_id; }
    if (event?.user_id !== null) { return event?.user_id; }
    if (event?.project_id !== null) { return event?.project_id; }
  }

}
