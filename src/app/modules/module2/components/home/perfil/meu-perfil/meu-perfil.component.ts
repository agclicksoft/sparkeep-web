import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from './../../../../../../core/services/post.service';
import { Post } from './../../../../../../shared/models/post.model';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { User } from './../../../../../../shared/models/user.model';
import { UserService } from './../../../../../../core/services/user.service';
import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-meu-perfil',
  templateUrl: './meu-perfil.component.html',
  styleUrls: ['./meu-perfil.component.css']
})
export class MeuPerfilComponent implements OnInit {

  userSession = JSON.parse(sessionStorage.getItem('user')) || JSON.parse(localStorage.getItem('user'));
  user: User;
  isLoggedUser = true;
  userLoggedId;
  id: number;

  myPosts: Post = new Post();
  page = 1;
  // TODO: CHANGE PAGE AND APPEND POSTS;

  spinnerPost = false;
  spinnerSide = false;

  isInspired = false;

  totalPartners: number;

  showMoreDescription = true;

  constructor(
    private userService: UserService,
    private utilitiesService: UtilitiesService,
    private router: Router,
    private route: ActivatedRoute,
    private ngxSpinner: NgxSpinnerService
  ) {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    // this.getUserById();
    // this.getMyPosts();
    if (this.id) {
      this.getUserById(this.id);
      this.getMyPosts(this.id);
    } else {
      this.getUserById(this.userSession.id);
      this.getMyPosts(this.userSession.id);
    }
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    const pos = document.body.scrollHeight;
    const max = window.innerHeight + window.scrollY;

    if (pos === max) {
      if (this.myPosts.page < this.myPosts.lastPage) {
        this.appendPosts();
      }
    }
  }

  getMyPosts(id) {
    this.spinnerPost = true;
    this.ngxSpinner.show();
    this.userService.getMyPosts(id, this.page).subscribe(response => {
      this.myPosts = response;
      this.page++;
      console.log('posts: ', this.myPosts);
      // console.log('response: ', response);
    }, error => {

    }, () => {
      this.ngxSpinner.hide();
      this.spinnerPost = false;
      this.spinnerSide = false;
    });
  }

  appendPosts() {
    this.userService.getMyPosts(this.userSession.id, this.page).subscribe(
      response => {
        this.myPosts.total = response.total,
          this.myPosts.page = response.page,
          this.myPosts.perPage = response.perPage,
          this.myPosts.lastPage = response.lastPage;
        this.myPosts.data = [...this.myPosts.data, ...response.data];
        // console.log('POST: ', this.posts);
      },
      error => {
        console.log(error);
      }
    );
  }

  setInspiration() {
    const inspiration = {
      user_inspiration_id: this.id,
      organization_inspiration_id: null,
    };

    this.utilitiesService.setInspiration(inspiration).subscribe(
      response => {
        this.utilitiesService.openSnackBar(`${response.status ? 'Você se inspirou no ' + this.user.name : response.message}`);
        this.isInspired = response.status;
        if (this.isInspired) {
          this.user.__meta__.total_inspireds = parseInt(this.user.__meta__.total_inspireds) + 1;
        } else {
          this.user.__meta__.total_inspireds = parseInt(this.user.__meta__.total_inspireds) - 1;
        }
      },
      error => {
        this.utilitiesService.openSnackBar(``);
      },
      () => {

      }
    );
  }

  getUserById(id) {
    this.userService.getUser(id).subscribe(response => {
      this.user = response;
      this.isInspired = response.inspired;
      // console.log('Usuário :', this.user);
      this.totalPartners =
        parseInt(this.user?.__meta__?.total_event_partners) +
        parseInt(this.user?.__meta__?.total_project_partners) +
        parseInt(this.user?.__meta__?.total_organization_partners);
    }, error => {
      this.utilitiesService.openSnackBar(`Ops, obtivemos um erro ao buscar as informações do usuário. <br/> Error: ${error.message}`);
    }, () => {
      this.getLoggedAccount();
    });
  }

  gotoEditProfile() {
    this.router.navigate(['/editar-perfil']);
  }

  getLoggedAccount() {
    this.userService.getAccountLogged().subscribe(
      response => {
        this.userLoggedId = response.id;
        response.id === this.user.id ? this.isLoggedUser = true : this.isLoggedUser = false;
      },
      error => {
        this.utilitiesService.openSnackBar('Não conseguimos identificar o usuário logado.');
      },
      () => {

      }
    );
  }

  renderDescription(data: string) {
    if (data && this.showMoreDescription) {
      return data.substring(0, 80);
    }else{
      return data;
    }
  }

}
