import { ModalDeleteUserComponent } from './../../../../../../shared/template/modal-delete-user/modal-delete-user.component';
import { MatDialog } from '@angular/material/dialog';
import { LocationService } from './../../../../../../core/services/location.service';
import { User } from './../../../../../../shared/models/user.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PassionsService } from './../../../../../../core/services/passions.service';
import { UtilitiesService } from './../../../../../../core/services/utilities.service';
import { UserService } from './../../../../../../core/services/user.service';
import { Router } from '@angular/router';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css'],
})
export class EditarPerfilComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private passionsService: PassionsService,
    private utilitiesService: UtilitiesService,
    private userService: UserService,
    private router: Router,
    private locationService: LocationService,
    public dialog: MatDialog
  ) {}

  get interestRegions(): FormArray {
    return this.formulario.get('interest_regions') as FormArray;
  }

  userSession: any = JSON.parse(sessionStorage.getItem('user'));
  user: User;

  // Input check - Dinâmico
  passions: any;
  passionsCheck: any = [
    {
      length: undefined,
    },
  ];

  imgUrlForm;
  imgRead;
  imgPost;

  locationState: any;
  locationCity: any;

  locationInterestState: any;
  locationInterestCity = [];

  isSubmited = false;

  clicked = false;

  // Select --
  bairro = null;
  cidade = null;
  estado = null;
  estadoSigla;

  interestState;
  interestSigla;

  formulario = this.formBuilder.group({
    id: [''],
    name: ['', [Validators.required, Validators.minLength(3)]],
    description: [
      '',
      [Validators.required, Validators.minLength(2), Validators.maxLength(250)],
    ],
    address_country: ['', Validators.required],
    address_zip_code: [''],
    address_city: ['', Validators.required],
    address_state: ['', Validators.required],
    address_street: ['', Validators.required],
    address_neighborhood: ['', Validators.required],
    address_complement: '',
    address_number: ['', Validators.required],
    cellphone: ['', [Validators.required]],
    telephone: ['', [Validators.required]],
    passions: [[], Validators.required],
    email: ['', [Validators.required, Validators.email]],
    show_email: [''],
    interest_region_state: [''],
    interest_region_city: [''],
    interest_regions: this.formBuilder.array([]),
    youtube: [''],
    facebook: [''],
    instagram: [''],
    twitter: [''],
    site: [''],
    receive_administrator_proposals: [''],
  });

  handleAddValidators;

  ngOnInit(): void {
    this.imgUrlForm = null;
    this.getAllStates();
    this.getUser();
  }

  getUser() {
    let interestValues;
    this.userService.getUser(this.userSession.id).subscribe(
      (Response) => {
        this.user = Response;

        this.checkImgUser(Response.image_url);
        if (
          Response.address_city === null ||
          Response.address_state === null ||
          Response.address_country === null
        ) {
          this.formulario.patchValue(Response);
          this.formulario.get('address_city').patchValue('');
          this.formulario.get('address_state').patchValue('');
          this.formulario.get('address_country').patchValue('');
        } else {
          this.formulario.patchValue(Response);
        }
      },
      (error) => {
        this.utilitiesService.openSnackBar(
          'Obtivemos um erro ao buscar o usuário.'
        );
      },
      () => {
        this.estado = this.user?.address_state;
        this.bairro = this.user?.address_neighborhood;
        this.cidade = this.user?.address_city;

        this.checkInterestRegions(this.user.interest_regions);

        this.getUserCity(this.estado);
        // this.getInterestCity(this.interestState);
      }
    );
  }

  checkInterestRegions(response: any) {
    if (response.length > 0) {
      for (let i = 1; i <= response.length; i++) {
        this.setFormGroup();
      }

      this.interestRegions.patchValue(response);

      for (let i = 0; i < this.interestRegions.length; i++) {
        this.onStateInterestChange(response[i].interest_region_state, i);
      }
    }
  }

  checkImgUser(imgUrl: string) {
    if (imgUrl !== null) {
      this.imgUrlForm = imgUrl;
    }
  }

  envioImg(event) {
    this.imgPost = new FormData();

    const reader = new FileReader();
    this.imgRead = true;

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlForm = reader.result as string;
    };
    this.imgPost.append('attachment', event.target.files[0]);
    console.log('this.imgUrlForm: ', this.imgUrlForm);
    console.log('this.imgPost: ', this.imgPost);
  }

  uploadAttachment(attachment) {
    let res;
    if (this.imgPost) {
      this.userService.uploadAttachment(attachment).subscribe(
        (response) => {
          this.utilitiesService.openSnackBar('Imagem enviada!');
          res = response;
        },
        (error) => {
          this.utilitiesService.openSnackBar(`Error: ${error.message}`);
        },
        () => {
          this.userSession.image_url = res.image_url;
          sessionStorage.setItem('user', JSON.stringify(this.userSession));
          setTimeout(() => {
            this.router.navigate(['/global']);
          }, 900);
        }
      );
    }
  }

  handleZipcode(cep) {
    if (cep.length === 9) {
      this.utilitiesService.getCep(cep).subscribe(
        (Response) => {
          // console.log('Response: ', Response);
          this.bairro = Response.bairro;
          this.cidade = Response.localidade;
          this.estado = Response.uf;
          this.formulario.patchValue({
            address_street: Response.logradouro,
            address_neighborhood: Response.bairro,
            address_city: Response.localidade,
            address_state: Response.uf,
          });
        },
        (error) => {
          this.utilitiesService.openSnackBar('Aconteceu algum erro.');
        },
        () => {
          // Adicionar Spinner
          this.getCityByStateId(this.estado);
          this.formulario.patchValue({ address_city: this.cidade });
        }
      );
    }
  }

  onPassionChange(event) {
    event.length > 0
      ? this.formulario.controls.passions.patchValue(event.map((el) => el.id))
      : this.formulario.controls.passions.patchValue([]);
  }

  onSubmit() {
    this.isSubmited = true;
    console.log(this.formulario.value);

    if (this.formulario.status === 'VALID') {
      if (
        this.formulario.value.passions.length > 0 &&
        this.formulario.value.passions[0].id
      ) {
        this.formulario.controls.passions.patchValue(
          this.formulario.value.passions.map((el) => el.id)
        );
      }

      this.userService.updateUser(this.formulario.value).subscribe(
        (response) => {
          // console.log('Response: ', response);
          this.utilitiesService.openSnackBar('Usuário atualizado!');
          this.uploadAttachment(this.imgPost);
        },
        (error) => {
          this.utilitiesService.openSnackBar(
            'Obtivemos um erro ao atualizar o usuário!'
          );
        },
        () => {
          this.userSession.name = this.formulario.get('name').value;
          sessionStorage.setItem('user', JSON.stringify(this.userSession));
          this.router.navigate(['/global']);
        }
      );
    } else {
      this.utilitiesService.openSnackBar('Insira os dados corretamente.');
    }
  }

  getAllStates() {
    this.locationService.getStates().subscribe((response) => {
      // console.log(response);
      this.locationState = response;
      this.locationInterestState = response;
    });
  }

  onStateOptionSelected(event) {
    let idState;
    for (const item of this.locationState) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityByStateId(idState);
        break;
      }
    }
  }

  getCityByStateId(id) {
    this.locationService.getCity(id).subscribe((response) => {
      this.locationCity = response;
      // console.log('Location', response);
    });

    this.formulario.patchValue({ address_city: this.cidade });
    // console.log(this.formulario.get('address_city').value, '<== VALOR');
  }

  getUserCity(estado) {
    this.locationService.getStates().subscribe(
      (res) => {
        for (const item of res) {
          if (estado === item.sigla) {
            this.estadoSigla = item.sigla;
            break;
          }
        }
      },
      (error) => {},
      () => {
        this.getCityByStateId(this.estadoSigla);
      }
    );
  }

  getInterestCity(estado) {
    this.locationService.getStates().subscribe(
      (res) => {
        for (const item of res) {
          if (estado === item.sigla) {
            this.interestSigla = item.sigla;
            break;
          }
        }
      },
      (error) => {},
      () => {
        this.getAllCities(this.interestSigla);
      }
    );
  }

  getAllCities(municipio) {
    this.locationService.getCity(municipio).subscribe((response) => {
      this.locationInterestCity = response;
    });
  }

  removeImage() {
    this.formulario.controls.image_url.patchValue('assets/img/user.svg');
    this.imgUrlForm = 'assets/img/user.svg';
  }

  deleteUser() {
    this.dialog.open(ModalDeleteUserComponent, {
      data: {
        id: this.user.id,
        type: 'User',
      },
    });
  }

  createFormInterestRegions() {
    return this.formBuilder.group({
      interest_region_state: ['', [Validators.required]],
      interest_region_city: [''],
    });
  }

  setFormGroup() {
    this.interestRegions.push(
      this.formBuilder.group({
        interest_region_state: ['', Validators.required],
        interest_region_city: ['', Validators.required],
      })
    );
  }

  addOtherRegion(): void {
    (this.interestRegions as FormArray).push(this.createFormInterestRegions());
    this.clicked = true;
  }

  removeRegion(index): void {
    this.interestRegions.removeAt(index);
  }

  onStateInterestChange(event, index) {
    let idState;
    if (event === '') {
      for (const item of this.locationInterestState) {
        if (item.sigla === event) {
          idState = item.id;
          this.getCityInterestById(idState);
          break;
        }
      }
    }
    if (event !== '') {
      this.locationInterestCity[index] = null;
      for (const item of this.locationInterestState) {
        if (item.sigla === event) {
          idState = item.id;
          this.locationService.getCity(idState).subscribe((response) => {
            this.locationInterestCity[index] = response;
          });
          break;
        }
      }
    }
  }

  getCityInterestById(id) {
    this.locationService.getCity(id).subscribe((response) => {
      this.locationInterestCity.push(response);
    });
  }

  handleRequireOrNotInput(data) {
    this.handleAddAllValidators();

    switch (data) {
      case 'telephone': {
        if (this.formulario.get('telephone').valid) {
          this.handleClearValidators('cellphone');
          this.handleClearValidators('email');
        }
        break;
      }
      case 'cellphone': {
        if (this.formulario.get('cellphone').valid) {
          this.handleClearValidators('telephone');
          this.handleClearValidators('email');
        }
        break;
      }
      case 'email': {
        if (this.formulario.get('email').valid) {
          this.handleClearValidators('telephone');
          this.handleClearValidators('cellphone');
        }
      }

      default: {
        break;
      }
    }
  }

  handleClearValidators = (value: string) => {
    this.formulario.get(value).clearValidators();
    this.formulario.get(value).updateValueAndValidity();
  };

  handleAddAllValidators = () => {
    this.formulario.get('telephone').setValidators([Validators.required]);
    this.formulario.get('telephone').updateValueAndValidity();
    this.formulario.get('cellphone').setValidators([Validators.required]);
    this.formulario.get('cellphone').updateValueAndValidity();
    this.formulario.get('email').setValidators([Validators.required]);
    this.formulario.get('email').updateValueAndValidity();
  };

  checkAndSetValidation() {
    if (this.formulario.get('email').value) {
      this.formulario
        .get('email')
        .setValidators([Validators.required, Validators.email]);
      this.formulario.get('telephone').clearValidators();
      this.formulario.get('cellphone').clearValidators();
    }
    if (this.formulario.get('telephone').value) {
      this.formulario.get('telephone').setValidators([Validators.required]);
      this.formulario.get('email').clearValidators();
      this.formulario.get('cellphone').clearValidators();
    }
    if (this.formulario.get('cellphone').value) {
      this.formulario.get('cellphone').setValidators([Validators.required]);
      this.formulario.get('email').clearValidators();
      this.formulario.get('telephone').clearValidators();
    }
    this.formulario.get('cellphone').updateValueAndValidity();
    this.formulario.get('email').updateValueAndValidity();
    this.formulario.get('telephone').updateValueAndValidity();
  }

  get interest_regions(): FormArray{
    return this.formulario.get('interest_regions') as FormArray;
  }
}
