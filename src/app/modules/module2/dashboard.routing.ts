import { PesCarenciasComponent } from './components/home/pesquisar/pes-carencias/pes-carencias.component';
import { PesPaixoesComponent } from './components/home/pesquisar/pes-paixoes/pes-paixoes.component';
import { PesLocalizacaoComponent } from './components/home/pesquisar/pes-localizacao/pes-localizacao.component';
import { PesEventosComponent } from './components/home/pesquisar/pes-eventos/pes-eventos.component';
import { PesProjetosComponent } from './components/home/pesquisar/pes-projetos/pes-projetos.component';
import { PesEntidadesComponent } from './components/home/pesquisar/pes-entidades/pes-entidades.component';
import { PesPessoasComponent } from './components/home/pesquisar/pes-pessoas/pes-pessoas.component';
import { PesquisarComponent } from './components/home/pesquisar/pesquisar.component';
import { ChatPartnershipComponent } from './../../shared/template/chat-partnership/chat-partnership.component';
import { VerEntidadeComponent } from './components/home/entidades/ver-entidade/ver-entidade.component';
import { VerEventoComponent } from './components/home/evento/ver-evento/ver-evento.component';
import { MeuPerfilComponent } from './components/home/perfil/meu-perfil/meu-perfil.component';
import { MinhasParceriasComponent } from './components/home/parcerias/minhas-parcerias/minhas-parcerias.component';
import { CriarEntidadeComponent } from './components/home/entidades/criar-entidade/criar-entidade.component';
import { MinhasEntidadesComponent } from './components/home/entidades/minhas-entidades/minhas-entidades.component';
import { CriarProjetoComponent } from './components/home/projeto/criar-projeto/criar-projeto.component';
import { MeusProjetosComponent } from './components/home/projeto/meus-projetos/meus-projetos.component';
import { CriarEventoComponent } from './components/home/evento/criar-evento/criar-evento.component';
import { MeusEventosComponent } from './components/home/evento/meus-eventos/meus-eventos.component';
import { EditarPerfilComponent } from './components/home/perfil/editar-perfil/editar-perfil.component';
import { MyInterestsComponent } from './components/menu-options/my-interests/my-interests.component';
import { MyNetworkComponent } from './components/menu-options/my-network/my-network.component';
import { GlobalComponent } from './components/menu-options/global/global.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { VisualizarProjetoComponent } from './components/home/projeto/visualizar-projeto/visualizar-projeto.component';
import { NotificacoesComponent } from './components/home/notificacoes/notificacoes.component';
import { MinhaRedeComponent } from './components/home/minha-rede/minha-rede.component';
import { PessoasComponent } from './components/home/minha-rede/pessoas/pessoas.component';
import { EntidadesComponent } from './components/home/minha-rede/entidades/entidades.component';
import { ProjetosComponent } from './components/home/minha-rede/projetos/projetos.component';
import { EventosComponent } from './components/home/minha-rede/eventos/eventos.component';
// Components ^

// Imports principais.
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // {path: '', component: LoginComponent},
  // {path: 'home', component: HomeComponent},
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'global'
      },
      // Posts
      {
        path: 'global',
        component: GlobalComponent
      },
      {
        path: 'minha-rede',
        component: MyNetworkComponent,
      },
      {
        path: 'meus-interesses',
        component: MyInterestsComponent,
      },

      // Perfil
      {
        path: 'perfil',
        component: MeuPerfilComponent
      },
      {
        path: 'perfil/:id',
        component: MeuPerfilComponent
      },
      {
        path: 'editar-perfil',
        component: EditarPerfilComponent
      },

      // Eventos
      {
        path: 'eventos',
        component: MeusEventosComponent
      },
      {
        path: 'criar-evento',
        component: CriarEventoComponent
      },
      {
        path: 'evento/:id',
        component: VerEventoComponent
      },
      {
        path: 'evento/:id/editar',
        component: CriarEventoComponent
      },

      // Projetos
      {
        path: 'projetos',
        component: MeusProjetosComponent
      },
      {
        path: 'projeto/:id',
        component: VisualizarProjetoComponent
      },
      {
        path: 'projeto/:id/editar',
        component: CriarProjetoComponent
      },
      {
        path: 'criar-projeto',
        component: CriarProjetoComponent
      },

      // Entidades
      {
        path: 'entidades',
        component: MinhasEntidadesComponent
      },
      {
        path: 'registrar-entidade',
        component: CriarEntidadeComponent
      },
      {
        path: 'entidade/:id',
        component: VerEntidadeComponent
      },
      {
        path: 'entidade/:id/editar',
        component: CriarEntidadeComponent
      },

      // Parcerias
      {
        path: 'parcerias',
        component: MinhasParceriasComponent
      },

      // Chat
      {
          path: 'chat',
          component: ChatPartnershipComponent
      },
      // Pesquisar
      {
        path: 'pesquisar',
        component: PesquisarComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'pessoas'
          },
          {
            path: 'pessoas',
            component: PesPessoasComponent
          },
          {
            path: 'entidades',
            component: PesEntidadesComponent
          },
          {
            path: 'projetos',
            component: PesProjetosComponent
          },
          {
            path: 'eventos',
            component: PesEventosComponent
          },
          {
            path: 'localizacao',
            component: PesLocalizacaoComponent
          },
          {
            path: 'paixoes',
            component: PesPaixoesComponent
          },
          {
            path: 'demandas',
            component: PesCarenciasComponent
          },
        ]
      },
      {
        path: 'notificacao',
        component: NotificacoesComponent,
      },
      // Minha Rede
      {
        path: 'rede',
        component: MinhaRedeComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'pessoas'
          },
          {
            path: 'pessoas',
            component: PessoasComponent,
          },
          {
            path: 'entidades',
            component: EntidadesComponent,
          },
          {
            path: 'projetos',
            component: ProjetosComponent,
          },
          {
            path: 'eventos',
            component: EventosComponent,
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
