import { NgxPaginationModule } from 'ngx-pagination';
// Modulos comuns
import { DashboardRoutingModule } from './dashboard.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Import SharedModule
import { SharedModule } from '../../shared/shared.module';

// Components
import { HomeComponent } from './components/home/home.component';
import { GlobalComponent } from './components/menu-options/global/global.component';
import { MyInterestsComponent } from './components/menu-options/my-interests/my-interests.component';
import { MyNetworkComponent } from './components/menu-options/my-network/my-network.component';
import { MenuComponent } from './components/menu-options/menu/menu.component';
import { RadarComponent } from './components/menu-options/radar/radar.component';
import { EditarPerfilComponent } from './components/home/perfil/editar-perfil/editar-perfil.component';
import { MeuPerfilComponent } from './components/home/perfil/meu-perfil/meu-perfil.component';
import { MeusEventosComponent } from './components/home/evento/meus-eventos/meus-eventos.component';
import { CriarEventoComponent } from './components/home/evento/criar-evento/criar-evento.component';
import { MinhasEntidadesComponent } from './components/home/entidades/minhas-entidades/minhas-entidades.component';
import { CriarEntidadeComponent } from './components/home/entidades/criar-entidade/criar-entidade.component';
import { MeusProjetosComponent } from './components/home/projeto/meus-projetos/meus-projetos.component';
import { CriarProjetoComponent } from './components/home/projeto/criar-projeto/criar-projeto.component';
import { MinhasParceriasComponent } from './components/home/parcerias/minhas-parcerias/minhas-parcerias.component';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutosizeModule } from 'ngx-autosize';
import { VerEventoComponent } from './components/home/evento/ver-evento/ver-evento.component';
import { VisualizarProjetoComponent } from './components/home/projeto/visualizar-projeto/visualizar-projeto.component';
import { VerEntidadeComponent } from './components/home/entidades/ver-entidade/ver-entidade.component';
import { NotificacoesComponent } from './components/home/notificacoes/notificacoes.component';
import { MinhaRedeComponent } from './components/home/minha-rede/minha-rede.component';
import { PessoasComponent } from './components/home/minha-rede/pessoas/pessoas.component';
import { EntidadesComponent } from './components/home/minha-rede/entidades/entidades.component';
import { ProjetosComponent } from './components/home/minha-rede/projetos/projetos.component';
import { EventosComponent } from './components/home/minha-rede/eventos/eventos.component';
import { PesquisarComponent } from './components/home/pesquisar/pesquisar.component';
import { PesLocalizacaoComponent } from './components/home/pesquisar/pes-localizacao/pes-localizacao.component';
import { PesPaixoesComponent } from './components/home/pesquisar/pes-paixoes/pes-paixoes.component';
import { PesPessoasComponent } from './components/home/pesquisar/pes-pessoas/pes-pessoas.component';
import { PesEntidadesComponent } from './components/home/pesquisar/pes-entidades/pes-entidades.component';
import { PesProjetosComponent } from './components/home/pesquisar/pes-projetos/pes-projetos.component';
import { PesEventosComponent } from './components/home/pesquisar/pes-eventos/pes-eventos.component';
import { PesCarenciasComponent } from './components/home/pesquisar/pes-carencias/pes-carencias.component';


@NgModule({
  imports: [
    DashboardRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxPhoneMaskBrModule,
    NgSelectModule,
    AutosizeModule,
    NgxPaginationModule
  ],
  declarations: [
    HomeComponent,
    GlobalComponent,
    MyInterestsComponent,
    MyNetworkComponent,
    MenuComponent,
    RadarComponent,
    EditarPerfilComponent,
    MeuPerfilComponent,
    MeusEventosComponent,
    CriarEventoComponent,
    MinhasEntidadesComponent,
    CriarEntidadeComponent,
    MeusProjetosComponent,
    CriarProjetoComponent,
    MinhasParceriasComponent,
    VerEventoComponent,
    VisualizarProjetoComponent,
    VerEntidadeComponent,
    PesquisarComponent,
    NotificacoesComponent,
    MinhaRedeComponent,
    PessoasComponent,
    EntidadesComponent,
    ProjetosComponent,
    EventosComponent,
    PesLocalizacaoComponent,
    PesPaixoesComponent,
    PesPessoasComponent,
    PesEntidadesComponent,
    PesProjetosComponent,
    PesEventosComponent,
    PesCarenciasComponent,
  ],
  providers: [

  ],
})
export class DashboardModule { }
