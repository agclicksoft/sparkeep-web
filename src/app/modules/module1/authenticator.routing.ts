import { PostagemComponent } from './components/postagem/postagem.component';
import { CompleteCadastroComponent } from './components/complete-cadastro/complete-cadastro.component';
import { NovaSenhaComponent } from './components/nova-senha/nova-senha.component';
import { EsqueciSenhaComponent } from './components/esqueci-senha/esqueci-senha.component';
import { CadastrarComponent } from './components/cadastrar/cadastrar.component';
import { LoginComponent } from './components/login/login.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ValidaCodigoComponent } from './components/valida-codigo/valida-codigo.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'cadastrar', component: CadastrarComponent},
  {path: 'esqueci', component: EsqueciSenhaComponent},
  {path: 'nova-senha', component: NovaSenhaComponent},
  {path: 'codigo', component: ValidaCodigoComponent},
  {path: 'complete-cadastro', component: CompleteCadastroComponent},
  {path: 'postagem/:id', component: PostagemComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticatorRoutingModule { }
