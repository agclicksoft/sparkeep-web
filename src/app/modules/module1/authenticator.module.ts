import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
// Modulos comuns
import { AuthenticatorRoutingModule } from './authenticator.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { EsqueciSenhaComponent } from './components/esqueci-senha/esqueci-senha.component';
import { LoginComponent } from './components/login/login.component';
import { CadastrarComponent } from './components/cadastrar/cadastrar.component';
import { NovaSenhaComponent } from './components/nova-senha/nova-senha.component';
import { CompleteCadastroComponent } from './components/complete-cadastro/complete-cadastro.component';
import { ValidaCodigoComponent } from './components/valida-codigo/valida-codigo.component';
import { PostagemComponent } from './components/postagem/postagem.component';

// Import SharedModule
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    AuthenticatorRoutingModule,
    CommonModule,
    NgxPhoneMaskBrModule,
    SharedModule,
  ],
  declarations: [
    LoginComponent,
    CadastrarComponent,
    EsqueciSenhaComponent,
    NovaSenhaComponent,
    CompleteCadastroComponent,
    ValidaCodigoComponent,
    PostagemComponent,
  ],
  providers: [
  ],
})
export class AuthenticatorModule { }
