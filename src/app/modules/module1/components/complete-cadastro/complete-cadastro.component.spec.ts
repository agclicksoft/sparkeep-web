import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteCadastroComponent } from './complete-cadastro.component';

describe('CompleteCadastroComponent', () => {
  let component: CompleteCadastroComponent;
  let fixture: ComponentFixture<CompleteCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
