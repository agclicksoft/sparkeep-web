import { AuthService } from './../../../../core/authentication/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-valida-codigo',
  templateUrl: './valida-codigo.component.html',
  styleUrls: ['./valida-codigo.component.css']
})
export class ValidaCodigoComponent implements OnInit {

  formulario: FormGroup;
  json = {
    email: '',
    code: '',
  };

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      code: ['', [Validators.required]],
    });
  }

  onSubmit(){
    if (this.formulario.status === 'VALID'){

      this.json = {
        email: sessionStorage.getItem('email'),
        code: this.formulario.get('code').value,
      };

      this.authService.verifyCode(this.json);
    }
  }

}
