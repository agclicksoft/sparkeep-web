import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidaCodigoComponent } from './valida-codigo.component';

describe('ValidaCodigoComponent', () => {
  let component: ValidaCodigoComponent;
  let fixture: ComponentFixture<ValidaCodigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidaCodigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidaCodigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
