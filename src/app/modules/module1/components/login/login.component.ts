import { TermsPoliciesAboutComponent } from './../../../../shared/template/terms-policies-about/terms-policies-about.component';

import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthService } from './../../../../core/authentication/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// import social login
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formulario: FormGroup;

  userSocial: SocialUser;
  loggedIn: boolean;

  // Serve para buscar dados da sessao do navegador e retorna em JSON para exibição funcionando Ex. userSession.id , userSession.name etc...
  // jsonSession = sessionStorage.getItem('user');
  // userSession = JSON.parse(this.jsonSession);

  constructor(
      private formBuilder: FormBuilder,
      private authServiceSocial: SocialAuthService,
      private authService: AuthService,
      private ngxSpinner: NgxSpinnerService,
      private router: Router,
      private dialog: MatDialog,
    ) { }

  ngOnInit(): void {

    this.formulario = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]],
    });

    this.authServiceSocial.authState.subscribe(userSocial => {
      this.userSocial = userSocial;
      this.loggedIn = (userSocial != null);
      if (this.userSocial){
        this.authService.authSocial(this.userSocial);
        this.authService.openSnackBar('Sendo redirecionado.');
      }
    },
     error => {
       console.log('Error: ', error);
     }
    );

  }

  onSubmit() {
    if (this.formulario.status === 'VALID') {
      this.authService.logar(this.formulario.value);
    } else {
      console.log('formulario inválido.');
    }
  }

  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }

  openModalAbout() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isAbout: true
      }
    });
  }

}
