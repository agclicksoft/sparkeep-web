import { MatDialog } from '@angular/material/dialog';
import { AuthService } from './../../../../core/authentication/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// import social login
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { TermsPoliciesAboutComponent } from 'src/app/shared/template/terms-policies-about/terms-policies-about.component';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {

  formulario: FormGroup;

  userSocial: SocialUser;
  loggedIn: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authServiceSocial: SocialAuthService,
    private authService: AuthService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(32)]],
    });

    this.authServiceSocial.authState.subscribe(userSocial => {
      this.userSocial = userSocial;
      console.log(this.userSocial);
      this.loggedIn = (userSocial != null);
      if (this.userSocial) {
        this.authService.authSocial(this.userSocial);
      }
    });
  }

  onSubmit() {
    if (this.formulario.status === 'VALID') {
      console.log(this.formulario.value);
      this.authService.cadastrar(this.formulario.value);
    } else {
      console.log('formulario inválido.');
    }
  }

  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }

  openPolicies() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isPolicies: true
      }
    });
  }

  openTerms() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isTerms: true
      }
    });
  }

  openModalAbout() {
    this.dialog.open(TermsPoliciesAboutComponent, {
      data: {
        isAbout: true
      }
    });
  }
}
