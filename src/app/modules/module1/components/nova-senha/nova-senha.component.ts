import { AuthService } from './../../../../core/authentication/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nova-senha',
  templateUrl: './nova-senha.component.html',
  styleUrls: ['./nova-senha.component.css']
})
export class NovaSenhaComponent implements OnInit {

  formulario: FormGroup;
  id = '';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.formulario = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(18)]],
    });
    this.id = sessionStorage.getItem('id');
  }

  onSubmit() {
    if (this.formulario.status === 'VALID') {

      // Split - diferenciando ID e Token || VERSÃO ANTIGA.
      // console.log('Formulário.getCode: ', this.formulario.get('code').value);
      // console.log('ID -- : ', this.id);

      // this.id = this.formulario.get('code').value.split('-');

      // console.log('ID: ', this.id[0]);
      // console.log('TOKEN: ', this.id[1]);

      console.log('valor: ', this.formulario.value);

      if (this.formulario.get('password').value === this.formulario.get('confirmPassword').value) {
        this.authService.resetPassword(this.formulario.value);
      } else {
        this.authService.openSnackBar('As senhas não conferem.');
      }

    } else {
      this.authService.openSnackBar('Formulário inválido !');
      setTimeout(() => {
        this.authService.openSnackBar('As senhas devem possuir tamanho 6 ou maior.');
      }, 1500);
    }
  }
}
