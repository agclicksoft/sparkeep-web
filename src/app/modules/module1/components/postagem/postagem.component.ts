import { AuthService } from './../../../../core/authentication/auth.service';
import { ModalSignInComponent } from './../../../../shared/template/modal-sign-in/modal-sign-in.component';
import { ShareExternallyComponent } from 'src/app/shared/template/share-externally/share-externally.component';
import { ComplaintPostComponent } from 'src/app/shared/template/complaint-post/complaint-post.component';
import { MatDialog } from '@angular/material/dialog';
import { PostService } from '../../../../core/services/post.service';
import { User } from 'src/app/shared/models/user.model';
import { Post } from 'src/app/shared/models/post.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from '../../../../core/services/utilities.service';
import { UserService } from 'src/app/core/services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-postagem',
  templateUrl: './postagem.component.html',
  styleUrls: ['./postagem.component.css']
})
export class PostagemComponent implements OnInit {

  id: number;
  user: User;
  myPosts: Post = new Post();
  spinnerPost = false;
  totalPartners: number;

  reported: boolean;

  POST_LIKE: boolean = false;

  img_clap = '../../../../../assets/svg/clap.svg';
  img_clap_roxo = '../../../../../assets/svg/clap-roxo.svg';

  constructor(
    private utilitiesService: UtilitiesService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private ngxSpinner: NgxSpinnerService,
    private postService: PostService,
    private userService: UserService,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.authService.USER_POST_LIKE = false;
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.getPost();
    this.getUserLogged();
  }

  getPost() {
    this.postService.getById(this.id).subscribe(
      response => {
        // console.log('POSTAGEM PUBLICA !!!!', response);
        this.myPosts = response;
      },
      error => {
        this.utilitiesService.openSnackBar(`Não foi possível exibir essa postagem.`);
      }
    );
  }

  clickClaps() {
    if (this.authService.USER_POST_LIKE) {
      this.likePost(this.id);
    } else {
      this.utilitiesService.openSnackBar(`Você precisa está logado para curtir uma publicação.`);
      this.openSignIn();
    }
  }

  likePost(id) {
    this.postService.likePost(id).subscribe(response => {
      let status;

      if (response.message === 'like') {
        status = 'Curtido';
        this.POST_LIKE = true;
        this.myPosts.__meta__.total_inspireds = parseInt(this.myPosts.__meta__.total_inspireds) + 1;
      } else {
        status = 'Descurtido';
        this.POST_LIKE = false;
        this.myPosts.__meta__.total_inspireds = parseInt(this.myPosts.__meta__.total_inspireds) - 1;
      }

      this.utilitiesService.openSnackBar(`${status}`);
      // console.log(response);
    }, error => {
      this.utilitiesService.openSnackBar(`${error.error}`);
    });
  }

  openDialog(post, event) {
    this.dialog.open(ComplaintPostComponent, {
      data: {
        post,
        event
      },
      maxWidth: '580px',
      width: '100%'
    });
  }

  openSignIn() {
    this.dialog.open(ModalSignInComponent, {
      maxWidth: '580px',
      width: '100%'
    });
  }

  sharePost(id) {
    this.utilitiesService.idPostShare = id;
    this.dialog.open(ShareExternallyComponent);
  }

  getUserLogged() {
    this.userService.getAccountLogged().subscribe(
      response => {
        this.user = response;
        this.authService.USER_POST_LIKE = true;
        // console.log('usuário ===>', this.user);
      },
      error => {
        this.utilitiesService.openSnackBar(`Usuário não logado, realize login para conseguir realizar ações!`)
        this.authService.USER_POST_LIKE = false;
        // this.utilitiesService.openSnackBar(`${error.error.message}`);
      }
    );
  }
}
