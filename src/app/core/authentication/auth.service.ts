import { NgxSpinnerService } from 'ngx-spinner';
import { SocialAuthService } from 'angularx-social-login';
import { Injectable, EventEmitter } from '@angular/core';

import { User } from '../../shared/models/user.model';
import { environment } from '../../../environments/environment';

import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { Session } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private SESSION_LIMIT_EXPIRES = 15;
  public USER_POST_LIKE;

  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private http: HttpClient,
    private authSocialService: SocialAuthService,
    private ngxSpinner: NgxSpinnerService,
  ) { }

  setSession(response, ambiente, type?) {
    let json;
    // Ambiente 'Local' utiliza os dados já do sparkeep
    if (ambiente === 'local') {
      json = {
        id: response.user.id,
        token: response.data.token,
        name: response.user.name,
        image_url: response.user.image_url,
        email: response.user.email,
        expiresAt: JSON.stringify(moment().add(this.SESSION_LIMIT_EXPIRES, 'minutes').valueOf())
      };
      sessionStorage.setItem('user', JSON.stringify(json));
    }
    // Ambiente 'social' utiliza os dados das redes sociais para se conectar.
    if (ambiente === 'social') {
      if (!!response.findUser) {
        json = {
          id: response.findUser.id,
          token: response.data.token,
          name: response.findUser.name,
          image_url: response.findUser.image_url,
          email: response.findUser.email,
          expiresAt: JSON.stringify(moment().add(this.SESSION_LIMIT_EXPIRES, 'minutes').valueOf())
        };
      } else {
        json = {
          id: response.user.id,
          token: response.data.token,
          name: response.user.name,
          image_url: response.user.image_url,
          email: response.user.email,
          expiresAt: JSON.stringify(moment().add(this.SESSION_LIMIT_EXPIRES, 'minutes').valueOf())
        };
      }
      sessionStorage.setItem('user', JSON.stringify(json));
      localStorage.setItem('user', JSON.stringify(json));
      // this.logar(json);
      if(type === 'modal'){
        return true;
      }

      if (response.findUser?.accepted_terms) {
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/complete-cadastro']);
      }

    }
  }

  logar(user: any, type?) {
    this.ngxSpinner.show();
    return this.http.post<any>(`${environment.API}/auth/signin`, user).subscribe(Response => {

      this.setSession(Response, 'local', type);
      /*
        Seta a resposta do servidor e o usuario recebido, passando como parametro para a Sessão.
      */
      this.openSnackBar('Usuário logado com sucesso !!!');
      console.log('Valor do login: ', Response);

      if(type){
        return true;
      }

      if (Response.user.accepted_terms) {
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/complete-cadastro']);
      }
    },
      error => {
        // console.log(error.error.text);
        this.ngxSpinner.hide();
        let message:string = '';

        if (!!error.error.text){
          message = error.error.text.split(':', 2)
          if (message[0] === 'E_USER_NOT_FOUND') {
            this.openSnackBar(`Usuário não encontrado!`);
          }else{
            this.openSnackBar(`Sinto muito, Não foi possivel Logar!`);
          }
        }else{
          this.openSnackBar(`Sinto muito, alguma coisa não parece está certa, verifique todos os campos!`);
        }
      },
      () => {
        this.ngxSpinner.hide();
      }
    );
  }

  authSocial(user, type?) {
    let resposta; // Testar codigo comentado.

    return this.http.post<any>(`${environment.API}/social/login`, user).subscribe(
      Response => {
        // resposta = Response; // Pega a resposta do servidor
        // resposta.findUser.image_url = user.photoUrl; // Seta a img na resposta para a sessão.
        resposta = Response;
        console.log('Response - AuthSocial', resposta);
        this.setSession(resposta, 'social', type);

        if(type){
          return true;
        }
      },
      error => {
        console.log('Sinto muito, ', error);
      },
      () => {
        this.openSnackBar('Usuário logado com sucesso !!!');
        // if (resposta.findUser.accepted_terms) {
        //   this.router.navigate(['/']);
        // } else {
        //   this.router.navigate(['/complete-cadastro']);
        // }
      }
    );
  }

  cadastrar(user: User) {
    return this.http.post<any>(`${environment.API}/auth/signup`, user).subscribe(Response => {
      console.log('Resposta do servidor: ', Response);
      this.openSnackBar('Usuário criado com sucesso!');
      this.router.navigate(['/login']);
    },
      error => {
        if (error.error.message === 'email já cadastrado') {
          this.openSnackBar(`E-mail já cadastrado!`);
        } else {
          this.openSnackBar(`Sinto muito, não foi possível criar o usuário!`);
        }
      },
      () => {
        return this.logar(user);
      }
    );
  }

  logout() {
    this.authSocialService.signOut();
    sessionStorage.clear();
    this.openSnackBar('Saindo...');
    setTimeout(() => {
      this.openSnackBar('Usuário deslogado!');
      this.router.navigate(['/login']);
    }, 900);
    // this.usuarioAutenticado = false;
    // this.mostrarMenuEmitter.emit(false);
  }

  // usuarioEstaAutenticado() { // GUARD -- Verifica se o usuário já está logado.
  //   if (sessionStorage.getItem('token')) {
  //     return true;
  //   }
  // }

  // verficarSenha(user: any) {
  //   if (this.http.post<any>(`${environment.API}/auth/signin`, user).subscribe(Response => {
  //     console.log();
  //   })
  //   ) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }



  forgotPassword(email) {
    return this.http.post<any>(`${environment.API}/auth/forgotpassword`, email).subscribe(Response => {
      console.log('Resposta do servidor: ', Response);
      this.openSnackBar(Response.message);
      this.router.navigate(['/codigo']);
      sessionStorage.setItem('email', email.email);
    },
      error => {
        console.log(error.error);
        this.openSnackBar(`Sinto muito, Não foi possível enviar o email!`);
      },
      () => {
        console.log('finally.');
      }
    );
  }

  verifyCode(value) {
    return this.http.post<any>(`${environment.API}/auth/validatecode`, value).subscribe(res => {
      this.openSnackBar('Código válido.');
      console.log('Valores: ', value);
      console.log('Resposta: ', res);

      sessionStorage.clear();
      sessionStorage.setItem('id', res.id);
      this.router.navigate(['/nova-senha']);
    },
      error => {
        //  console.log(error);
        this.openSnackBar('Sinto muito, não foi possível verificar o código.');
      },
      () => {
        console.log('finally');

      }
    );
  }

  resetPassword(password) {
    return this.http.post<any>(`${environment.API}/auth/resetpassword/?id=${sessionStorage.getItem('id')}`, password)
      .subscribe(Response => {
        console.log('Resposta do servidor: ', Response);
        this.openSnackBar('Senha alterada com sucesso!');
        this.router.navigate(['/']);
        sessionStorage.clear();
      },
        error => {
          console.log(error.error);
          this.openSnackBar(`Sinto muito, não foi possível alterar a senha!`);
        },
        () => {
          console.log('finally');
        }
      );
  }

  openSnackBar(mensagem) {
    this.snackBar.open(mensagem, 'X', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
}
