import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  public myEventsReload = new EventEmitter<any>();
  public myProjectsReload = new EventEmitter<any>();
  public myOrganizationsReload = new EventEmitter<any>();

  searchInMyNetwork = new EventEmitter<any>();
  globalSearch = new EventEmitter<any>();

  // enviar os valores para identificar se é evento / projeto / entidade / organização.
  partnershipReceiverValue: any;
  // envia o id para compartilhar a postagem...
  idPostShare: any;

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ) { }

  getCep(cep): Observable<any>{
    return this.http.get<any>(`https://viacep.com.br/ws/${cep}/json/`);
  }

  setInspiration(inspiration): Observable<any>{
    return this.http.post<any>(`${environment.API}/inspires`, inspiration);
  }

  openSnackBar(mensagem) {
    this.snackBar.open(mensagem, 'X', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
}
