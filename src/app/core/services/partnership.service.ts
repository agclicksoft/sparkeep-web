import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PartnershipService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(page, keyword): Observable<any> {
    return this.http.get<any>(`${environment.API}/partners?keyword=${keyword}&page=${page}`);
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${environment.API}/partners/${id}`);
  }

  update(id, object): Observable<any> {
    return this.http.put<any>(`${environment.API}/partners/${id}`, object);
  }

  create(object): Observable<any> {
    return this.http.post<any>(`${environment.API}/partners`, object);
  }

  chat(id, keyword, userPage, eventPage, projectPage, organizationPage): Observable<any> {
    return this.http.get<any>(`${environment.API}/users/${id}/request-partners?userPerpage=8&eventPerpage=8&projectPerpage=8&organizationPerpage=8&userPage=${userPage}&eventPage=${eventPage}&projectPage=${projectPage}&organizationPage=${organizationPage}&status=0&keyword=${keyword}`);
  }

  acceptAll(partnersIds){
    return this.http.post<any>(`${environment.API}/partners/accept-all`, partnersIds);
  }

  rejectAll(partnersIds){
    return this.http.post<any>(`${environment.API}/partners/reject-all`, partnersIds);
  }

  favorite(object){
    return this.http.post<any>(`${environment.API}/partners/favorite`, object);
  }
}
