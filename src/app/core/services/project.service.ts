import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(
    private http: HttpClient
  ) { }

  getAllProjects(keyword, page): Observable <any>{
    return this.http.get<any>(`${environment.API}/projects?keyword=${keyword}&page=${page}&perpage=9`);
  }

  store(project) {
    return this.http.post<any>(`${environment.API}/projects`, project);
  }

  getProject(id) {
    return this.http.get<any>(`${environment.API}/projects/${id}`);
  }

  update(id, project) {
    return this.http.put<any>(`${environment.API}/projects/${id}`, project);
  }

  getPosts(id, page) {
    return this.http.get<any>(`${environment.API}/projects/${id}/posts?page=${page}`);
  }

  uploadAttachment(id, attachment) {
    return this.http.post<any>(`${environment.API}/projects/${id}/attachment`, attachment);
  }

  getMyProjects(id): Observable<any>{
    return this.http.get<any>(`${environment.API}/users/${id}/projects`);
  }

  getProjectEvents(id) {
    return this.http.get<any>(`${environment.API}/projects/${id}/events`);
  }

  getProjectPartners(id) {
    return this.http.get<any>(`${environment.API}/projects/${id}/partners`);
  }

  delete(id){
    return this.http.delete<any>(`${environment.API}/organizations/${id}`);
  }
}
