import { Post } from './../../shared/models/post.model';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject, } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  profile = new EventEmitter<any>();
  public addPost = new BehaviorSubject<Post>(null);

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ){  }

  getAll(): Observable<any[]>{
    return this.http.get<any[]>(`${environment.API}/posts`);
  }

  getById(id): Observable<Post>{
    return this.http.get<Post>(`${environment.API}/posts/${id}`);
  }

  delete(id){
    return this.http.delete<any>(`${environment.API}/posts/${id}`);
  }

  update(post: Post): Observable<Post>{
    return this.http.put<Post>(`${environment.API}/posts/${post.id}`, post);
  }

  create(post: Post): Observable<any>{
    return this.http.post<any>(`${environment.API}/posts`, post);
  }

  updateImg(idPost, img): Observable<Post>{
    return this.http.post<Post>(`${environment.API}/posts/${idPost}/uploadfiles`, img);
  }

  // VER-TUDO = Vazio * PESSOAS = user * ENTIDADES = organization * PROJETO = project * EVENTO = event
  // My Interests --
  getMyInterest(filtro, pagina): Observable<Post>{
    return this.http.get<Post>(`${environment.API}/posts/my-interests?profile=${filtro}&page=${pagina}`);
  }
  // My Network --
  getMyNetwork(filtro, pagina): Observable<Post>{
    return this.http.get<Post>(`${environment.API}/posts/my-network?profile=${filtro}&page=${pagina}`);
  }
  // Global --
  getGlobal(filtro, pagina): Observable<Post>{
    return this.http.get<Post>(`${environment.API}/posts/global?profile=${filtro}&page=${pagina}`);
  }

  // Curtir Post
  likePost(id): Observable<any>{
    return this.http.post<any>(`${environment.API}/posts/${id}/like`, '');
  }

  // Denunciar Post
  reportPost(id, data): Observable<any> {
    return this.http.post<any>(`${environment.API}/posts/${id}/report`, data);
  }

  openSnackBar(mensagem) {
    this.snackBar.open(mensagem, 'X', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
}
