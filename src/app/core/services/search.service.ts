import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient,
  ) { }

  getLocations(object, keyword, userPage, eventPage, projectPage, organizationPage): Observable<any>{
    return this.http.post<any>(`${environment.API}/search/location?keyword=${keyword}&userPage=${userPage}&userPerpage=3&eventPage=${eventPage}&eventPerpage=3&projectPage=${projectPage}&projectPerpage=3&organizationPage=${organizationPage}&organizationPerpage=3`, object);
  }

  getShortages(object, keyword, eventPage, projectPage): Observable<any>{
    return this.http.post<any>(`${environment.API}/search/shortages?keyword=${keyword}&eventPage=${eventPage}&eventPerpage=3&projectPage=${projectPage}&projectPerpage=3`, object);
  }

  getPassions(object, keyword, userPage, eventPage, projectPage, organizationPage): Observable<any>{
    return this.http.post<any>(`${environment.API}/search/passions?keyword=${keyword}&userPage=${userPage}&userPerpage=3&eventPage=${eventPage}&eventPerpage=3&projectPage=${projectPage}&projectPerpage=3&organizationPage=${organizationPage}&organizationPerpage=3`, object);
  }
}
