import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PassionsService {

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ) { }

  getAll(): Observable<any>{
    return this.http.get<any>(`${environment.API}/passions`);
  }

  openSnackBar(mensagem) {
    this.snackBar.open(mensagem, 'X', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
}
