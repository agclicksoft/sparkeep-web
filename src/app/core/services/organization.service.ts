import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  getAllOrganizations(keyword, page): Observable<any>{
    return this.http.get<any>(`${environment.API}/organizations?keyword=${keyword}&page=${page}&perpage=9`);
  }

  getById(id): Observable<any>{
    return this.http.get<any>(`${environment.API}/organizations/${id}`);
  }

  getAllUserOrganizations(id): Observable<any>{
    return this.http.get<any>(`${environment.API}/users/${id}/organizations`);
  }

  getOrganizationsPosts(id): Observable<any>{
    return this.http.get<any>(`${environment.API}/organizations/${id}/posts`);
  }

  update(id, object): Observable<any>{
    return this.http.put<any>(`${environment.API}/organizations/${id}`, object);
  }

  create(object): Observable<any>{
    return this.http.post<any>(`${environment.API}/organizations`, object);
  }

  uploadAttachment(id, attachment) {
    return this.http.post<any>(`${environment.API}/organizations/${id}/attachment`, attachment);
  }

  getOrganizationProjects(id) {
    return this.http.get<any>(`${environment.API}/organizations/${id}/projects`);
  }

  getOrganizationEvents(id) {
    return this.http.get<any>(`${environment.API}/organizations/${id}/events`);
  }

  getOrganizationPartners(id) {
    return this.http.get<any>(`${environment.API}/organizations/${id}/partners`);
  }

  delete(id){
    return this.http.delete<any>(`${environment.API}/organizations/${id}`);
  }
}
