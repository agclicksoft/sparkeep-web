import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private http: HttpClient,
  ) { }

  getAllNotifications(id, page) {
    return this.http.get<any>(`${environment.API}/users/${id}/notifications?page=${page}`);
  }

  haveNotificationNotViewed(id) {
    return this.http.get<any>(`${environment.API}/users/${id}/notifications-badge`);
  }

  checkNotification(id) {
    return this.http.post<any>(`${environment.API}/users/${id}/notifications`, null);
  }

  haveChatNotificationNotViewed(id){
    return this.http.get<any>(`${environment.API}/users/${id}/chat-badge`);
  }

  checkChatNotification(id){
    return this.http.post<any>(`${environment.API}/users/${id}/chat-badge`, null);
  }

  handleAcceptOrRecuseAdministrator(user_id, role, role_id, action) {
    return this.http.put<any>(`${environment.API}/users/${user_id}/${role}/${role_id}`, action);
  }

}
