import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceTypeService {

  constructor(
    private http: HttpClient
  ) { }

  getAll() {
    return this.http.get<any>(`${environment.API}/shortages`);
  }

  getAllTimes(){
    return this.http.get<any>(`${environment.API}/maintenance-time`);
  }
}
