import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(
    private http: HttpClient
  ) { }

  getStates() {
    return this.http.get<Array<any>>('https://servicodados.ibge.gov.br/api/v1/localidades/estados');
  }

  getCity(id): Observable<any>{
    return this.http.get<any>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${id}/municipios`);
  }

}
