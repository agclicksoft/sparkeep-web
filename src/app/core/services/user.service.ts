import { Post } from './../../shared/models/post.model';
import { User } from './../../shared/models/user.model';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ) { }

  getAllUsers(keyword, page): Observable<User> {
    return this.http.get<User>(`${environment.API}/users?keyword=${keyword}&page=${page}&perpage=9`);
  }

  // getUserByName(name): Observable<User>{
  //   return this.http.get<User>(`${environment.API}/users?keyword=${name}`);
  // }

  getUser(id): Observable<User> {
    return this.http.get<User>(`${environment.API}/users/${id}`);
  }

  updateUser(user): Observable<User> {
    return this.http.put<User>(`${environment.API}/users/${user.id}`, user);
  }

  uploadAttachment(attachment) {
    return this.http.post<any>(`${environment.API}/users/attachment`, attachment);
  }

  // Get Somethings
  getMyPosts(id, page): Observable<Post> {
    return this.http.get<Post>(`${environment.API}/users/${id}/posts?page=${page}`);
  }

  getMyProjects(id): Observable<any> {
    return this.http.get<any>(`${environment.API}/users/${id}/projects`);
  }

  getMyEvents(id): Observable<any> {
    return this.http.get<any>(`${environment.API}/users/${id}/events`);
  }

  getMyOrganizations(id) {
    return this.http.get<any>(`${environment.API}/users/${id}/organizations`);
  }

  getMyPartnerships(id, keyword, userPage, eventPage, projectPage, organizationPage) {
    return this.http.get<any>(`${environment.API}/users/${id}/partners?keyword=${keyword}&userPage=${userPage}&eventPage=${eventPage}&projectPage=${projectPage}&organizationPage=${organizationPage}&userPerpage=4&eventPerpage=4&projectPerpage=4&organizationPerpage=4`);
  }

  getAccountLogged(): Observable<any> {
    return this.http.get<any>(`${environment.API}/auth/account`);
  }

  openSnackBar(mensagem) {
    this.snackBar.open(mensagem, 'X', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }

  delete(id){
    return this.http.delete<any>(`${environment.API}/users/${id}`);
  }
}
