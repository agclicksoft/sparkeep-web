import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MyNetworkService {

  constructor(
    private http: HttpClient
  ) { }

  getUsers(keyword, page) {
    return this.http.get<any>(`${environment.API}/my-network/users?keyword=${keyword}&page=${page}&perpage=9`);
  }
  getOrganizations(keyword, page) {
    return this.http.get<any>(`${environment.API}/my-network/organizations?keyword=${keyword}&page=${page}&perpage=9`);
  }
  getEvents(keyword, page) {
    return this.http.get<any>(`${environment.API}/my-network/events?keyword=${keyword}&page=${page}&perpage=9`);
  }
  getProjects(keyword, page) {
    return this.http.get<any>(`${environment.API}/my-network/projects?keyword=${keyword}&page=${page}&perpage=9`);
  }
}
