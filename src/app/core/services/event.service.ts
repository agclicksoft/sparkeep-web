import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(
    private http: HttpClient,
  ) { }

  getAllEvents(keyword, page): Observable<any>{
    return this.http.get<any>(`${environment.API}/events?keyword=${keyword}&page=${page}&perpage=9`);
  }

  getById(id): Observable<any>{
    return this.http.get<any>(`${environment.API}/events/${id}`);
  }

  getAllUserEvents(id): Observable<any>{
    return this.http.get<any>(`${environment.API}/users/${id}/events`);
  }

  update(id, object): Observable<any>{
    return this.http.put<any>(`${environment.API}/events/${id}`, object);
  }

  create(object): Observable<any>{
    return this.http.post<any>(`${environment.API}/events`, object);
  }

  uploadAttachment(id, attachment) {
    return this.http.post<any>(`${environment.API}/events/${id}/attachment`, attachment);
  }

  // Confirmar e Buscar pessoas ---
  confirmPeople(event): Observable<any>{
    return this.http.post<any>(`${environment.API}/events/${event.event_id}/confirm`, event);
  }

  getConfirmedPeople(id){
    return this.http.get<any>(`${environment.API}/events/${id}/confirmed-persons`);
  }

  delete(id){
    return this.http.delete<any>(`${environment.API}/events/${id}`);
  }

  getEventsPosts(id, page) {
    return this.http.get<any>(`${environment.API}/events/${id}/posts?page=${page}`);
  }
}
