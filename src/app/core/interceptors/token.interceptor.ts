import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private token;

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const requestUrl: Array<any> = request.url.split('/');
    const ApiURL: Array<any> = environment.API.split('/');

    if (sessionStorage.getItem('user')){
      this.token = JSON.parse(sessionStorage.getItem('user')); // PEGA O USER DE SESSION NO FORMATO STRING E TRANSFORMA EM JSON
    }else{
      this.token = JSON.parse(localStorage.getItem('user')); // PEGA O USER DE LOCAL NO FORMATO STRING E TRANSFORMA EM JSON
    }

    if (this.token && (requestUrl[2] === ApiURL[2])){
        // tslint:disable-next-line: object-literal-key-quotes
        const newRequest = request.clone({setHeaders: { 'Authorization' : `Bearer ${this.token.token}`} }); // MOSTR
        return next.handle(newRequest);
    }else{
        return next.handle(request);
    }
  }
}
