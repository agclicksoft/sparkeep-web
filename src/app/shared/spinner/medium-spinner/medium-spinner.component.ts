import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-medium-spinner',
  templateUrl: './medium-spinner.component.html',
  styleUrls: ['./medium-spinner.component.css']
})
export class MediumSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
