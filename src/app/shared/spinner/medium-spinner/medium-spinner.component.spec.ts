import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediumSpinnerComponent } from './medium-spinner.component';

describe('MediumSpinnerComponent', () => {
  let component: MediumSpinnerComponent;
  let fixture: ComponentFixture<MediumSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediumSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediumSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
