import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-partial-spinner',
  templateUrl: './partial-spinner.component.html',
  styleUrls: ['./partial-spinner.component.css']
})
export class PartialSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
