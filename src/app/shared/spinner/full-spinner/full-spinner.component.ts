import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-full-spinner',
  templateUrl: './full-spinner.component.html',
  styleUrls: ['./full-spinner.component.css']
})
export class FullSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
