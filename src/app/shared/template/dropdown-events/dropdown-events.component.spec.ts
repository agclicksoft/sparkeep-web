import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownEventsComponent } from './dropdown-events.component';

describe('DropdownEventsComponent', () => {
  let component: DropdownEventsComponent;
  let fixture: ComponentFixture<DropdownEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
