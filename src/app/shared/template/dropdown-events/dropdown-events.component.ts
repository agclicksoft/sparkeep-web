import { Router } from '@angular/router';
import { OrganizationService } from 'src/app/core/services/organization.service';
import { ProjectService } from 'src/app/core/services/project.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { UserService } from 'src/app/core/services/user.service';
import { Component, OnInit, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-dropdown-events',
  templateUrl: './dropdown-events.component.html',
  styleUrls: ['./dropdown-events.component.css']
})
export class DropdownEventsComponent implements OnInit, OnChanges {

  @Input('idUserEvent') idUserEvent = null;
  @Input('type') type = '';

  myEvents: Array<any> = [];
  panelOpenState = false;
  url;
  id;

  userSession = JSON.parse(sessionStorage.getItem('user'));

  constructor(
    private userService: UserService,
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService,
    private organizationService: OrganizationService,
    private router: Router
  ) {
    this.url = router.url.split('/')[1];
    this.id = router.url.split('/')[2];
  }

  ngOnInit(): void {
    this.getEvents(this.url, this.id);
  }

  getAllUserEvents(id) {
    let myEvents;
    this.userService.getMyEvents(id).subscribe( response => {
      myEvents = response.ownerEvents;
    }, error => {
      this.utilitiesService.openSnackBar(`Error ao carregar projetos ${error.message}`);
    }, () => {
      myEvents = this.reduceDescription(myEvents);
      myEvents = this.reduceName(myEvents);
      this.myEvents = myEvents;
    });
  }

  getEvents(url, id){
    switch (url) {
      case 'entidade':
        this.organizationService.getOrganizationEvents(id).subscribe(response => {
          console.log(response.data);
          for (const event of response.data) {
            this.myEvents.push({
              id: event.id,
              name: event.name,
              image_url: event.image_url,
              date: event.date,
              start_time: event.start_time,
              address_street: event.address_street,
              local: event.local,
              address_city: event.address_city,
              address_state: event.state,
              description: event.description
            });
          }
        });
        break;

      case 'projeto':
        this.projectService.getProjectEvents(id).subscribe(response => {
          console.log(this.myEvents);
          for (const event of response.data) {
            this.myEvents.push({
              id: event.id,
              name: event.name,
              image_url: event.image_url,
              description: event.description
            });
          }
        });
        break;
    }
  }

  reduceName(event){
    for (const item of event) {
      if (item.name !== null ){
        if (item.name.length > 18){
          item.name = item.name.substring(0, 22) + '...';
        }

      }else{
        item.name = '...';
      }
    }
    return event;
  }

  reduceDescription(event){
    for (const item of event) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return event;
  }

  ngOnChanges() {
    this.doWhenId(this.idUserEvent, this.type);
  }

  private doWhenId(input: string, type: string){
    // console.log('#1 -', input, ' #2 -', type);
    if (input !== null && type === ''){
      this.getAllUserEvents(input);
    }
    if (input !== null && type === 'project'){
      this.getEvents('', input);
    }
  }
}
