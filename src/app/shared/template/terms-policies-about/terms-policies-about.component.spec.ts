import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsPoliciesAboutComponent } from './terms-policies-about.component';

describe('TermsPoliciesAboutComponent', () => {
  let component: TermsPoliciesAboutComponent;
  let fixture: ComponentFixture<TermsPoliciesAboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsPoliciesAboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsPoliciesAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
