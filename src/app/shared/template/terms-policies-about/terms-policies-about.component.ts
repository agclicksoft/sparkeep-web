import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-terms-policies-about',
  templateUrl: './terms-policies-about.component.html',
  styleUrls: ['./terms-policies-about.component.css']
})
export class TermsPoliciesAboutComponent implements OnInit {

    title: string;
    text: string;
    isAbout: boolean = false;
    isTerms: boolean = false;
    isPolicies: boolean = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: DialogData
        ) {}

  ngOnInit(): void {
      this.setData();
  }

  setData(): void {
    if(this.data.isTerms) {
        this.isTerms = true;
    }
    else if(this.data.isPolicies) {
        this.isPolicies = true;
    }
    else if(this.data.isAbout) {
        this.isAbout = true;
    }
  }

}

interface DialogData {
    isAbout?: boolean;
    isPolicies?: boolean;
    isTerms?: boolean;
}
