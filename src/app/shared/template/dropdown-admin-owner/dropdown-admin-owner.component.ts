import { ProjectService } from 'src/app/core/services/project.service';
import { EventService } from './../../../core/services/event.service';
import { User } from './../../models/user.model';
import { Router } from '@angular/router';
import { OrganizationService } from './../../../core/services/organization.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { UserService } from './../../../core/services/user.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

type typeProfile = 'User' | 'Organization' | 'Event' | 'Project';

@Component({
  selector: 'app-dropdown-admin-owner',
  templateUrl: './dropdown-admin-owner.component.html',
  styleUrls: ['./dropdown-admin-owner.component.css']
})
export class DropdownAdminOwnerComponent implements OnChanges {

  @Input() idOwner = null;
  @Input() administrators: Array<User> = [];
  @Input() typeProfile: typeProfile = 'User';

  owner: User;

  panelOpenState = false;

  userSession = JSON.parse(sessionStorage.getItem('user'));

  constructor(
    private userService: UserService,
    private organizationService: OrganizationService,
    private eventService: EventService,
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService,
    private router: Router
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.handleTypeProfile(this.typeProfile);
  }

  handleTypeProfile(typeP: typeProfile) {
    // console.log(typeP);
    typeP === 'User' ? this.getUser(this.idOwner) : '';
    typeP === 'Project' ? this.getProject(this.idOwner) : '';
    typeP === 'Event' ? this.getEvent(this.idOwner) : '';
    typeP === 'Organization' ? this.getOrganization(this.idOwner) : '';
  }

  handleReturnRoute(typeP: typeProfile): string{
    if (typeP === 'User') { return 'perfil'; }
    if (typeP === 'Project') { return 'projeto'; }
    if (typeP === 'Organization') { return 'entidade'; }
    if (typeP === 'Event') { return 'evento'; }
  }

  getUser(id) {
    this.userService.getUser(id).subscribe(
      response => {
        this.owner = response;
      },
      error => {
        this.utilitiesService.openSnackBar(`Sinto muito não foi possível buscar os dados.`);
      }
    );
  }

  getProject(id) {
    this.projectService.getProject(id).subscribe(
      response => {
        this.owner = response;
      },
      error => {
        this.utilitiesService.openSnackBar(`Sinto muito não foi possível buscar os dados.`);
      }
    );
  }

  getOrganization(id) {
    this.organizationService.getById(id).subscribe(
      response => {
        this.owner = response;
      },
      error => {
        this.utilitiesService.openSnackBar(`Sinto muito não foi possível buscar os dados.`);
      }
    );
  }

  getEvent(id) {
    this.eventService.getById(id).subscribe(
      response => {
        this.owner = response;
      },
      error => {
        this.utilitiesService.openSnackBar(`Sinto muito não foi possível buscar os dados.`);
      }
    );
  }

}
