import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownAdminOwnerComponent } from './dropdown-admin-owner.component';

describe('DropdownAdminOwnerComponent', () => {
  let component: DropdownAdminOwnerComponent;
  let fixture: ComponentFixture<DropdownAdminOwnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownAdminOwnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownAdminOwnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
