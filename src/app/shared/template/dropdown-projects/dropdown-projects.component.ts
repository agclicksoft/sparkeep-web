import { Router } from '@angular/router';
import { OrganizationService } from 'src/app/core/services/organization.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { UserService } from 'src/app/core/services/user.service';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-dropdown-projects',
  templateUrl: './dropdown-projects.component.html',
  styleUrls: ['./dropdown-projects.component.css']
})
export class DropdownProjectsComponent implements OnInit, OnChanges {

  @Input('idUserProject') idUserProject = null;

  myProjects: Array<any> = [];
  panelOpenState = false;
  url;
  id;

  userSession = JSON.parse(sessionStorage.getItem('user'));

  constructor(
    private userService: UserService,
    private utilitiesService: UtilitiesService,
    private organizationService: OrganizationService,
    private router: Router
  ) {
    this.url = router.url.split('/')[1];
    this.id = router.url.split('/')[2];
  }

  ngOnInit(): void {
    this.getProjects(this.url, this.id);
  }

  getAllUserProjects(id){
    let myProjects;
    this.userService.getMyProjects(id).subscribe( response => {
      myProjects = response.ownerProjects;
      // console.log('thismyproject', this.myProjects);
    }, error => {
      this.utilitiesService.openSnackBar(`Error ao carregar projetos ${error.message}`);
    }, () => {
      myProjects = this.reduceDescription(myProjects);
      myProjects = this.reduceName(myProjects);
      this.myProjects = myProjects;
    });
  }

  getProjects(url, id) {
    switch (url) {
      case 'entidade':
        this.organizationService.getOrganizationProjects(id).subscribe(response => {
          for (const project of response.data) {
            this.myProjects.push({
              id: project.id,
              name: project.name,
              description: project.description,
              image_url: project.image_url
            });
          }
        });
        break;
    }
  }

  reduceName(projects){
    for (const item of projects) {
      if (item.name !== null ){
        if (item.name.length > 18){
          item.name = item.name.substring(0, 22) + '...';
        }

      }else{
        item.name = '...';
      }
    }
    return projects;
  }

  reduceDescription(projects){
    for (const item of projects) {
      if (item.description !== null ){
        item.description = item.description.substring(0, 22) + '...';
      }else{
        item.description = '...';
      }
    }
    return projects;
  }

  ngOnChanges() {
    this.doWhenIdUser(this.idUserProject);
  }

  private doWhenIdUser(input: string){
    if (input !== null){
      // console.log('valores do input: ', input);
      this.getAllUserProjects(input);
    }
  }

}
