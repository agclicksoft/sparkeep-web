import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownPartnershipsComponent } from './dropdown-partnerships.component';

describe('DropdownPartnershipsComponent', () => {
  let component: DropdownPartnershipsComponent;
  let fixture: ComponentFixture<DropdownPartnershipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownPartnershipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownPartnershipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
