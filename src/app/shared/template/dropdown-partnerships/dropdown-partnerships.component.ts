import { MaintenanceTypeService } from './../../../core/services/maintenance-type.service';
import { MyPartnershipDialogComponent } from './../my-partnership-dialog/my-partnership-dialog.component';
import { MyPartnersDialogComponent } from 'src/app/shared/template/my-partners-dialog/my-partners-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { OrganizationService } from './../../../core/services/organization.service';
import { ProjectService } from 'src/app/core/services/project.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown-partnerships',
  templateUrl: './dropdown-partnerships.component.html',
  styleUrls: ['./dropdown-partnerships.component.css']
})
export class DropdownPartnershipsComponent implements OnInit {

  panelOpenState = false;
  userId;
  myPartnerships: Array<any> = [];
  url;
  id;

  constructor(
    private userService: UserService,
    private router: Router,
    private projectService: ProjectService,
    private organizationService: OrganizationService,
    public dialog: MatDialog,
    private shortagesService: MaintenanceTypeService,
  ) {
    this.url = router.url.split('/')[1];
    this.id = router.url.split('/')[2];
  }

  ngOnInit(): void {
    this.getAccountLogged();
  }

  getAccountLogged() {
    this.userService.getAccountLogged().subscribe(response => {
      this.userId = response.id;
    }, errror => {

    }, () => {
      this.getMyPartnerships(this.url, this.id, this.userId);
    });
  }

  getAllUserPartners(id) {
    this.userService.getMyPartnerships(id, '', 1, 1, 1, 1).subscribe(response => {
      // console.log('USER PARTNER: ', response);
      if (response.projectPartners.data.length > 0) {
        this.myPartnerships.push({
          id: response.projectPartners.data.slice(-1)[0].project.id,
          name: response.projectPartners.data.slice(-1)[0].project.name,
          description: response.projectPartners.data.slice(-1)[0].project.description,
          image_url: response.projectPartners.data.slice(-1)[0].project.image_url,
          type: 'projeto',
          shortage_id: response.projectPartners?.data.slice(-1)[0].shortage_id
        });
      }
      if (response.eventPartners.data.length > 0) {
        this.myPartnerships.push({
          id: response.eventPartners.data.slice(-1)[0].eventData.id,
          name: response.eventPartners.data.slice(-1)[0].eventData.name,
          description: response.eventPartners.data.slice(-1)[0].eventData.description,
          image_url: response.eventPartners.data.slice(-1)[0].eventData.image_url,
          type: 'evento',
          shortage_id: response.eventPartners.data.slice(-1)[0].shortage_id
        });
      }
      if (response.organizationPartners.data.length > 0) {
        this.myPartnerships.push({
          id: response.organizationPartners.data.slice(-1)[0].organization.id,
          name: response.organizationPartners.data.slice(-1)[0].organization.name,
          description: response.organizationPartners.data.slice(-1)[0].organization.description,
          image_url: response.organizationPartners.data.slice(-1)[0].organization.image_url,
          type: 'entidade',
          shortage_id: response.organizationPartners.data.slice(-1)[0].shortage_id
        });
      }
    });
  }

  getMyPartnerships(url, id, userId) {
    switch (url) {
      case 'projeto':
        this.projectService.getProjectPartners(id).subscribe(response => {
          console.log('RESPONSE PROJECT: ', response);
          for (const partnership of response.data) {
            this.myPartnerships.push(partnership);
          }
        });
        break;
      case 'entidade':
        this.organizationService.getOrganizationPartners(id).subscribe(response => {
          console.log('RESPONSE ORGANIZATION: ', response);
          for (const partnership of response.data) {
            this.myPartnerships.push(partnership);
          }
        });
        break;
    }
    if (url === 'perfil' && !id) {
      this.getAllUserPartners(userId);
    }
    else if (url === 'perfil' && id) {
      this.getAllUserPartners(id);
    }
  }

  handlePartnership(partner) {
    let shortage;
    let receiver;

    if (partner.user_send_id) {
      receiver = 'Usuário';
    }
    if (partner.organization_send_id) {
      receiver = 'Entidade';
    }
    if (partner.project_send_id) {
      receiver = 'Projeto';
    }
    if (partner.event_send_id) {
      receiver = 'Evento';
    }

    this.shortagesService.getAll().subscribe(
      response => {
        for (const item of response) {
          item.id === partner.shortage_id ? shortage = item.description : '';
        }
      },
      error => {
        console.log('error: ', error.error);
      },
      () => {
        // console.log('Valor da partner', partner);
        if (this.url === 'perfil') {
          this.dialog.open(MyPartnershipDialogComponent, {
            data: {
              partner_id: partner.id,
              shortages_id: partner.shortage_id,
              name: partner.name,
              id: partner.id,
              title: partner.title,
              subtitle: partner.type,
              description: partner.description,
              typePartner: shortage,
              partnership_agreement: partner.partnership_agreement,
              image_url: partner.image_url,
            }
          });
        }else{
          this.dialog.open(MyPartnershipDialogComponent, {
            data: {
              partner_id: partner.id,
              shortages_id: partner.shortage_id,
              name: partner.partner.name,
              id: partner.partner.id,
              title: partner.partner.title,
              subtitle: receiver,
              description: partner.partner.description,
              typePartner: shortage,
              partnership_agreement: partner.partner.partnership_agreement,
              image_url: partner.partner.image_url,
            }
          });
        }

      }
    );
  }

}
