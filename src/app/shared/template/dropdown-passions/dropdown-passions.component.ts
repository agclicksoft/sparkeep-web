import { UtilitiesService } from './../../../core/services/utilities.service';
import { PassionsService } from './../../../core/services/passions.service';
import { Passion } from './../../models/passion.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown-passions',
  templateUrl: './dropdown-passions.component.html',
  styleUrls: ['./dropdown-passions.component.css']
})
export class DropdownPassionsComponent implements OnInit {

  panelOpenState = false;
  @Input('myPassions') myPassions: Array<any> = [];
  allPassions;

  constructor(
    private passionsService: PassionsService,
    private utilitiesService: UtilitiesService,
  ) { }

  ngOnInit(): void {
    // console.log('My passions: ', this.myPassions);
    // this.getAllPassions();
  }

  getAllPassions(){
    this.passionsService.getAll().subscribe( response => {
      this.allPassions = response;
      console.log(this.myPassions)
      // console.log('todas as passions: ', this.allPassions);
    }, error => {

    }, () => {
      // this.passionsArray();
    });
  }

  // passionsArray(){
  //   // this.myPassions = [1, 2, 3];
  //   console.log('My passions: ', this.myPassions);
  //   for (const item of this.allPassions) {
  //     for (const myItem of this.myPassions){
  //       if (myItem === item.id){
  //         this.arrayPassions.push(item);
  //       }
  //     }
  //   }
  // }

}
