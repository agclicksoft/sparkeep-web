import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownPassionsComponent } from './dropdown-passions.component';

describe('DropdownPassionsComponent', () => {
  let component: DropdownPassionsComponent;
  let fixture: ComponentFixture<DropdownPassionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownPassionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownPassionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
