import { UtilitiesService } from './../../../core/services/utilities.service';
import { Post } from 'src/app/shared/models/post.model';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/core/services/user.service';
import { PassionsService } from './../../../core/services/passions.service';
import { AuthService } from './../../../core/authentication/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostService } from 'src/app/core/services/post.service';
import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  userSession = JSON.parse(sessionStorage.getItem('user'));
  formulario: FormGroup;

  // filtroPrincipal = 'global';
  // post = new Post();

  events: Array<any> = [];
  projects: Array<any> = [];
  organizations: Array<any> = [];

  isSubmited = false;

  // TUDO RELACIONADO A POST E A ENVIO DE IMAGENS
  post;
  imgUrlForm;
  imgElement;
  imgPost; // POST IMAGEM
  imagesToAddPost = [];
  passions;
  imgSelectedSender = 'assets/img/user.svg';

  constructor(
    private postService: PostService,
    private formBuilder: FormBuilder,
    private _ngZone: NgZone,
    private authService: AuthService,
    private passionsService: PassionsService,
    private userService: UserService,
    private ngxSpinner: NgxSpinnerService,
    private dialog: MatDialog,
    private utilitiesService: UtilitiesService,
  ) { }

  // Autosize Material Design
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  triggerResize() {
    // autoSize TextArea.
    this._ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  resetPost() {
    delete this.events;
    delete this.projects;
    delete this.organizations;

    // TUDO RELACIONADO A POST E A ENVIO DE IMAGENS
    delete this.post;
    delete this.imgUrlForm;
    delete this.imgElement;
    this.imgPost = null; // POST IMAGEM
    delete this.passions;
    this.imgSelectedSender = 'assets/img/user.svg';
  }

  ngOnInit(): void {
    this.resetPost();
    this.post = new Post();
    this.imgUrlForm = Array();
    this.imgElement = Array();
    this.imgPost = new FormData();

    this.formulario = this.formBuilder.group({
      user_id: [this.userSession.id],
      author: [''],
      select_profile: ['', Validators.required],
      privacy: ['public', Validators.required],
      description: ['', Validators.required],
      author_user_id: null,
      author_event_id: null,
      author_project_id: null,
      author_organization_id: null,
      passions: ['', Validators.required],
    });

    this.getPassions();
    this.getMyOrganizations();
    this.getMyProjects();
    this.getMyEvents();
  }

  getPassions() {
    this.passionsService.getAll().subscribe(Response => {
      this.passions = Response;
    }, error => {
      this.userService.openSnackBar('');
    }
    );
  }

  getMyEvents() {
    let events;
    this.userService.getMyEvents(this.userSession.id).subscribe(
      response => {
        events = [...response.ownerEvents, ...response.administratorsEvents];
        // console.log('events', this.events );
      },
      error => {
        this.userService.openSnackBar('Erro ao buscar meus eventos');
      },
      () => {
        this.events = this.reduceName(events);  // Reduz o nome do evento caso > 20
      }
    );
  }

  getMyProjects() {
    let projects;
    this.userService.getMyProjects(this.userSession.id).subscribe(
      response => {
        projects = [...response.ownerProjects, ...response.administratorsProjects];
        // console.log('projects', this.projects );
      },
      error => {
        this.userService.openSnackBar('Erro ao buscar meus projetos');
      },
      () => {
        this.projects = this.reduceName(projects);
      }
    );
  }

  getMyOrganizations() {
    let organizations;
    this.userService.getMyOrganizations(this.userSession.id).subscribe(
      response => {
        organizations = [...response.ownerOrganizations, ...response.administratorsOrganizations];
        // console.log('organizations', this.organizations );
      },
      error => {
        this.userService.openSnackBar('Erro ao buscar meus organizations');
      },
      () => {
        this.organizations = this.reduceName(organizations);
      }
    );
  }

  envioImg(event) {
    console.log('files event: ', event.target.files);
    for (const item of event.target.files) {
      if (item.type.split('/')[0] !== 'image') {
        this.utilitiesService.openSnackBar('Por favor insira um arquivo do tipo imagem.');
        return false;
      }
      if (item.size >= 2500000){
        this.utilitiesService.openSnackBar(`A imagem ${item.name} é muito grande.`);
        return false;
      }
    }
    if (event.target.files.length > 10 ||
      this.imgElement.length >= 10 && this.imgUrlForm.length >= 10 ||
      this.imgElement.length + event.target.files.length > 10 ||
      this.imgUrlForm.length + event.target.files.length > 10
    ) {
      this.utilitiesService.openSnackBar(`Você tem um limite de 10 imagens por post!!!`);
      return 0;
    }
    if (this.imgElement.length < 10 && this.imgUrlForm.length < 10) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < event.target.files.length; i++) {
        const reader = new FileReader(); // Gera uma instancia nova do FileReader

        reader.readAsDataURL(event.target.files[i]);
        reader.onload = () => {
          this.imgUrlForm.push(reader.result as string);
          this.imagesToAddPost.push({
            path: reader.result as string
          });
        };

        this.imgElement.push(event.target.files[i]);
      }
      this.formulario.controls.description.clearValidators();
      this.formulario.controls.description.updateValueAndValidity();
    } else {
      this.postService.openSnackBar('Excedeu o limite de imagens');
    }
  }

  removeImgDOM(index) {
    this.imgUrlForm.splice(index, 1); // no index remova 1
    this.imgElement.splice(index, 1);
  }

  selectChange(value) {
    const roleSend = value.split('_', 2);
    this.profileSend(roleSend[0], roleSend[1]);
    this.changeImg(value.split('_', 2));
  }

  changeImg(value) {
    // console.log('Valor[0] ', value[0], ' --- Valor[1] ', value[1]);
    switch (value[0]) {
      case 'user':
        this.imgSelectedSender = this.userSession.image_url;
        break;
      case 'project':
        this.setProjectImg(value[1]);
        break;
      case 'event':
        this.setEventImg(value[1]);
        break;
      case 'organization':
        this.setOrganizationImg(value[1]);
        break;
    }
  }

  setProjectImg(id) {
    for (const project of this.projects) {
      if (project.id === parseInt(id)) {
        this.imgSelectedSender = project.image_url;
      }
    }
  }
  setEventImg(id) {
    for (const event of this.events) {
      if (event.id === parseInt(id)) {
        this.imgSelectedSender = event.image_url;
      }
    }
  }
  setOrganizationImg(id) {
    for (const organization of this.organizations) {
      if (organization.id === parseInt(id)) {
        this.imgSelectedSender = organization.image_url;
      }
    }
  }

  profileSend(value, id) {
    this.formulario.patchValue({
      author: value,
      author_user_id: null,
      author_event_id: null,
      author_project_id: null,
      author_organization_id: null,
    });

    switch (value) {
      case 'user': {
        this.formulario.controls.author_user_id.patchValue(id);
        break;
      }
      case 'event': {
        this.formulario.controls.author_event_id.patchValue(id);
        break;
      }
      case 'project': {
        this.formulario.controls.author_project_id.patchValue(id);
        break;
      }
      case 'organization': {
        this.formulario.controls.author_organization_id.patchValue(id);
        break;
      }
    }
  }

  onSubmit() {
    this.ngxSpinner.show();
    this.isSubmited = true;
    if (this.formulario.valid) {
      // Adicionando as imagens no formulário **--**
      for (let i = 0; i < this.imgElement.length; i++) {
        this.imgPost.append('attachments', this.imgElement[i]);
      }

      // console.log('Valor do formulário: ', this.formulario.value);
      if (this.imgElement <= 0 ){
        this.utilitiesService.openSnackBar('Por favor adicione uma descrição, para continuar com a postagem...');
      }
      // SERVICE POST
      let response;
      this.postService.create(this.formulario.value).subscribe(
        res => {
          let addPost = res.post;
          console.log(res);
          addPost.owner = true;
          addPost.images = this.imagesToAddPost;
          this.postService.addPost.next(addPost);
          this.postService.openSnackBar('Postagem feita com sucesso!');
          $('#modalPost').modal('hide');
          response = res;
        },
        error => {
          this.ngxSpinner.hide();
          // console.log('Error: ', error.error);
          $('#modalPost').modal('hide');
          this.postService.openSnackBar('Não foi possível criar a postagem.');
          setTimeout(() => {
            this.postService.openSnackBar(error.error.message);
          }, 800);
        },
        () => {
          this.updateImgs(response);
        }
      );
    } else {
      this.ngxSpinner.hide();
      this.utilitiesService.openSnackBar('Postagem inválida.');
    }
  }

  updateImgs(response) {
    if (response?.post?.id && this.imgPost) {
      this.postService.updateImg(response?.post?.id, this.imgPost).subscribe(
        res => {
          this.postService.openSnackBar('Imagens Enviadas!');
          this.ngxSpinner.hide();
        },
        error => {
          this.ngxSpinner.hide();
          console.log('Error: ', error.error);
          this.postService.openSnackBar('Não foi possível enviar as imagens.');
        },
        () => {
          this.ngOnInit();
        }
      );
    } else {
      this.ngxSpinner.hide();
      this.ngOnInit();
    }
  }

  reduceName(object) {
    for (const item of object) {
      if (item.name !== null && item.name.length > 20) {
        item.name = item.name.substring(0, 22) + '...';
      } else {
        item.description = '...';
      }
    }
    return object;
  }

}
