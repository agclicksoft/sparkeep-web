import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintPostComponent } from './complaint-post.component';

describe('ComplaintPostComponent', () => {
  let component: ComplaintPostComponent;
  let fixture: ComponentFixture<ComplaintPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplaintPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
