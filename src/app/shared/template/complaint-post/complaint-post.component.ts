import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from 'src/app/core/services/post.service';
import { UserService } from 'src/app/core/services/user.service';
import { UtilitiesService } from 'src/app/core/services/utilities.service';
import { Post } from '../../models/post.model';

@Component({
  selector: 'app-complaint-post',
  templateUrl: './complaint-post.component.html',
  styleUrls: ['./complaint-post.component.css']
})
export class ComplaintPostComponent implements OnInit {

  reported;

  complaint = this.fb.group({
    comment: ['', Validators.required]
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Data,
    private fb: FormBuilder,
    private postService: PostService,
    private utilitiesService: UtilitiesService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  reportPost(id, event) {
    event.stopPropagation();
    this.postService.reportPost(id, this.complaint.value).subscribe(
      response => {
        this.reported = true;
        this.dialog.closeAll();
        this.utilitiesService.openSnackBar('Post denunciado com sucesso!');
      },
      error => {
        console.log(error);
      }
    );
  }

}

interface Data {
  post: Post;
  event: any;
}
