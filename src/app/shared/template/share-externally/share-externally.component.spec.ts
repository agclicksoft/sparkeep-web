import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareExternallyComponent } from './share-externally.component';

describe('ShareExternallyComponent', () => {
  let component: ShareExternallyComponent;
  let fixture: ComponentFixture<ShareExternallyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareExternallyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareExternallyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
