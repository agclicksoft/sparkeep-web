import { Post } from 'src/app/shared/models/post.model';
import { PostService } from 'src/app/core/services/post.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-share-externally',
  templateUrl: './share-externally.component.html',
  styleUrls: ['./share-externally.component.css']
})
export class ShareExternallyComponent implements OnInit, OnDestroy {

  idPost;
  post = new Post();

  constructor(
    private utilService: UtilitiesService,
    private postService: PostService
  ) {
    this.idPost = this.utilService.idPostShare;
  }

  ngOnDestroy(): void {
    this.utilService.idPostShare = null;
  }

  ngOnInit(): void {
    // console.log('ID POSTAGEM: ', this.idPost);
    this.getPost(this.idPost);
  }

  getPost(id){
    this.postService.getById(id).subscribe(
      response => {
        this.post = response;
        console.log('A POSTAGEM: ', this.post);
      },
      error => {
        this.utilService.openSnackBar(`Não foi possível obter os dados da postagem.`);
      }
    );
  }

}
