import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from './../../../core/authentication/auth.service';
import { SocialUser, SocialAuthService, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-sign-in',
  templateUrl: './modal-sign-in.component.html',
  styleUrls: ['./modal-sign-in.component.css']
})
export class ModalSignInComponent implements OnInit {

  formulario: FormGroup;

  userSocial: SocialUser;
  loggedIn: boolean;

  statusSocialLogin: any = false;

  // Serve para buscar dados da sessao do navegador e retorna em JSON para exibição funcionando Ex. userSession.id , userSession.name etc...
  // jsonSession = sessionStorage.getItem('user');
  // userSession = JSON.parse(this.jsonSession);

  constructor(
      private formBuilder: FormBuilder,
      private authServiceSocial: SocialAuthService,
      private authService: AuthService,
      private ngxSpinner: NgxSpinnerService,
      private router: Router,
      private dialog: MatDialog,
    ) { }

  ngOnInit(): void {

    this.formulario = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]],
    });

    this.authServiceSocial.authState.subscribe(userSocial => {
      this.userSocial = userSocial;
      this.loggedIn = (userSocial != null);
      if (this.userSocial){
        this.statusSocialLogin = this.authService.authSocial(this.userSocial, 'modal');
        this.authService.USER_POST_LIKE = true;
        this.dialog.closeAll();
        this.authService.openSnackBar('Sendo redirecionado.');
      }
    },
     error => {
       console.log('Error: ', error);
     }
    );
  }

  onSubmit() {
    if (this.formulario.status === 'VALID') {
      const status = this.authService.logar(this.formulario.value, 'modal');
      this.authService.USER_POST_LIKE = true;
      status && this.dialog.closeAll();
    } else {
      console.log('formulario inválido.');
    }
  }

  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }
}
