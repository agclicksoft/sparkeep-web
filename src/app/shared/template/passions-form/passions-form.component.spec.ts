import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassionsFormComponent } from './passions-form.component';

describe('PassionsFormComponent', () => {
  let component: PassionsFormComponent;
  let fixture: ComponentFixture<PassionsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassionsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassionsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
