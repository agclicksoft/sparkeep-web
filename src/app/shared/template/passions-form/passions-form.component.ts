import { Component, OnInit, EventEmitter, Output, Input, OnChanges } from '@angular/core';
import { PassionsService } from '../../../core/services/passions.service';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-passions-form',
  templateUrl: './passions-form.component.html',
  styleUrls: ['./passions-form.component.css']
})
export class PassionsFormComponent implements OnInit, OnChanges {

  isLoading = true;
  passions: Array<any> = [];
  passionsForm = this.fb.group({});

  @Input() projectForm: any;
  @Output() passionsEmit = new EventEmitter<any>();

  constructor(
    private passionsService: PassionsService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getAllPassions();
  }

  ngOnChanges() {
    this.setPassionsForm(this.projectForm.passions);
  }

  getAllPassions() {
    this.passionsService.getAll().subscribe(
      response => {
        for (const item of response) { this.passionsForm.addControl(item.name, new FormControl(false)); }
        this.passions = response;
        this.isLoading = false;
        this.setPassionsForm(this.projectForm.passions);
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  checkControl() {
    const passions = [];
    for (const i in this.passions) {
      if (this.passionsForm.controls[this.passions[i].name].value) {
        passions.push(this.passions[i]);
      }
    }
    this.passionsEmit.emit(passions);
  }

  setPassionsForm(passions) {
    for (const property in this.passionsForm.controls) {
      if (passions.map(el => el.name).indexOf(property) > -1) {
        this.passionsForm.controls[property].patchValue(true);
      }
    }
  }
}
