import { UtilitiesService } from './../../../core/services/utilities.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-add-administrators',
  templateUrl: './add-administrators.component.html',
  styleUrls: ['./add-administrators.component.css']
})
export class AddAdministratorsComponent implements OnInit {

  keyword = '';
  page = 1;

  users: User = new User;
  @Input() administrators: Array<any> = [];

  userSession = JSON.parse(sessionStorage.getItem('user')) || JSON.parse(localStorage.getItem('user'));

  @Output() administratorsEmit = new EventEmitter();

  @Output() closeModalEmit = new EventEmitter();

  constructor(
    private userService: UserService,
    private utils: UtilitiesService,
  ) { }

  ngOnInit(): void {
    this.getAllUsers('', 1);
  }

  getAllUsers(keyword, page) {
    this.keyword = keyword;
    this.page = page;
    this.userService.getAllUsers(keyword, page).subscribe(
      response => {
        this.users.total = response.total;
        this.users.perPage = response.perPage;
        this.users.page = response.page;
        this.users.data = response.data.filter(el => el.id !== this.userSession.id && el.receive_administrator_proposals);
      },
      error => {
        this.utils.openSnackBar(`Oops, obtivemos um erro ao buscar os usuários.`);
      }
    );
  }

  closeModal() {
    this.closeModalEmit.emit(true);
  }

  userControl(user, index) {

    const alreadyExists = this.administrators.map( (el) => el.id).indexOf(user.id);

    if (alreadyExists != -1) {
      for (const [i, item] of this.administrators.entries()) {
        if (item.id === user.id) {
          this.administrators.splice(i, 1);
          // document.querySelectorAll('.user')[index].setAttribute('style', 'background: #FFF;')
        }
      }
    }
    else {
      this.administrators.push(user);
      // document.querySelectorAll('.user')[index].setAttribute('style', 'background: #F1F1F1;')
    }

    this.administratorsEmit.emit(this.administrators);

    this.closeModal();
  }

  previous() {
    if (this.page <= 1) {
      this.page = 1;
    } else {
      this.page--;
    }
  }
  next() {
    this.page++;
  }

  changePage(page: number) {
    if (page <= 1) {
      this.page = 1;
    } else {
      this.page = page;
    }
    this.getAllUsers(this.keyword, this.page);
  }

}

