import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortagesFormComponent } from './shortages-form.component';

describe('ShortagesFormComponent', () => {
  let component: ShortagesFormComponent;
  let fixture: ComponentFixture<ShortagesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortagesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortagesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
