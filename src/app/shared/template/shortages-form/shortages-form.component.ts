import { Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MaintenanceTypeService } from '../../../core/services/maintenance-type.service';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-shortages-form',
  templateUrl: './shortages-form.component.html',
  styleUrls: ['./shortages-form.component.css']
})
export class ShortagesFormComponent implements OnInit, OnChanges {

  isLoading = true;

  shortages: Array<any> = [];
  maintenanceTimes: Array<any> = [];

  maintenanceTimeForm = this.fb.group({});
  shortagesForm = this.fb.group({});

  kindOfHelp = '';

  @Input() projectForm: any;
  @Output() shortagesEmit = new EventEmitter<any>();
  @Output() maintenanceTimeEmit = new EventEmitter<any>();
  @Output() kindOfHelpEmit = new EventEmitter();

  constructor(
    private maintenanceTypeService: MaintenanceTypeService,
    private fb: FormBuilder
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    this.kindOfHelp = this.projectForm.kind_of_help;
    this.emitKindOfHelp(this.kindOfHelp);

    this.setShortagesForm(this.projectForm.shortages);
    this.setMaintenanceTime(this.projectForm.maintenance_times);
  }

  ngOnInit(): void {
    this.getAllShortages();
    this.getAllTimes();
    // for (let item of this.maintenanceTimes)
    //   this.maintenanceTimeForm.addControl(item, new FormControl(false));
  }

  getAllShortages() {
    this.maintenanceTypeService.getAll().subscribe(
      response => {
        for (const item of response) { this.shortagesForm.addControl(item.description, new FormControl(false)); }
        this.shortages = response;
        this.isLoading = false;
        this.setShortagesForm(this.projectForm.shortages);
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  getAllTimes() {
    this.maintenanceTypeService.getAllTimes().subscribe(
      response => {
        // console.log('Resposta MaintenanceTIME: ', response);
        for (const item of response) { this.maintenanceTimeForm.addControl(item.description, new FormControl(false)); }
        this.maintenanceTimes = response;
        this.isLoading = false;
        this.setMaintenanceTime(this.projectForm.maintenanceTimeForm);
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  checkMaintenanceTypeControl() {
    const shortages = [];
    for (const i in this.shortages) {
      if (this.shortagesForm.controls[this.shortages[i].description].value) {
        shortages.push(this.shortages[i]);
      }
    }

    console.log(shortages);
    this.shortagesEmit.emit(shortages);
  }

  checkMaintenanceTimeControl() {
    const maintenances = [];
    for (const index in this.maintenanceTimes) {
      if (this.maintenanceTimeForm.controls[this.maintenanceTimes[index].description].value) {
        maintenances.push(this.maintenanceTimes[index]);
      }
    }

    console.log(maintenances);
    this.maintenanceTimeEmit.emit(maintenances);
  }


  emitKindOfHelp(kindOfHelp) {
    this.kindOfHelpEmit.emit(kindOfHelp);
  }

  setShortagesForm(shortages) {
    for (const property in this.shortagesForm.controls) {
      if (shortages.map(el => el.description).indexOf(property) > -1) {
        this.shortagesForm.controls[property].patchValue(true);
      }
    }
  }

  setMaintenanceTime(maintenanceTime) {
    for (const property in this.maintenanceTimeForm.controls) {
      if (maintenanceTime?.map(el => el.description).indexOf(property) > -1) {
        this.maintenanceTimeForm.controls[property].patchValue(true);
      }
    }
  }

  // setMaintenanceTime(maintenanceTime) {
  //   this.maintenanceTimeForm.controls.maintenanceTime.patchValue(maintenanceTime);
  // }

  // checkMaintenanceTimeControl(maintenanceTime) {
  //   this.maintenanceTimeEmit.emit(maintenanceTime);
  // }

}
