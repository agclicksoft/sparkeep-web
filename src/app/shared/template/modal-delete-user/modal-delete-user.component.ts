import { EventService } from './../../../core/services/event.service';
import { OrganizationService } from 'src/app/core/services/organization.service';
import { ProjectService } from 'src/app/core/services/project.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
@Component({
  selector: 'app-modal-delete-user',
  templateUrl: './modal-delete-user.component.html',
  styleUrls: ['./modal-delete-user.component.css']
})
export class ModalDeleteUserComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalDeleteUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialog: MatDialog,
    private userService: UserService,
    private projectService: ProjectService,
    private organizationService: OrganizationService,
    private eventService: EventService,
    private utilsService: UtilitiesService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.dialogRef.close();
  }

  modalDelete(type: deleteType, id: number | string) {
    type === 'User' ? this.deleteUser(id) : '';
    type === 'Event' ? this.deleteEvent(id) : '';
    type === 'Organization' ? this.deleteOrganization(id) : '';
    type === 'Project' ? this.deleteProject(id) : '';
  }

  deleteUser(id) {
    this.userService.delete(id).subscribe(
      response => {
        this.utilsService.openSnackBar(`Usuário deletado com sucesso.`);
      },
      error => {
        this.utilsService.openSnackBar(`Obtivemos um erro ao deletar perfil.`);
      },
      () => {
        sessionStorage.clear();
        this.router.navigate(['/login']);
      }
    );
  }
  deleteEvent(id) {
    this.eventService.delete(id).subscribe(
      response => {
        this.utilsService.openSnackBar(`Usuário deletado com sucesso.`);
      },
      error => {
        this.utilsService.openSnackBar(`Obtivemos um erro ao deletar perfil.`);
      }, () => {
        this.closeModal();
        this.router.navigate(['/global']);
      }
    );
  }
  deleteOrganization(id) {
    this.organizationService.delete(id).subscribe(
      response => {
        this.utilsService.openSnackBar(`Usuário deletado com sucesso.`);
      },
      error => {
        this.utilsService.openSnackBar(`Obtivemos um erro ao deletar perfil.`);
      }, () => {
        this.closeModal();
        this.router.navigate(['/global']);
      }
    );
  }
  deleteProject(id) {
    this.projectService.delete(id).subscribe(
      response => {
        this.utilsService.openSnackBar(`Usuário deletado com sucesso.`);
      },
      error => {
        this.utilsService.openSnackBar(`Obtivemos um erro ao deletar perfil.`);
      }, () => {
        this.closeModal();
        this.router.navigate(['/global']);
      }
    );
  }

}

type deleteType = 'User' | 'Organization' | 'Project' | 'Event';
interface DialogData {
  id: number | string;
  type: deleteType;
}
