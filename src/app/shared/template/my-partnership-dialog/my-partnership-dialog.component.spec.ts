import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPartnershipDialogComponent } from './my-partnership-dialog.component';

describe('MyPartnershipDialogComponent', () => {
  let component: MyPartnershipDialogComponent;
  let fixture: ComponentFixture<MyPartnershipDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPartnershipDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPartnershipDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
