import { MaintenanceTypeService } from './../../../core/services/maintenance-type.service';
import { PartnershipService } from './../../../core/services/partnership.service';
import { Component, Inject, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-my-partnership-dialog',
  templateUrl: './my-partnership-dialog.component.html',
  styleUrls: ['./my-partnership-dialog.component.css']
})
export class MyPartnershipDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private partnershipService: PartnershipService,
    private shortageService: MaintenanceTypeService,
  ) {
  }

  ngOnInit(): void {
  }

  lowerSubtitle(data: string){
    return data.toLocaleLowerCase();
  }

}
interface DialogData {
  partner_id;
  shortage_id;
  name;
  id;
  title;
  subtitle;
  description;
  typePartner;
  partnership_agreement;
  image_url;
}
