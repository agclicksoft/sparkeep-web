import { ComplaintPostComponent } from 'src/app/shared/template/complaint-post/complaint-post.component';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ShareExternallyComponent } from './../share-externally/share-externally.component';
import { PostService } from './../../../core/services/post.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { Post } from './../../models/post.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input('posts') posts: Post = new Post;

  img_clap = '../../../../../assets/svg/clap.svg';
  img_clap_roxo = '../../../../../assets/svg/clap-roxo.svg';

  reported = false;

  user;

  constructor(
    private utilitiesService: UtilitiesService,
    private postService: PostService,
    private userService: UserService,
    private dialog: MatDialog,
    private router: Router,
  ) { }

  ngOnInit(): void {
    // console.log('POST: ', this.posts);
    this.getUserLogged();
  }

  likePost(id) {
    this.postService.likePost(id).subscribe(Response => {
      const message = Response?.message;
      const index = this.posts.data.map(el => el.id).indexOf(id);
      this.posts.data[index].liked = !this.posts.data[index].liked;
      if (this.posts.data[index].liked){
        this.posts.data[index].__meta__.total_inspireds = parseInt(this.posts.data[index].__meta__.total_inspireds) + 1;
      }else{
        this.posts.data[index].__meta__.total_inspireds = parseInt(this.posts.data[index].__meta__.total_inspireds) - 1;
      }
    }, error => {
      this.utilitiesService.openSnackBar(`${error.error}`);
    });
  }

  // reportPost(id, event) {
  //   event.stopPropagation();

  //   this.postService.reportPost(id).subscribe(
  //     response => {
  //       this.reported = true;
  //       setTimeout(() => {
  //         document.getElementById('dropdown').click();
  //       }, 1500);
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  openDialog(post, event) {
    this.dialog.open(ComplaintPostComponent, {
     data: {
       post,
       event
     },
     maxWidth: '580px',
     width: '100%'
    });
  }

  gotoProfile(post){
    post.author === 'user' ? this.router.navigate([`/perfil/${post?.authorUser?.id}`]) : '';
    post.author === 'event' ? this.router.navigate([`/evento/${post?.authorEvent?.id}`]) : '';
    post.author === 'project' ? this.router.navigate([`/projeto/${post?.authorProject?.id}`]) : '';
    post.author === 'organization' ? this.router.navigate([`/entidade/${post?.authorOrganization?.id}`]) : '';
  }

  sharePost(id) {
    this.utilitiesService.idPostShare = id;
    this.dialog.open(ShareExternallyComponent);
  }

  deletePost(post){
    this.postService.delete(post.id).subscribe(
      response => {
        this.utilitiesService.openSnackBar('Postagem deletada.');
      },
      error => {
        this.utilitiesService.openSnackBar(`Obtivemos um error`);
      }
    );
  }

  handleDeletePost(post){
    // console.log('Esse é o post: ', post);
    if ( post.authorUser ){
      this.user.id === post.authorUser.id ? this.deletePost(post) : '';
    }
    if ( post.authorProject ){
      this.user.id === post.authorProject.user_id ? this.deletePost(post) : '';
    }
    if ( post.authorOrganization ){
      this.user.id === post.authorOrganization.user_id ? this.deletePost(post) : '';
    }
    if ( post.authorEvent ){
      this.user.id === post.authorEvent.user_id ? this.deletePost(post) : '';
    }
  }

  getUserLogged(){
    this.userService.getAccountLogged().subscribe(
      response => {
        this.user = response;
        // console.log('usuário ===>', this.user);
      },
      error => {
        this.utilitiesService.openSnackBar(`${error.error.message}`);
      }
    );
  }

}
