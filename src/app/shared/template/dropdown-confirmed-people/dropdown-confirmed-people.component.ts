import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown-confirmed-people',
  templateUrl: './dropdown-confirmed-people.component.html',
  styleUrls: ['./dropdown-confirmed-people.component.css']
})
export class DropdownConfirmedPeopleComponent implements OnInit {

  panelOpenState = false;

  @Input('confirmedPeople') peopleConfirmed;
  @Input('className') className: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
