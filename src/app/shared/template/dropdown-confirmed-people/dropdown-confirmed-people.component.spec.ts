import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownConfirmedPeopleComponent } from './dropdown-confirmed-people.component';

describe('DropdownConfirmedPeopleComponent', () => {
  let component: DropdownConfirmedPeopleComponent;
  let fixture: ComponentFixture<DropdownConfirmedPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownConfirmedPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownConfirmedPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
