import { UtilitiesService } from './../../../core/services/utilities.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EventService } from './../../../core/services/event.service';
import { Component, OnChanges, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  donate = 'Doação de roupas';
  project = 'Moda Baixa Renda';
  entity = 'Belive Earth';
  partnership = 'Moda Sustentável';

  userSession = JSON.parse(sessionStorage.getItem('user')) || JSON.parse(localStorage.getItem('user'));

  id: number;
  userId: number;
  myProjects: Array<any> = [];
  myEvents: Array<any> = [];
  myOrganizations: Array<any> = [];
  myPartnerships: Array<any> = [];

  isLoadingProject = false;
  isLoadingEvent = false;
  isLoadingOrganization = false;
  isLoadingPartnership = false;

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private ngxSpinner: NgxSpinnerService,
    private utilitiesService: UtilitiesService,
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.reloadMyEvent();
    this.reloadMyProject();
    this.reloadMyOrganization();

    this.getMyProjects();
    this.getMyEvents();
    this.getMyOrganizations();
    this.getAccountLogged();
  }

  reloadMyEvent(){
    this.utilitiesService.myEventsReload.subscribe( response => {
      // console.log('RESPONSE:', response);
      this.isLoadingEvent = true;
      this.ngxSpinner.show();
      this.myEvents.splice(0, 1);
      this.myEvents.push(response);
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.isLoadingEvent = false;
      this.ngxSpinner.hide();
    });
  }

  reloadMyProject(){
    this.utilitiesService.myProjectsReload.subscribe( response => {
      this.isLoadingProject = true;
      this.ngxSpinner.show();
      this.myProjects.splice(0, 1);
      this.myProjects.push(response);
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.isLoadingProject = false;
      this.ngxSpinner.hide();
    });
  }

  reloadMyOrganization(){
    this.utilitiesService.myOrganizationsReload.subscribe( response => {
      this.isLoadingOrganization = true;
      this.ngxSpinner.show();
      this.myOrganizations.splice(0, 1);
      this.myOrganizations.push(response);
    }, error => {
      this.ngxSpinner.hide();
    }, () => {
      this.isLoadingOrganization = false;
      this.ngxSpinner.hide();
    });
  }

  modalPost(): void {
    $('#modalPost').modal('show');
  }

  getMyEvents(){
    this.isLoadingEvent = true;
    this.ngxSpinner.show();
    this.userService.getMyEvents(this.userSession.id).subscribe(response => {
      let length = response.ownerEvents.length;
      for (const [index, item] of response.ownerEvents.entries()) {
        if (index > length - 3) {
          this.myEvents.push(item);
        }
      }

      length = response.administratorsEvents.length;
      for (const [index, item] of response.administratorsEvents.entries()) {
        if (index > length - 3) {
          this.myEvents.push(item);
        }
      }
      // console.log('meus Eventos:', this.myEvents);
    },
    error => {
      console.log(error);
      this.isLoadingEvent = false;
      this.ngxSpinner.hide();
    }, () => {
      this.isLoadingEvent = false;
      this.ngxSpinner.hide();
    });
  }

  getMyProjects() {
    // Está com o this.id --- Pode trocar por this.userSession.id
    this.isLoadingProject = true;
    this.ngxSpinner.show();
    this.userService.getMyProjects(this.userSession.id).subscribe(
      response => {
        // console.log(response.ownerProjects);
        let length = response.ownerProjects.length;
        for (const [index, item] of response.ownerProjects.entries()) {
          if (index > length - 3) {
          this.myProjects.push(item);
          }
        }

        length = response.administratorsProjects.length;
        for (const [index, item] of response.administratorsProjects.entries()) {
          if (index > length - 3) {
          this.myProjects.push(item);
          }
        }

      }, error => {
        console.log(error);
        this.isLoadingProject = false;
        this.ngxSpinner.hide();
      }, () => {
        this.isLoadingProject = false;
        this.ngxSpinner.hide();
      }
    );
  }

  getMyOrganizations(){
    this.isLoadingOrganization = true;
    this.ngxSpinner.show();
    this.userService.getMyOrganizations(this.userSession.id).subscribe(response => {
      let length = response.ownerOrganizations.length;
      for (const [index, item] of response.ownerOrganizations.entries()) {
        if (index > length - 3) {
          this.myOrganizations.push(item);
        }
      }

      length = response.administratorsOrganizations.length;
      for (const [index, item] of response.administratorsOrganizations.entries()) {
        if (index > length - 3) {
          this.myOrganizations.push(item);
        }
      }
      // console.log('meus Eventos:', this.myEvents);
    },
    error => {
      console.log(error);
      this.isLoadingOrganization = false;
      this.ngxSpinner.hide();
    }, () => {
      this.isLoadingOrganization = false;
      this.ngxSpinner.hide();
    });
  }

  getAccountLogged() {
    this.userService.getAccountLogged().subscribe(response => {
      this.userId = response.id;
    }, error => {

    }, () => {
      this.getMyPartnerships();
    }
    );
  }

  getMyPartnerships() {
    this.isLoadingPartnership = true;
    this.ngxSpinner.show();
    this.userService.getMyPartnerships(this.userId, '', 1, 1, 1, 1).subscribe(response => {
      // if (response.userPartners.total > 0) {
      //   this.myPartnerships.push(response.userPartners.data[0].users);
      // }
      // console.log('RESPONSE PARTNER: ', response);
      if (response.eventPartners.total > 0) {
        this.myPartnerships.push(response.eventPartners.data.slice(-1)[0].eventData);
      }
      if (response.projectPartners.total > 0) {
        this.myPartnerships.push(response.projectPartners.data.slice(-1)[0].project);
      }
      if (response.organizationPartners.total > 0) {
        this.myPartnerships.push(response.organizationPartners.data.slice(-1)[0].organization);
      }
     }, error => {

     }, () => {
       this.isLoadingPartnership = false;
       this.ngxSpinner.hide();
     });
  }

}
