import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatOrganizationsComponent } from './chat-organizations.component';

describe('ChatOrganizationsComponent', () => {
  let component: ChatOrganizationsComponent;
  let fixture: ComponentFixture<ChatOrganizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatOrganizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatOrganizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
