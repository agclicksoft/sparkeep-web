import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-chat-organizations',
  templateUrl: './chat-organizations.component.html',
  styleUrls: ['./chat-organizations.component.css']
})
export class ChatOrganizationsComponent implements OnInit {

  @Input() organizationPartners;
  @Input() page = 1;
  @Output() pageChange = new EventEmitter();
  @Output() receiverMessage = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onClickOrganization(organization) {
    this.receiverMessage.emit(organization);
  }

}
