import { UtilitiesService } from './../../../../core/services/utilities.service';
import { PartnershipService } from './../../../../core/services/partnership.service';
import { Component, OnInit, Input} from '@angular/core';
import * as moment from 'moment';

type typeChat = 'Send' | 'Received';
@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.css']
})
export class ChatMessageComponent implements OnInit{

  @Input() object;

  @Input() shortages;
  @Input() maintenance_times;

  @Input() typeMessage: typeChat = 'Send';

  constructor(
    private partnerService: PartnershipService,
    private utilitiesService: UtilitiesService,
  ) { }

  ngOnInit(): void {
  }

  handleShortageName(id) {
    // console.log("Valor do shortages: ", this.shortages);
    for (const item of this.shortages) {
      if (id === item.id) {
        return item.description;
      }
    }
  }

  handleMaintenanceTime(id){
    for (const item of this.maintenance_times){
      if (id === item.id){
        return item.description;
      }
    }
  }

  acceptPartnership(id, index) {
    const object = {
      status: 1
    };

    this.object.myProfile.splice(index, 1);
    this.partnerService.update(id, object).subscribe(
      response => {
        // console.log("Valor da response: ", response);
        this.utilitiesService.openSnackBar('Pedido de parceria aceito!');

      },
      error => {
        this.utilitiesService.openSnackBar(`Obtivemos um erro ao aceitar a parceria. Error: ${error.error.message}`);
      }
    );
  }

  declinePartnership(id) {
    const object = {
      status: 0
    };
    this.partnerService.update(id, object).subscribe(
      response => {
        // console.log("Valor da response: ", response);
        this.utilitiesService.openSnackBar('Pedido de parceria recusado!');
      },
      error => {
        this.utilitiesService.openSnackBar(`Obtivemos um erro ao recusar a parceria. Error: ${error.error.message}`);
      }
    );
  }

  acceptAllPartnership() {
    const partners = {
      partnersIds: [],
    };

    for (const item of this.object.myProfile) {
      partners.partnersIds.push(item.pivot.id);
    }

    this.partnerService.acceptAll(partners).subscribe(
      response => {
        this.utilitiesService.openSnackBar('Todos os pedidos de parcerias aceito!');
      },
      error => {
        this.utilitiesService.openSnackBar(`Obtivemos um error ao tentar aceitar todas as parcerias, Erro: ${error.error.message}`);
      },
      () => {
        this.object.myProfile = null;
      }
    );
  }

  declineAllPartnership() {

    const partners = {
      partnersIds: [],
    };

    for (const item of this.object.myProfile) {
      partners.partnersIds.push(item.pivot.id);
    }

    this.partnerService.rejectAll(partners).subscribe(
      response => {
        this.utilitiesService.openSnackBar('Todos os pedidos de parcerias recusado!');
      },
      error => {
        this.utilitiesService.openSnackBar(`Obtivemos um error ao tentar recusar todas as parcerias, Erro: ${error.error.message}`);
      }
    );
  }
}
