import { Component, Input, OnInit, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
  selector: 'app-chat-users',
  templateUrl: './chat-users.component.html',
  styleUrls: ['./chat-users.component.css'],
})
export class ChatUsersComponent implements OnInit {

  @Input() usersPartners: any;
  @Input() page = 1;
  @Output() mudouPagina = new EventEmitter();
  @Output() receiverMessage = new EventEmitter();

  constructor() { }

  ngOnInit(): void { }

  onClickUser(user) {
    this.receiverMessage.emit(user);
  }
}
