import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
@Component({
  selector: 'app-chat-events',
  templateUrl: './chat-events.component.html',
  styleUrls: ['./chat-events.component.css']
})
export class ChatEventsComponent implements OnInit {

  @Input() eventPartners;
  @Input() page = 1;
  @Output() pageChange = new EventEmitter();
  @Output() receiverMessage = new EventEmitter();

  totalPagination = 0;

  constructor() { }
  ngOnInit(): void {
  }

  onClickEvent(event) {
    this.receiverMessage.emit(event);
  }

}
