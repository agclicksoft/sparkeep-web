import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatEventsComponent } from './chat-events.component';

describe('ChatEventsComponent', () => {
  let component: ChatEventsComponent;
  let fixture: ComponentFixture<ChatEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
