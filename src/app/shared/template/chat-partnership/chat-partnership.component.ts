import { MaintenanceTypeService } from './../../../core/services/maintenance-type.service';
import { PartnershipService } from './../../../core/services/partnership.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { Component, OnInit, OnChanges, SimpleChanges, HostListener } from '@angular/core';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from 'src/app/core/services/user.service';

type typeMenu = 'Send' | 'Received';
type profileMenu = 'user' | 'event' | 'project' | 'organization';
@Component({
  selector: 'app-chat-partnership',
  templateUrl: './chat-partnership.component.html',
  styleUrls: ['./chat-partnership.component.css']
})
export class ChatPartnershipComponent implements OnInit {

  isLoading = true;

  url: string;
  viewMessage = false;

  objectReceiver;

  userId;

  partner: any;

  keyword = '';

  typeMenu: typeMenu = 'Send';
  profileMenu: profileMenu = 'user'; // Submenus

  // Páginas de pesquisa
  userPage = 1;
  eventPage = 1;
  projectPage = 1;
  organizationPage = 1;

  shortages;
  maintenance_times;

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private partnerService: PartnershipService,
    private utilService: UtilitiesService,
    private ngxSpinner: NgxSpinnerService,
    private shortagesService: MaintenanceTypeService,
  ) {
  }

  handleTypeMenu(type): void {
    this.getAccountLogged();
    this.partner = null;
    this.getParthership(this.userId, '', 1, 1, 1, 1);
    this.objectReceiver = null;
    this.userPage = 1;
    this.eventPage = 1;
    this.projectPage = 1;
    this.organizationPage = 1;
  }

  ngOnInit(): void {
    this.ngxSpinner.show();
    this.getAllShortages();
    this.getAllMaintenanceTime();
    this.getAccountLogged();
  }

  getParthership(userId, keyword, userPage, eventPage, projectPage, organizantionPage): void {
    this.partner = null;
    this.partnerService.chat(userId, keyword, userPage, eventPage, projectPage, organizantionPage).subscribe(response => {

      console.log(response);

      this.typeMenu === 'Send' ? this.partner = response.send : this.partner = response.received;
      // console.log('Partner Valor: ', response);

    }, error => {
      this.utilService.openSnackBar('Não conseguimos buscar os pedidos de parcerias.');
      this.ngxSpinner.hide();
      this.isLoading = false;
    }, () => {
      this.ngxSpinner.hide();
      this.isLoading = false;
    });
  }

  getAccountLogged(): void {
    this.userService.getAccountLogged().subscribe(response => {
      this.userId = response.id;
    }, error => {
      this.ngxSpinner.hide();
      this.isLoading = false;
    }, () => {
      this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
    }
    );
  }

  getAllShortages() {
    this.shortagesService.getAll().subscribe(
      response => {
        this.shortages = response;
        // console.log('Valor shortages component pai', this.shortages);
      },
      error => {
        this.utilService.openSnackBar('Não foi possível buscar as carências.');
      }
    );
  }
  getAllMaintenanceTime() {
    this.shortagesService.getAllTimes().subscribe(
      response => {
        this.maintenance_times = response;
        // console.log('Valor shortages component pai', this.shortages);
      },
      error => {
        this.utilService.openSnackBar('Não foi possível buscar os tempos de carências.');
      }
    );
  }

  onSubmit() {
    this.dialog.open(ConfirmDialogComponent);
  }

  showMessage(data): void {
    this.viewMessage = true;
    this.objectReceiver = data;
  }

  changePageUser(event) {
    this.userPage = event.page;
    this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }
  changePageEvent(event) {
    this.eventPage = event.page;
    this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }
  changePageProject(event) {
    this.projectPage = event.page;
    this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }
  changePageOrganization(event) {
    this.organizationPage = event.page;
    this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
  }

  // Ao clicar no perfil
  receiverMessageUser(event) {
    this.objectReceiver = event;
    // console.log('Object receiver', this.objectReceiver);
  }
  receiverMessageProject(event) {
    this.objectReceiver = event;
    // console.log('Object receiver', this.objectReceiver);
  }
  receiverMessageEvent(event) {
    this.objectReceiver = event;
    // console.log('Object receiver', this.objectReceiver);
  }
  receiverMessageOrganization(event) {
    this.objectReceiver = event;
    // console.log('Object receiver', this.objectReceiver);
  }

  handleSearch(event) {
    if (event) {
      this.keyword = event;
      this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
    } else {
      this.keyword = '';
      this.getParthership(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
    }
  }

  handleAppendRequests(userId, keyword, userPage, eventPage, projectPage, organizantionPage) {
    this.partnerService.chat(userId, keyword, userPage, eventPage, projectPage, organizantionPage).subscribe(response => {

      // console.log('Response', response.send);
      // this.typeMenu === 'Send' ? this.partner = [response.send] : this.partner = response.received;
      // console.log('Partner Valor: ', response);

      if (this.typeMenu === 'Send') {
        switch (this.profileMenu) {
          case 'user': {
            this.partner.usersPartners.data =
              [...this.partner.usersPartners.data, ...response.send.usersPartners.data];
            break;
          }
          case 'event': {
            this.partner.eventPartners.data =
              [...this.partner.eventPartners.data, ...response.send.eventPartners.data];
            break;
          }
          case 'project': {
            this.partner.projectPartners.data =
              [...this.partner.projectPartners.data, ...response.send.projectPartners.data];
            break;
          }
          case 'organization': {
            this.partner.organizationPartners.data =
              [...this.partner.organizationPartners.data, ...response.send.organizationPartners.data];
            break;
          }
        }
      }

    }, error => {
      this.utilService.openSnackBar('Não conseguimos buscar os pedidos de parcerias.');
      this.ngxSpinner.hide();
      this.isLoading = false;
    }, () => {
      this.ngxSpinner.hide();
      this.isLoading = false;
    });
  }

  onChatScroll(event: any) {
    const maxScroll = event.target.scrollHeight - event.target.clientHeight;
    // console.log('Evento: ', event);
    if (event.target.scrollTop === maxScroll) {
      switch (this.profileMenu) {
        case 'user': {
          if (this.userPage <= this.partner?.usersPartners?.lastPage) {
            this.userPage++;
            this.handleAppendRequests(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
          }
          break;
        }
        case 'event': {
          if (this.eventPage <= this.partner?.eventsPartners?.lastPage) {
            this.eventPage++;
            this.handleAppendRequests(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
          }
          break;
        }
        case 'project': {
          if (this.projectPage <= this.partner?.projectPartners?.lastPage) {
            this.projectPage++;
            this.handleAppendRequests(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
          }
          break;
        }
        case 'organization': {
          if (this.organizationPage <= this.partner?.organizationPartners?.lastPage) {
            this.organizationPage++;
            this.handleAppendRequests(this.userId, this.keyword, this.userPage, this.eventPage, this.projectPage, this.organizationPage);
          }
          break;
        }
      }
    }

  }


}
