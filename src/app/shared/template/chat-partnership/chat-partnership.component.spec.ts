import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatPartnershipComponent } from './chat-partnership.component';

describe('ChatPartnershipComponent', () => {
  let component: ChatPartnershipComponent;
  let fixture: ComponentFixture<ChatPartnershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatPartnershipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatPartnershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
