import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';


@Component({
  selector: 'app-chat-projects',
  templateUrl: './chat-projects.component.html',
  styleUrls: ['./chat-projects.component.css']
})
export class ChatProjectsComponent implements OnInit {

  @Input() projectPartners;
  @Input() page = 1;
  @Output() pageChange = new EventEmitter();
  @Output() receiverMessage = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  // changePage(page) {
  //   // console.log(page);
  // }

  // previous() {
  //   if (this.page <= 1) {
  //     this.page = 1;
  //   } else {
  //     this.page--;
  //   }
  //   this.pageChange.emit({ page: this.page });
  // }
  // next() {
  //   this.page++;
  //   this.pageChange.emit({ page: this.page });
  // }

  onClickProject(project) {
    this.receiverMessage.emit(project);
  }
}
