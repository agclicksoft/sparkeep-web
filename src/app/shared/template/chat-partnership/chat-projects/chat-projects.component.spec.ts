import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatProjectsComponent } from './chat-projects.component';

describe('ChatProjectsComponent', () => {
  let component: ChatProjectsComponent;
  let fixture: ComponentFixture<ChatProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
