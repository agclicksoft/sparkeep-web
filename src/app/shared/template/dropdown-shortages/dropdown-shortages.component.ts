import { Component, Input, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-dropdown-shortages',
  templateUrl: './dropdown-shortages.component.html',
  styleUrls: ['./dropdown-shortages.component.css']
})
export class DropdownShortagesComponent implements OnInit {

  panelOpenState = false;

  @Input('shortages') myShortages = null;
  @Input() kind_of_help = null;
  @Input('className') className = '';

  constructor() { }

  ngOnInit(): void {
  }

}
