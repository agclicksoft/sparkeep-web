import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownShortagesComponent } from './dropdown-shortages.component';

describe('DropdownShortagesComponent', () => {
  let component: DropdownShortagesComponent;
  let fixture: ComponentFixture<DropdownShortagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownShortagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownShortagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
