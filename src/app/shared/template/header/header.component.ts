import { NotificationService } from './../../../core/services/notification.service';
import { UserService } from './../../../core/services/user.service';
import { Router } from '@angular/router';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { Component, Input, OnInit } from '@angular/core';
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userSession = JSON.parse(sessionStorage.getItem('user'));
  @Input() sideNavRight; // SideNavRight --- Variavel que faz abrir o menu.

  search = null;
  hiddenNotify;
  hiddenChat = false;
  userId;

  constructor(
    private utilitiesService: UtilitiesService,
    private router: Router,
    private userService: UserService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.getAccountLogged();
  }

  toogleBadgeVisibility() {
    this.notificationService.checkNotification(this.userId).subscribe(response => {
      this.hiddenNotify = true;
    });
  }

  toogleChatBadgeVisibility() {
    this.notificationService.checkChatNotification(this.userId).subscribe(response => {
      this.hiddenChat = true;
    });
  }

  getAllChatNotification(id){
    this.notificationService.haveChatNotificationNotViewed(id).subscribe(response => {
      response.show_badge ? this.hiddenChat = false : this.hiddenChat = true;
    });
  }

  getAllNotification(id) {
    this.notificationService.haveNotificationNotViewed(id).subscribe(response => {

      if (response.show_badge) {
        this.hiddenNotify = false;
        // console.log(this.hidden);
      } else {
        this.hiddenNotify = true;
        // console.log(this.hidden);
      }
    });
  }

  getAccountLogged(): void {
    this.userService.getAccountLogged().subscribe(response => {
      this.userId = response.id;
    }, error => {

    }, () => {
      this.getAllNotification(this.userId);
      this.getAllChatNotification(this.userId);
    });
  }

  btnFiltro() {
    $('#modalFiltro').modal('show');
  }

  openSideNav(){
    this.sideNavRight = !this.sideNavRight;
    console.log('Valor do sideNav: ', this.sideNavRight);
  }

  gotoSearch(){
    if (this.router.url !== '/pesquisar'){
      switch (this.router.url){
        case '/pesquisar/pessoas': {
          this.router.navigate(['/pesquisar/pessoas']);
          break;
        }
        case '/pesquisar/entidades': {
          this.router.navigate(['/pesquisar/entidades']);
          break;
        }
        case '/pesquisar/projetos': {
          this.router.navigate(['/pesquisar/projetos']);
          break;
        }
        case '/pesquisar/eventos': {
          this.router.navigate(['/pesquisar/eventos']);
          break;
        }
        case '/pesquisar/localizacao': {
          this.router.navigate(['/pesquisar/localizacao']);
          break;
        }
        case '/pesquisar/paixoes': {
          this.router.navigate(['/pesquisar/paixoes']);
          break;
        }
        case '/pesquisar/ofertas': {
          this.router.navigate(['/pesquisar/ofertas']);
          break;
        }
        case '/pesquisar/demandas': {
          this.router.navigate(['/pesquisar/demandas']);
          break;
        }
        default: {
          this.router.navigate(['/pesquisar']);
        }
      }
    }else{

    }
  }

  handleSearch(event){
    if (event){
      // event.preventDefault();
      this.utilitiesService.globalSearch.emit(event);
    }else{
      this.utilitiesService.globalSearch.emit('');
    }
  }

}
