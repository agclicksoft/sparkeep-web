import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-localizacao-projetos',
  templateUrl: './localizacao-projetos.component.html',
  styleUrls: ['./localizacao-projetos.component.css']
})
export class LocalizacaoProjetosComponent implements OnInit {
  @Input('projects') projects: Projects;
  @Input() page = 1;
  @Output() mudouPagina = new EventEmitter();

  ngOnInit() {}

  changePage(page) {
    console.log(page);
  }

  previous() {
    if (this.page <= 1 ) {
      this.page = 1;
    } else {
      this.page--;
    }
    this.mudouPagina.emit({page: this.page});
  }
  next() {
    this.page++;
    this.mudouPagina.emit({page: this.page});
  }

}

interface Projects {
  data: [];
  total;
  perPage;
  lastPage;
}
