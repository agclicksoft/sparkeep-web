import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalizacaoProjetosComponent } from './localizacao-projetos.component';

describe('LocalizacaoProjetosComponent', () => {
  let component: LocalizacaoProjetosComponent;
  let fixture: ComponentFixture<LocalizacaoProjetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalizacaoProjetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizacaoProjetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
