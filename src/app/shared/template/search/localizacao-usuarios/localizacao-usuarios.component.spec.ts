import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalizacaoUsuariosComponent } from './localizacao-usuarios.component';

describe('LocalizacaoUsuariosComponent', () => {
  let component: LocalizacaoUsuariosComponent;
  let fixture: ComponentFixture<LocalizacaoUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalizacaoUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizacaoUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
