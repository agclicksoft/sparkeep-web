import { EventEmitter, Output } from '@angular/core';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-localizacao-usuarios',
  templateUrl: './localizacao-usuarios.component.html',
  styleUrls: ['./localizacao-usuarios.component.css']
})
export class LocalizacaoUsuariosComponent implements OnInit {

  @Input('users') users: Users;
  @Input() page = 1;
  @Output() mudouPagina = new EventEmitter();

  ngOnInit() {}

  changePage(page) {
    console.log(page);
  }

  previous() {
    if (this.page <= 1 ) {
      this.page = 1;
    } else {
      this.page--;
    }
    this.mudouPagina.emit({page: this.page});
  }
  next() {
    this.page++;
    this.mudouPagina.emit({page: this.page});
  }

}

interface Users {
  data: [];
  total;
  perPage;
  lastPage;
}
