import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-localizacao-eventos',
  templateUrl: './localizacao-eventos.component.html',
  styleUrls: ['./localizacao-eventos.component.css']
})
export class LocalizacaoEventosComponent implements OnInit {

  @Input('events') events: Events;
  @Input() page = 1;
  @Output() mudouPagina = new EventEmitter();

  ngOnInit() {}

  changePage(page) {
    console.log(page);
  }

  previous() {
    if (this.page <= 1 ) {
      this.page = 1;
    } else {
      this.page--;
    }
    this.mudouPagina.emit({page: this.page});
  }
  next() {
    this.page++;
    this.mudouPagina.emit({page: this.page});
  }

}

interface Events {
  data: [];
  total;
  perPage;
  lastPage;
}
