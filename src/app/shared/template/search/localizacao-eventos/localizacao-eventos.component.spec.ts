import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalizacaoEventosComponent } from './localizacao-eventos.component';

describe('LocalizaocaoEventosComponent', () => {
  let component: LocalizacaoEventosComponent;
  let fixture: ComponentFixture<LocalizacaoEventosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalizacaoEventosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizacaoEventosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
