import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-localizacao-entidades',
  templateUrl: './localizacao-entidades.component.html',
  styleUrls: ['./localizacao-entidades.component.css']
})
export class LocalizacaoEntidadesComponent implements OnInit {

  @Input('organizations') organizations: Organizations;
  @Input() page = 1;
  @Output() mudouPagina = new EventEmitter();

  ngOnInit() {}

  changePage(page) {
    // console.log(page);
  }

  previous() {
    if (this.page <= 1 ) {
      this.page = 1;
    } else {
      this.page--;
    }
    this.mudouPagina.emit({page: this.page});
  }
  next() {
    this.page++;
    this.mudouPagina.emit({page: this.page});
  }

}

interface Organizations {
  data: [];
  total;
  perPage;
  lastPage;
}
