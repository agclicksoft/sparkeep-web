import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalizacaoEntidadesComponent } from './localizacao-entidades.component';

describe('LocalizacaoEntidadesComponent', () => {
  let component: LocalizacaoEntidadesComponent;
  let fixture: ComponentFixture<LocalizacaoEntidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalizacaoEntidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizacaoEntidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
