import { EventService } from './../../../core/services/event.service';
import { PartnershipService } from './../../../core/services/partnership.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MaintenanceTypeService } from './../../../core/services/maintenance-type.service';
import { OrganizationService } from './../../../core/services/organization.service';
import { ProjectService } from './../../../core/services/project.service';
import { UtilitiesService } from './../../../core/services/utilities.service';
import { SuccessDialogComponent } from './../success-dialog/success-dialog.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Component, Input, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-my-partners-dialog',
  templateUrl: './my-partners-dialog.component.html',
  styleUrls: ['./my-partners-dialog.component.css'],
})
export class MyPartnersDialogComponent implements OnInit {
  id: number;
  url: string;

  myProjects;
  myOrganizations;
  myEvents;

  receiver; // obtem os valores da parceria.

  shortages;

  hasPartnershipAgreement = false;

  userSession = JSON.parse(sessionStorage.getItem('user'));

  formPartner: FormGroup;

  imgFromPartner = '/assets/img/user.svg';

  isSubmited = false;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<MyPartnersDialogComponent>,
    private utilService: UtilitiesService,
    private partnershipService: PartnershipService,
    private projectService: ProjectService,
    private organizationService: OrganizationService,
    private eventService: EventService,
    private maintenanceService: MaintenanceTypeService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.receiver = this.utilService.partnershipReceiverValue;
  }

  ngOnInit(): void {
    // console.log('Valor Do receiver: ', this.receiver);

    this.formPartner = this.fb.group({
      message: ['', [Validators.required]],
      shortage_id: ['', [Validators.required]],
      maintenance_time_id: ['', [Validators.required]],
      partnership_agreement: [''],
      user_send_id: null,
      user_receiver_id: null,
      organization_send_id: null,
      organization_receiver_id: null,
      project_send_id: null,
      project_receiver_id: null,
      event_send_id: null,
      event_receiver_id: null,
      status: 0,
      select_validation: ['', [Validators.required]],
    });

    this.receiverType(this.receiver.role, this.receiver.id);
    this.handleNoHasShortage(this.data?.has_shortages ? true : false);
    this.getMyOrganizations();
    this.getMyProjects();
    this.getMyEvents();
    this.getShortages();
  }

  onSubmit() {
    this.isSubmited = true;
    // console.log('FORMULÁRIO DE SOLICITAÇÃO DE PARCERIA: ', this.formPartner.value);
    if (this.formPartner.status === 'VALID') {
      this.partnershipService.create(this.formPartner.value).subscribe(
        (response) => {
          this.utilService.openSnackBar(
            'Solicitação de parceria enviada com sucesso!'
          );
          console.log('Response parceria enviada: ', response);
        },
        (error) => {
          this.utilService.openSnackBar(
            `Oops, obtivemos um erro, ERRO: ${error.error.message}`
          );
        },
        () => {
          this.dialog.open(SuccessDialogComponent);
          this.dialogRef.close();
        }
      );
    } else {
      this.utilService.openSnackBar(`Campos Inválidos, insira corretamente.`);
    }
  }

  getMyProjects() {
    this.projectService.getMyProjects(this.userSession.id).subscribe(
      (response) => {
        // console.log('MODAL PROPOR - ORGANIZAÇÃO: ', response);
        this.myProjects = [
          ...response.ownerProjects,
          ...response.administratorsProjects,
        ];
      },
      (error) => {
        this.utilService.openSnackBar(
          `Oops, obtivemos um erro. Error: ${error.error.message}`
        );
      },
      () => {
        // console.log('My Projects com o spreed: ', this.myProjects);
      }
    );
  }

  getMyOrganizations() {
    this.organizationService
      .getAllUserOrganizations(this.userSession.id)
      .subscribe(
        (response) => {
          // console.log('MODAL PROPOR - ORGANIZAÇÃO: ', response);
          this.myOrganizations = [
            ...response.ownerOrganizations,
            ...response.administratorsOrganizations,
          ];
        },
        (error) => {
          this.utilService.openSnackBar(
            `Oops, obtivemos um erro. Error: ${error.error.message}`
          );
        },
        () => {
          // console.log('My Organizations com o spreed: ', this.myOrganizations);
        }
      );
  }

  getMyEvents() {
    this.eventService.getAllUserEvents(this.userSession.id).subscribe(
      (response) => {
        this.myEvents = [
          ...response.ownerEvents,
          ...response.administratorsEvents,
        ];
      },
      (error) => {
        this.utilService.openSnackBar(
          `Oops, obtivemos um erro. Error: ${error.error.message}`
        );
      },
      () => {
        // console.log('My Organizations com o spreed: ', this.myOrganizations);
      }
    );
  }

  selectChange(event, value) {
    const roleSend = value.split('_', 2);
    this.switchSend(roleSend[0], roleSend[1]);
    switch (roleSend[0]) {
      case 'user':
        this.imgFromPartner =
          this.userSession.image_url !== null
            ? this.userSession.image_url
            : '/assets/img/user.svg';
        break;
      case 'event': {
        this.setEventImg(roleSend[1]);
        break;
      }
      case 'organization':
        this.setOrganizationImg(roleSend[1]);
        break;
      case 'project':
        this.setProjectImg(roleSend[1]);
        break;
    }
  }

  setOrganizationImg(id) {
    for (const organization of this.myOrganizations) {
      // tslint:disable-next-line: radix
      if (organization.id === parseInt(id)) {
        this.imgFromPartner = organization.image_url;
      }
    }
  }

  setProjectImg(id) {
    for (const project of this.myProjects) {
      // tslint:disable-next-line: radix
      if (project.id === parseInt(id)) {
        this.imgFromPartner = project.image_url;
      }
    }
  }

  setEventImg(id) {
    for (const event of this.myEvents) {
      // tslint:disable-next-line: radix
      if (event.id === parseInt(id)) {
        this.imgFromPartner = event.image_url;
      }
    }
  }

  receiverType(role, id) {
    this.formPartner.patchValue({
      user_receiver_id: null,
      project_receiver_id: null,
      organization_receiver_id: null,
      event_receiver_id: null,
    });
    switch (role) {
      case 'user': {
        this.formPartner.controls.user_receiver_id.patchValue(id);
        break;
      }
      case 'project': {
        this.formPartner.controls.project_receiver_id.patchValue(id);
        break;
      }
      case 'organization': {
        this.formPartner.controls.organization_receiver_id.patchValue(id);
        break;
      }
      case 'event': {
        this.formPartner.controls.event_receiver_id.patchValue(id);
        break;
      }
    }
  }

  switchSend(value, id) {
    this.formPartner.patchValue({
      user_send_id: null,
      organization_send_id: null,
      project_send_id: null,
      event_send_id: null,
    });
    switch (value) {
      case 'user': {
        this.formPartner.controls.user_send_id.patchValue(id);
        break;
      }
      case 'project': {
        this.formPartner.controls.project_send_id.patchValue(id);
        break;
      }
      case 'organization': {
        this.formPartner.controls.organization_send_id.patchValue(id);
        break;
      }
      case 'event': {
        this.formPartner.controls.event_send_id.patchValue(id);
        break;
      }
    }
  }

  getShortages() {
    this.maintenanceService.getAll().subscribe(
      (response) => {
        this.shortages = response;
      },
      (error) => {
        this.utilService.openSnackBar(
          'Oops, tivemos um error ao obter os tipos de ajuda.'
        );
      }
    );
  }

  handlePartnershipAgreement(id) {
    this.formPartner.get('partnership_agreement').clearValidators();
    this.formPartner.controls.partnership_agreement.patchValue(null);
    for (const item of this.shortages) {
      if (item.description === 'Patrocínio financeiro' && item.id == id) {
        this.hasPartnershipAgreement = true;
        this.formPartner.controls.partnership_agreement.setValidators([
          Validators.required,
        ]);
        break;
      } else {
        this.hasPartnershipAgreement = false;
      }
    }
  }

  handleNoHasShortage(hasShortages: boolean) {
    if (!hasShortages || hasShortages !== null) {
      this.formPartner.get('shortage_id').clearValidators();
      this.formPartner.controls.shortage_id.patchValue('');
      this.formPartner.get('maintenance_time_id').clearValidators();
      this.formPartner.controls.maintenance_time_id.patchValue('');
    } else{
      this.formPartner.controls.shortage_id.setValidators([
        Validators.required,
      ]);
      this.formPartner.controls.maintenance_time_id.setValidators([
        Validators.required,
      ]);
    }
  }
}
interface DialogData {
  shortages: any[];
  maintenance_times: any[];
  has_shortages: boolean;
}
