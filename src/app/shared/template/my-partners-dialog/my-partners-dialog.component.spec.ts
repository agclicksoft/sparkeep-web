import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPartnersDialogComponent } from './my-partners-dialog.component';

describe('MyPartnersDialogComponent', () => {
  let component: MyPartnersDialogComponent;
  let fixture: ComponentFixture<MyPartnersDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPartnersDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPartnersDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
