import { User } from './user.model';
import { Passion } from './passion.model';
import { Post } from './post.model';

export class Project {
  id?: any;
  user_id: number;
  organization_id: number;
  name: string;
  description: string;
  local: string;
  telephone: string;
  cellphone: string;
  show_telephone: boolean;
  show_cellphone: boolean;
  maintenance_time?: string;
  maintenance_times?: string;
  image_url: string;
  posts: Array<Post> = [];
  administrators: Array<User> = [];
  passions: Array<Passion> = [];
  shortages: Array<any> = [];
  address_neighborhood?: string;
  address_city?: string;
  address_state?: string;
  kind_of_help: any;
  has_shortages: boolean;

  data?: Array<projectData> = [];
  total?: number;
  perPage?: number;
  page?: number;
  lastPage?: number;

  youtube?: string;
  facebook?: string;
  instagram?: string;
  twitter?: string;
  site?: string;
}

class projectData {
  id?: any;
  user_id: number;
  organization_id: number;
  name: string;
  description: string;
  local: string;
  telephone: string;
  show_telephone: boolean;
  maintenance_time?: string;
  maintenance_times?: string;
  image_url: string;
  posts: Array<Post> = [];
  administrators: Array<User> = [];
  passions: Array<Passion> = [];
  shortages: Array<any> = [];
  address_neighborhood?: string;
  address_city?: string;
  address_state?: string;
  has_shortages: boolean;
}
