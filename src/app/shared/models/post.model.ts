import { User } from './user.model';
import { Image } from './image.model';
import { Passion } from './passion.model';

export class Post{
  id?: number;
  user_id?: number;
  author?: string;
  description?: string;
  privacity?: string;
  liked?: boolean;
  created_at?: string;
  updated_at?: string;
  author_user_id?: number;
  author_event_id?: number;
  author_organization_id?: number;
  author_project_id?: number;
  authorImage?: any;
  authorUser?: User;
  authorName?: any;
  authorEvent?: any;
  authorProject?: any;
  passions?: Passion[];
  images?: Image[] = [];
  inspireds: any[];
  data?: Array<PostInfo>;
  total?: number;
  perPage?: number;
  page?: number;
  lastPage?: number;
  __meta__: any;
}

class PostInfo {
  id?: number;
  user_id?: number;
  author?: string;
  description?: string;
  privacity?: string;
  liked?: boolean;
  created_at?: string;
  updated_at?: string;
  author_user_id?: number;
  author_event_id?: number;
  author_organization_id?: number;
  author_project_id?: number;
  authorImage?: any;
  authorUser?: User;
  authorName?: any;
  authorEvent?: any;
  authorProject?: any;
  passions?: Passion[];
  images?: Image[] = [];
  inspireds: any[];
  __meta__: any;
  owner?: boolean;
}
