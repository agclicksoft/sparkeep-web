export class Image {
  id?: number;
  post_id?: number;
  path?: string;
  key?: string;
  name?: string;
  created_at: string;
  updated_at: string;
}
