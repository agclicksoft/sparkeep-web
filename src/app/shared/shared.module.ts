import { TermsPoliciesAboutComponent } from 'src/app/shared/template/terms-policies-about/terms-policies-about.component';
import { CommonModule } from '@angular/common';

import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
registerLocaleData(ptBr);

// Routes module
import { Routes, RouterModule } from '@angular/router';

// Imports modulos importantes
import { TokenInterceptor } from './../core/interceptors/token.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

// Social Login
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angularx-social-login';

// Button share
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';

// Imports MaterialDesign
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// Mask
import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from 'ngx-currency';

// Components
import { SidenavComponent } from './template/sidenav/sidenav.component';
import { HeaderComponent } from './template/header/header.component';
import { PassionsFormComponent } from './template/passions-form/passions-form.component';
import { ShortagesFormComponent } from './template/shortages-form/shortages-form.component';
import { AddAdministratorsComponent } from './template/add-administrators/add-administrators.component';
import { FullSpinnerComponent } from './spinner/full-spinner/full-spinner.component';
import { PartialSpinnerComponent } from './spinner/partial-spinner/partial-spinner.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PostComponent } from './template/post/post.component';
import { DropdownPassionsComponent } from './template/dropdown-passions/dropdown-passions.component';
import { DropdownProjectsComponent } from './template/dropdown-projects/dropdown-projects.component';
import { DropdownPartnershipsComponent } from './template/dropdown-partnerships/dropdown-partnerships.component';
import { DropdownEventsComponent } from './template/dropdown-events/dropdown-events.component';
import { MediumSpinnerComponent } from './spinner/medium-spinner/medium-spinner.component';
import { DropdownShortagesComponent } from './template/dropdown-shortages/dropdown-shortages.component';
import { DropdownConfirmedPeopleComponent } from './template/dropdown-confirmed-people/dropdown-confirmed-people.component';
import { MyPartnersDialogComponent } from './template/my-partners-dialog/my-partners-dialog.component';
import { SuccessDialogComponent } from './template/success-dialog/success-dialog.component';
import { ChatPartnershipComponent } from './template/chat-partnership/chat-partnership.component';
import { ConfirmDialogComponent } from './template/confirm-dialog/confirm-dialog.component';
import { ShareExternallyComponent } from './template/share-externally/share-externally.component';
import { NewPostComponent } from './template/new-post/new-post.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { MyPartnershipDialogComponent } from './template/my-partnership-dialog/my-partnership-dialog.component';
import { LocalizacaoUsuariosComponent } from './template/search/localizacao-usuarios/localizacao-usuarios.component';
import { LocalizacaoEventosComponent } from './template/search/localizacao-eventos/localizacao-eventos.component';
import { LocalizacaoProjetosComponent } from './template/search/localizacao-projetos/localizacao-projetos.component';
import { LocalizacaoEntidadesComponent } from './template/search/localizacao-entidades/localizacao-entidades.component';
import { ChatUsersComponent } from './template/chat-partnership/chat-users/chat-users.component';
import { ChatEventsComponent } from './template/chat-partnership/chat-events/chat-events.component';
import { ChatOrganizationsComponent } from './template/chat-partnership/chat-organizations/chat-organizations.component';
import { ChatProjectsComponent } from './template/chat-partnership/chat-projects/chat-projects.component';
import { ChatMessageComponent } from './template/chat-partnership/chat-message/chat-message.component';
import { DropdownAdminOwnerComponent } from './template/dropdown-admin-owner/dropdown-admin-owner.component';
import { ModalDeleteUserComponent } from './template/modal-delete-user/modal-delete-user.component';
import { SendComponent } from './template/chat-partnership/options/send/send.component';
import { ReceivedComponent } from './template/chat-partnership/options/received/received.component';
import { ComplaintPostComponent } from './template/complaint-post/complaint-post.component';
import { ModalSignInComponent } from './template/modal-sign-in/modal-sign-in.component';

// config de provider
const socialProvider = {
  provide: 'SocialAuthServiceConfig',
  useValue: {
    autoLogin: false,
    providers: [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(
          '710183299333-ml4mnvaoeq6p2g5o11g79s937drvhfh0.apps.googleusercontent.com'
        ),
      },
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('320987222588602'),
      },
    ],
  } as SocialAuthServiceConfig,
};

@NgModule({
  declarations: [
    HeaderComponent,
    SidenavComponent,
    PassionsFormComponent,
    ShortagesFormComponent,
    AddAdministratorsComponent,
    FullSpinnerComponent,
    PartialSpinnerComponent,
    PostComponent,
    DropdownPassionsComponent,
    DropdownProjectsComponent,
    DropdownPartnershipsComponent,
    DropdownEventsComponent,
    MediumSpinnerComponent,
    DropdownShortagesComponent,
    DropdownConfirmedPeopleComponent,
    MyPartnersDialogComponent,
    SuccessDialogComponent,
    ChatPartnershipComponent,
    ConfirmDialogComponent,
    TermsPoliciesAboutComponent,
    ShareExternallyComponent,
    NewPostComponent,
    MyPartnershipDialogComponent,
    LocalizacaoUsuariosComponent,
    LocalizacaoEventosComponent,
    LocalizacaoProjetosComponent,
    LocalizacaoEntidadesComponent,
    ChatUsersComponent,
    ChatEventsComponent,
    ChatOrganizationsComponent,
    ChatProjectsComponent,
    ChatMessageComponent,
    DropdownAdminOwnerComponent,
    ModalDeleteUserComponent,
    SendComponent,
    ReceivedComponent,
    ComplaintPostComponent,
    ModalSignInComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatNativeDateModule,
    MatRippleModule,
    MatIconModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatSidenavModule,
    NgxMaskModule.forRoot(),
    NgxCurrencyModule,
    RouterModule,
    NgxSpinnerModule,
    MatExpansionModule,
    MatDialogModule,
    MatCardModule,
    NgxPaginationModule,
    MatBadgeModule,
    ShareButtonsModule,
    ShareIconsModule,
    MatSlideToggleModule
  ],
  exports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatNativeDateModule,
    MatRippleModule,
    MatIconModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatSidenavModule,
    HeaderComponent,
    SidenavComponent,
    SocialLoginModule,
    NgxMaskModule,
    NgxCurrencyModule,
    PassionsFormComponent,
    ShortagesFormComponent,
    AddAdministratorsComponent,
    FullSpinnerComponent,
    PartialSpinnerComponent,
    NgxSpinnerModule,
    PostComponent,
    DropdownPassionsComponent,
    DropdownProjectsComponent,
    DropdownPartnershipsComponent,
    DropdownEventsComponent,
    DropdownShortagesComponent,
    DropdownConfirmedPeopleComponent,
    DropdownAdminOwnerComponent,
    MediumSpinnerComponent,
    MatExpansionModule,
    MatDialogModule,
    NewPostComponent,
    MyPartnersDialogComponent,
    NgxPaginationModule,
    MatBadgeModule,
    LocalizacaoUsuariosComponent,
    LocalizacaoEventosComponent,
    LocalizacaoProjetosComponent,
    LocalizacaoEntidadesComponent,
    ShareButtonsModule,
    ShareIconsModule,
    MatSlideToggleModule,
    ModalSignInComponent
  ],
  providers: [
    socialProvider,
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true, }
  ]
})
export class SharedModule { }
