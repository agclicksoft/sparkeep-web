import { TokenInterceptor } from './core/interceptors/token.interceptor';
import { DashboardModule } from './modules/module2/dashboard.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modularização
import { AuthenticatorModule } from './modules/module1/authenticator.module';
import { SharedModule } from './shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutosizeModule } from 'ngx-autosize';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    SharedModule,
    AuthenticatorModule,
    DashboardModule,
    NgSelectModule,
    AutosizeModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


