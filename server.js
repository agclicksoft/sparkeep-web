const express = require('express');
const compression = require('compression');
const app = express();
const path = require('path');

app.use(compression())

const nomeApp = 'sparkeep-web';

// FORÇAR HTTPS E REDIRECIONAR CASO SEJA HTTP
app.use((req, res, next) => { //Cria um middleware onde todas as requests passam por ele
  if ((req.headers["x-forwarded-proto"] || "").endsWith("http")) //Checa se o protocolo informado nos headers é HTTP
      res.redirect(`https://${req.hostname}${req.url}`); //Redireciona pra HTTPS
  else //Se a requisição já é HTTPS
      next(); //Não precisa redirecionar, passa para os próximos middlewares que servirão com o conteúdo desejado
});

app.use(express.static(`${__dirname}/dist/${nomeApp}`));

app.get('/*', (request, response) => {
  response.sendFile(path.join(`${__dirname}/dist/${nomeApp}/index.html`));
});

app.listen(process.env.PORT || 3000)
