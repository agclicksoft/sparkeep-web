# Sparkeep

![](./img.jpg)

## Sparkeep: Liam e Yuri

```
Iniciado em 27 de out de 2020. Concluído em xx de xxx de 2020.
```

Esse projeto se trata da versão front-end da aplicação, contendo as funcionalidades especificadas de acordo com o cliente, sendo o uso visual da aplicação com seus recursos e requisitos.

## Pré-requisitos

- Node.js `12.18.1` ou superior.
- NPM `6.14.4` ou superior.
- Angular-CLI `9.1.7` ou superior.


> Vá em [Node.js](https://nodejs.org/en/) e realize a instalação antes de prosseguir.
> Instale o [Angular-CLI](https://angular.io/guide/setup-local) e execute o passo a passo.

## Get Started

Siga os passos abaixo para conseguir rodar o projeto e visualizar suas funcionalidades.

### Instalando dependências

Para instalar todas as depêndencias do projeto, rode no terminal:

```
$ npm install
```

ou

```
$ yarn
```

### Buildando aplicação

Após ter todas as depedências instaladas, rode:

```
$ npm run build
```

ou

```
$ yarn build
```

Para **produção** utilize:

```
$ npm run build --prod
```

ou

```
$ yarn build --prod
```

#### Observações

> **OBS 1:** Perceba que foi gerada uma pasta **dist** no diretório raiz. Esta pasta contém todos os arquivos **.ts** de **src** transpilados para **.js**

### Startando aplicação

Após ter realizado o build da aplicação, rode:

#### Modo Desenvolvimento

```
$ npm run web
```

ou

```
$ yarn web
```

Terá uma saída parecida com esta:

> **Output:** > ** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
> : Compiled successfully.

Logo após, já será possível acessar `http://localhost:4200/` e usufruir da aplicação.

## Sobre

Essa Aplicação foi construída utilizando `Angular` com `Typescript`.

Durante o desenvolvimento utilizamos as boas práticas de **Clean Code** que é um estilo de desenvolvimento, que tem por foco a facilidade para escrever, ler e manter o código.

Utilizamos também o **ESLint** para conciliar com o estilo de desenvolvimento acima, e ajudar nas boas práticas de programação.

#### Observações

> **OBS 1:** Para saber mais detalhes sobre a arquitetura utilizada nesta Aplicação, dê uma olhada em [info](./info.txt).

The end.
